<?php

/**
 * Provides a 'Quick Facts Sidebar' Block
 *
 * @Block(
 *   id = "validatek_case_studies_quick_facts",
 *   admin_label = @Translation("[Validatek] Quick Facts"),
 * )
 */

namespace Drupal\validatek_case_studies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

class QuickFactsSidebar extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $block = [];

    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      $fields = [
        // Case Study fields.
        'field_services',
        'field_client',
        'field_contract_vehicles',

        // Technology and Methodology fields.
        // 'field_services', // "Category"
        'field_primary_stakeholders',
        'field_tools',
        'field_associated_concepts',
        'field_proven_success_at',
      ];

      foreach ($fields as $field_name) {
        if ($node->hasField($field_name) && !$node->{$field_name}->isEmpty()) {
          $references = $node->{$field_name}->referencedEntities();
          $block_id = str_ireplace('field_', '', $field_name);

          $block[$block_id] = [
            'header' => [
              '#markup' => $node->{$field_name}->getFieldDefinition()->getLabel(),
            ],
            'content' => [],
          ];

          foreach ($references as $term) {
            $block[$block_id]['content'][] = [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $term->label(),
              '#attributes' => [
                'class' => 'quick-facts-link'
              ],
            ];
          }
        }
      }

    }

    return $block;
  }

  public function getCacheTags() {
    // We need to add the current node id as a cache tag.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      // else just do normal stuff.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    // We also url path context.
    return Cache::mergeContexts(parent::getCacheContexts(), array('url.path'));
  }

}
