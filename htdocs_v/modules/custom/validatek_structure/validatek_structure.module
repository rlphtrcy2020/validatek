<?php

/**
 * @file
 * Contains validatek_structure.module..
 */

 use Drupal\Core\Entity\EntityInterface;
 use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
 use Drupal\Core\Url;

module_load_include('inc', 'validatek_structure', 'validatek_structure.token');

/**
 * Helper function for getting subtypes that should be grid based.
 */
function _validatek_grid_subtypes() {
  $subtypes = [
    'contract_vehicles' => 'content_grid',
    'search' => 'content_grid',
    'clients' => 'content_grid',
    'awards' => 'content_grid',
    'certifications' => 'content_grid',
  ];

  // Allow for alters.
  \Drupal::moduleHandler()->alter('grid_subtypes', $subtypes);


  return $subtypes;
}
/**
 * Subtype field values function
 */
function validatek_structure_subtype_values() {
  $subtypes = array(
    'clients' => 'Our Clients',
    'search' => 'Search',
    'contract_vehicles' => 'Contract Vehicles',
    'awards' => 'Awards',
    'certifications' => 'Certifications',
  );

  // Allow for alters.
  \Drupal::moduleHandler()->alter('validatek_structure_subtype', $subtypes);

  // Sort by array value + keep index.
  asort($subtypes);

  return $subtypes;
}

/**
 * Helper function to return provided subtype node.
 */
function validatek_structure_get_subtype($subtype = '') {
  if (empty($subtype)) {
    return FALSE;
  }

  // Entity Query.
  $efq = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'landing_page')
    ->condition('field_subtype', $subtype)
    ->range(0, 1);

  $nid = $efq->execute();

  if (empty($nid)) {
    return FALSE;
  }

  // Get the nid out of the array.
  $nid = reset($nid);
  return $nid;
}

function validatek_structure_get_all_subtypes() {
  // Entity Query.
  $efq = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'page')
    ->exists('field_subtype.value')
    ;

  $nids = $efq->execute();

  if (empty($nids)) {
    return FALSE;
  }

  return $nids;
}

/**
 * Implements hook_theme().
 *
 * @param $existing
 * @param $type
 * @param $theme
 * @param $path
 * @return array
 */
function validatek_structure_theme($existing, $type, $theme, $path) {
  return array(
    'actual_button' => array(
      'variables' => array('attributes' => NULL,'value' => NULL),
      'file' => 'validatek_structure.theme.inc',
    ),
    'content_grid' => array(
      'variables' => array('items' => NULL, 'readMore' => NULL),
    ),
    'logo_grid' => array(
      'variables' => array('items' => NULL),
    ),
    'people_grid' => array(
      'variables' => array(
        'items' => NULL,
        'header' => NULL
      ),
    ),
    'unordered_list' => array(
      'variables' => array('attributes' => NULL, 'items' => NULL),
      'file' => 'validatek_structure.theme.inc',
    ),
  );
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * @param array $suggestions
 * @param array $variables
 */
function validatek_structure_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  if (!empty($variables['elements']['#node']->field_subtype)) {
    if (!$variables['elements']['#node']->field_subtype->isEmpty()) {
      $node = $variables['elements']['#node'];
      $subtype = $variables['elements']['#node']->field_subtype->getValue();
      $subtype = reset($subtype);
      $suggestions[] = 'node__'.$node->bundle().'__'.$subtype['value'].'__'.$variables['elements']['#view_mode'];
    }
  }
}

/**
 * Implements hook_query_tag_alter().
 */
function validatek_structure_query_random_alter(Drupal\Core\Database\Query\AlterableInterface $query) {
  $query->orderRandom();
}

/**
 * Implements hook_node_view_alter().
 */
function validatek_structure_node_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  $mode = $display->getMode();
  $bundle = $entity->bundle();
  // Landing Page full view.
  if ($bundle == 'landing_page' && $mode == 'default') {
    if (!$entity->field_subtype->isEmpty()) {
      switch ($entity->field_subtype->value) {
        case 'contract_vehicles':
          $efq = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', 'contract_vehicle')
            ->sort('created','DESC');

          if ($nids = $efq->execute()) {
            $node_manager = \Drupal::entityTypeManager()->getStorage('node');
            $node_builder = \Drupal::entityManager()->getViewBuilder('node');
            $nodes = $node_manager->loadMultiple($nids);
            $views = $node_builder->viewMultiple($nodes, 'teaser');
            $build['content_grid'] = [
              '#theme' => 'content_grid',
              '#items' => $views,
              '#readMore' => [],
            ];
          }
          break;

        case 'awards':
        case 'certifications':
        case 'clients':
          $efq = \Drupal::entityQuery('taxonomy_term')
            ->sort('weight','ASC');
          if ($entity->field_subtype->value == 'clients') {
            $efq->condition('vid', 'clients');
          } elseif ($entity->field_subtype->value == 'awards') {
            $efq->condition('vid', 'awards');
          } else {
            $efq->condition('vid', 'certifications');
          }

          if ($tids = $efq->execute()) {
            $term_manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
            $terms = $term_manager->loadMultiple($tids);
            $items = [];
            foreach ($terms as $term) {
              $temp = [
                'title' => $term->label()
              ];
              if (!$term->field_image->isEmpty()) {
                $image = $term->field_image->view([
                  'label' => 'hidden',
                  'type' => 'image',
                  'settings' => ['image_style' => 'x_160']
                ]);
                $temp['logo'] = $image;
              }
              $items[] = $temp;
            }
            $build['logo_grid'] = [
              '#theme' => 'logo_grid',
              '#items' => $items,
            ];
          }
          break;
      }
    }
  }

  // Service Type page.
  if ($bundle == 'service_type' && $mode == 'default') {
    $efq = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'services')
      ->condition('field_type', $entity->id())
      ->sort('weight','ASC');

    if ($tids = $efq->execute()) {
      $items = [];
      $term_manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $terms = $term_manager->loadMultiple($tids);
      foreach ($terms as $term) {
        $temp = [
          'title' => $term->label(),
          'description' => ['#type' => 'markup', '#markup' => $term->getDescription()],
        ];
        if (!$term->field_content->isEmpty()) {
          $node_manager = \Drupal::entityTypeManager()->getStorage('node');
          $node_builder = \Drupal::entityManager()->getViewBuilder('node');
          $entity = $term->field_content->getValue();
          $entity = reset($entity);
          $node = $node_manager->load($entity['target_id']);
          $temp['content'] = $node_builder->view($node,'teaser');
        }
        $items[] = $temp;
      }
      $build['services'] = $items;
    }
  }
}

/**
 * Implements hook_preprocess_page().
 */
function validatek_structure_preprocess_page(&$vars) {
  if (!empty($vars['node'])) {
    if ($vars['node']->bundle() == 'landing_page') {
      if (!$vars['node']->field_subtype->isEmpty()) {
        $subtype = $vars['node']->field_subtype->value;
        $grids = _validatek_grid_subtypes();
        $grid_keys = array_keys($grids);
        if (in_array($subtype,$grid_keys)) {
          switch ($grids[$subtype]) {
            case 'content_grid':
              $vars['use_content_grid'] = TRUE;
              break;

            case 'people_grid':
              $vars['use_people_grid'] = TRUE;
              break;
          }
        }
      }
    } elseif ($vars['node']->bundle() == 'service_type') {
      $vars['use_services_grid'] = TRUE;
    }
  }
}

/**
 * Implements hook_js_alter().
 */
function validatek_structure_js_alter(&$javascript, \Drupal\Core\Asset\AttachedAssetsInterface $assets) {
  // We need to remove the push menu and our custom menu so that IMCE JS will work.
  // Currently, Push Menu errors when it's loaded in the IMCE window.
  $current_path = \Drupal::service('path.current')->getPath();
  if ($current_path == '/imce') {
    unset($javascript['themes/validatek/assets/js/bundle.js']);
  }
}
