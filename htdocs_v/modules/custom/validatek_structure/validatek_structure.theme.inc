<?php

/**
 * @file
 * Contains nafcu_structure.theme.inc
 */

use Drupal\Core\Template\Attribute;
use Drupal\Core\Render\Element;

/**
 * Preprocess function for custom UL theme function
 * This is largely copied from core/theme/item_list.
 */
function template_preprocess_unordered_list(&$variables) {
  if (empty($variables['items']) || empty($variables['attributes'])) { return; }
  $variables['attributes'] = new Attribute($variables['attributes']);
  foreach ($variables['items'] as &$item) {
    $attributes = array();
    // If the item value is an array, then it is a render array.
    if (is_array($item)) {
      // Determine whether there are any child elements in the item that are not
      // fully-specified render arrays. If there are any, then the child
      // elements present nested lists and we automatically inherit the render
      // array properties of the current list to them.
      foreach (Element::children($item) as $key) {
        $child = &$item[$key];
        // If this child element does not specify how it can be rendered, then
        // we need to inherit the render properties of the current list.
        if (!isset($child['#type']) && !isset($child['#theme']) && !isset($child['#markup'])) {
          // Since item-list.html.twig supports both strings and render arrays
          // as items, the items of the nested list may have been specified as
          // the child elements of the nested list, instead of #items. For
          // convenience, we automatically move them into #items.
          if (!isset($child['#items'])) {
            // This is the same condition as in
            // \Drupal\Core\Render\Element::children(), which cannot be used
            // here, since it triggers an error on string values.
            foreach ($child as $child_key => $child_value) {
              if ($child_key[0] !== '#') {
                $child['#items'][$child_key] = $child_value;
                unset($child[$child_key]);
              }
            }
          }
          // Lastly, inherit the original theme variables of the current list.
          $child['#theme'] = $variables['theme_hook_original'];
        }
      }
    }

    // Set the item's value and attributes for the template.
    $item = array(
      'value' => $item,
      'attributes' => new Attribute($attributes),
    );
  }
}

/**
 * Preprocess function for actual buttons.
 */
function template_preprocess_actual_button(&$variables) {
  $variables['attributes'] = new Attribute($variables['attributes']);
}
