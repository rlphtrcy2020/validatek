(function ($) {
  "use strict";

  $('.content-card-grid-cta a').on('click',function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $(this).addClass('loading');

    $.ajax({
      dataType: "json",
      url: url,
      success: function(data) {
        var container = '.content-card-grid-inner';
        if (data.nodes) {
          $(container).append(data.nodes);
        }
        if (data.more) {
          $('.content-card-grid-cta a').attr('href', data.more);
          $('.content-card-grid-cta a').removeClass('loading');
        } else {
          $('.content-card-grid-cta a').remove();
        }
      },
      error: function(data) {
        //console.log(data);
      }
    });
  });

})(jQuery);
