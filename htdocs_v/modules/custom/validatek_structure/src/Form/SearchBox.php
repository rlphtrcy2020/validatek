<?php

namespace Drupal\validatek_structure\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Providing global search box.
 */
class SearchBox extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'validatek_structure_searchbox';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['search_text'] = array(
      '#type' => 'search',
      '#default_value' => '',
      '#attributes' => ['placeholder' => 'Search by keyword...', 'class' => ['js-header-search']],
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Search',
      '#button_type' => 'primary',
      '#attributes' => ['class' => ['search-submit','-plain']]
    );

    $form['close'] = [
      '#theme' => 'actual_button',
      '#value' => '',
      '#attributes' => ['class' => ['js-search-close','-plain'],'aria-label' => 'close dialog']
    ];

    $form['#attributes']['class'][] = 'search-field';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('search_text');

    if ($subtype = validatek_structure_get_subtype('search')) {
      $params = [
        'query' => [
          'keywords' => $value,
        ],
      ];

      $url = Url::fromRoute('entity.node.canonical', ['node' => $subtype], $params);
      $form_state->setRedirectUrl($url);
    }
  }

}
