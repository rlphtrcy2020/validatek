<?php

namespace Drupal\validatek_structure;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Refinable CacheableJsonResponse definition.
 */
class QueryBucket implements RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

}
