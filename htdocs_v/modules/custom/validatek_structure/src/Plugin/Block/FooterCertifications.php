<?php

/**
 * Provides a 'Footer Certifications' Block
 *
 * @Block(
 *   id = "validatek_structure_footer_certifications",
 *   admin_label = @Translation("[Validatek] Footer Certifications"),
 * )
 */

namespace Drupal\validatek_structure\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FooterCertifications extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  protected function blockAccess(AccountInterface $account) {
    $access = AccessResult::allowed();
    if (!empty($this->routeMatch) && ($node = $this->routeMatch->getParameter('node'))) {
      if ($node->bundle() == 'webform') {
        $access = AccessResult::forbidden();
      }
    }

    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    // Get global Admin Entity.
    if ($ae = admin_entity_get_entities('global')) {
      if (!$ae->field_cmmi->isEmpty()) {
        $cmmi = $ae->field_cmmi->view([
          'label' => 'hidden',
          'type' => 'image',
          'settings' => ['image_style' => '300_y']
        ]);
        $block['cmmi'] = $cmmi;
      }

      if (!$ae->field_sri->isEmpty()) {
        $sri = $ae->field_sri->view([
          'label' => 'hidden',
          'type' => 'image',
          'settings' => ['image_style' => '200_y']
        ]);
        $block['sri'] = $sri;
      }
    }

    return $block;
  }

  public function getCacheTags() {
    // We need to add the current node id as a cache tag.
    if (!empty($this->routeMatch) && ($node = $this->routeMatch->getParameter('node'))) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      // else just do normal stuff.
      return parent::getCacheTags();
    }
  }
}
