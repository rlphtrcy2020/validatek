<?php

/**
 * Provides a 'Footer Branding' Block
 *
 * @Block(
 *   id = "validatek_structure_footer_branding",
 *   admin_label = @Translation("[Validatek] Footer Branding"),
 * )
 */

namespace Drupal\validatek_structure\Plugin\Block;

use Drupal\Core\Block\BlockBase;

class FooterBranding extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get global Admin Entity.
    $ae = admin_entity_get_entities('global');

    // Setup copyright and year; then append text from admin.
    $markup = '&copy; '.date('Y').' ';

    if (!empty($ae) && !$ae->field_copyright->isEmpty()) {
      $text = $ae->field_copyright->getValue();
      if (!empty($text)) {
        $text = reset($text);
        $markup .= $text['value'];
      }
    }

    $block = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];

    return $block;
  }
}
