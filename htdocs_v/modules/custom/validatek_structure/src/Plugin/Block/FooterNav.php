<?php

namespace Drupal\validatek_structure\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides a generic Menu block.
 *
 * @Block(
 *   id = "utility_footer_nav",
 *   admin_label = @Translation("[Validatek] Utility Footer Nav"),
 * )
 */
class FooterNav extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  public function getDerivativeId() {
    return 'footer';
  }

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuLinkTreeInterface $menu_tree, MenuActiveTrailInterface $menu_active_trail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail')
    );
  }

  /**
   * Form API callback: Processes the menu_levels field element.
   *
   * Adjusts the #parents of menu_levels to save its children at the top level.
   */
  public static function processMenuLevelParents(&$element, FormStateInterface $form_state, &$complete_form) {
    array_pop($element['#parents']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = 'footer';
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);

    // We need to set max depth.
    $parameters->setMaxDepth(1);

    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $this->menuTree->transform($tree, $manipulators);
    $menu = $this->menuTree->build($tree);
    $links = [];
    if (!empty($menu['#items'])) {
      foreach ($menu['#items'] as $key => $menu_link) {
        $links[] = [
          '#type' => 'link',
          '#url' => $menu_link['url'],
          '#title' => $menu_link['title']
        ];
      }
    }

    // Get global Admin Entity.
    $ae = admin_entity_get_entities('global');
    $social_media = [];

    if (!empty($ae)) {
      // LinkedIn.
      $linkedin = $ae->field_linkedin->view();
      if (!empty($linkedin[0])) {
        $linkedin[0]['#options']['attributes']['class'][] = 'linkedin';
        $social_media[] = $linkedin[0];
      }
      
      // Facebook.
      $facebook = $ae->field_facebook->view();
      if (!empty($facebook[0])) {
        $facebook[0]['#options']['attributes']['class'][] = 'facebook';
        $social_media[] = $facebook[0];
      }

      // Twitter.
      $twitter = $ae->field_twitter->view();
      if (!empty($twitter[0])) {
        $twitter[0]['#options']['attributes']['class'][] = 'twitter';
        $social_media[] = $twitter[0];
      }

      // Instagram.
      $instagram = $ae->field_instagram->view();
      if (!empty($instagram[0])) {
        $instagram[0]['#options']['attributes']['class'][] = 'instagram';
        $social_media[] = $instagram[0];
      }

      $contact = [];
      // Phone.
      if (!$ae->field_telephone->isEmpty()) {
        $contact['telephone'] = $ae->field_telephone->view([
          'label' => 'hidden',
          'type' => 'telephone_link'
        ]);
      }
      // Email.
      if (!$ae->field_email->isEmpty()) {
        $contact['email'] = $ae->field_email->view([
          'label' => 'hidden',
          'type' => 'email_mailto'
        ]);
      }
      // Address.
      if ($address = $ae->field_address->getValue()) {
        $address = reset($address);
        $contact['address'] = $address['value'];
      }
    }

    return [
      'links' => $links,
      'social' => $social_media,
      'contact' => $contact
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'level' => 1,
      'depth' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Even when the menu block renders to the empty string for a user, we want
    // the cache tag for this menu to be set: whenever the menu is changed, this
    // menu block must also be re-rendered for that user, because maybe a menu
    // link that is accessible for that user has been added.
    $cache_tags = parent::getCacheTags();
    $cache_tags[] = 'config:system.menu.' . $this->getDerivativeId();
    return $cache_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // ::build() uses MenuLinkTreeInterface::getCurrentRouteMenuTreeParameters()
    // to generate menu tree parameters, and those take the active menu trail
    // into account. Therefore, we must vary the rendered menu by the active
    // trail of the rendered menu.
    // Additional cache contexts, e.g. those that determine link text or
    // accessibility of a menu, will be bubbled automatically.
    $menu_name = $this->getDerivativeId();
    return Cache::mergeContexts(parent::getCacheContexts(), ['route.menu_active_trails:' . $menu_name]);
  }
}
