<?php

/**
 * Provides a 'Page Header' Block
 *
 * @Block(
 *   id = "validatek_structure_page_header",
 *   admin_label = @Translation("[Validatek] Page Header"),
 * )
 */

namespace Drupal\validatek_structure\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

class PageHeader extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get global Admin Entity.
    $ae = admin_entity_get_entities('global');
    $block = [
      'title' => '',
      'image' => '',
      'text' => '',
    ];
    // Get current node, unless its the front page.
    if (\Drupal::service('path.matcher')->isFrontPage()) {
      // Slider stuff goes here.
      if (!empty($ae)) {
        if (!$ae->field_homepage_slider->isEmpty()) {
          $block['slides'] = $ae->field_homepage_slider->view([
            'label' => 'hidden'
          ]);
          $block['slides']['#cache']['contexts'][] = 'url.path';
        }
      }
    } elseif ($node = \Drupal::routeMatch()->getParameter('node')) {
      $block['title'] = ['#type' => 'markup','#markup' => $node->label()];
      $block['title']['#cache']['contexts'][] = 'url.path';
      if ($node->hasField('field_image') && $node->bundle() != 'person') {
        if (!$node->field_image->isEmpty()) {
          $block['image'] = $node->field_image->view([
            'label' => 'hidden',
            'type' => 'image_url',
            'settings' => ['image_style' => '1500_y']
          ]);
          $block['image']['#cache']['contexts'][] = 'url.path';
        }
      }
      if ($node->hasField('field_header_text')) {
        if (!$node->field_header_text->isEmpty()) {
          $block['text'] = $node->field_header_text->view([
            'label' => 'hidden'
          ]);
          $block['text']['#cache']['contexts'][] = 'url.path';
        }
      }
    } else {
      $request = \Drupal::request();
      $route_match = \Drupal::routeMatch();
      $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
      $block['title'] = ['#type' => 'markup','#markup' => $title];
      $block['title']['#cache']['contexts'][] = 'url.path';
    }

    if (empty($block['image']) && !empty($ae)) {
      if (!$ae->field_image->isEmpty()) {
        $block['image'] = $ae->field_image->view([
          'label' => 'hidden',
          'type' => 'image_url',
          'settings' => ['image_style' => '1500_y']
        ]);
        $block['image']['#cache']['contexts'][] = 'url.path';
      }
    }

    // Back link.
    if (!empty($node)) {
      $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
      $links = $menu_link_manager->loadLinksByRoute('entity.node.canonical', array('node' => $node->id()));
      if ($link = reset($links)) {
        if ($parent = $link->getParent()) {
          $parent = $menu_link_manager->createInstance($parent);
          $parent_title = $parent->getTitle();
          $parent_url = $parent->getUrlObject();
          $parent_url->setOptions(['attributes' => ['class' => ['angle-hero-back-link']]]);
          $block['back_link'] = [
            '#type' => 'link',
            '#title' => 'Back to '.$parent_title,
            '#url' => $parent_url,
            '#cache' => [
              'contexts' => ['url.path'],
            ]
          ];
        }
      } else {
        if ($node->bundle() == 'contract_vehicle') {
          if ($cv = validatek_structure_get_subtype('contract_vehicles')) {
            $url = Url::fromRoute('entity.node.canonical', ['node' => $cv], ['attributes' => ['class' => ['angle-hero-back-link']]]);
            $title = 'Back To Contract Vehicles';
            $block['back_link'] = [
              '#type' => 'link',
              '#title' => $title,
              '#url' => $url,
              '#cache' => [
                'contexts' => ['url.path'],
              ]
            ];
          }
        }
      }
    }

    // Allow for alter.
    \Drupal::moduleHandler()->alter('page_header', $block);

    return $block;
  }
}
