<?php

namespace Drupal\validatek_structure\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Url;

class ReadMoreController extends ControllerBase {

  /**
   * Data endpoint for secure ember side.
   */
  function getMore() {
    $data = ['nodes' => [],'more' => ''];
    $query = \Drupal::request()->query->all();
    if (empty($query['type'])) {
      $response = new JsonResponse($data, 200, ['Content-Type' => 'application/vnd.api+json']);
      return $response;
    }
    $type = $query['type'];

    $efq = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->pager(8);

    switch ($type) {
      case 'news_item':
        $efq->condition('type', ['case_study','news_item','whitepaper'], 'IN')
          ->sort('created','DESC');
        break;

      case 'person':
        $efq->condition('type', 'person')
          ->condition('field_person_type', 'featured')
          ->sort('field_weight','ASC');

      default:
        $efq->condition('type', $type)
          ->sort('created','DESC');
        break;
    }

    if ($nids = $efq->execute()) {
      $node_manager = \Drupal::entityTypeManager()->getStorage('node');
      $node_builder = \Drupal::entityManager()->getViewBuilder('node');
      $nodes = $node_manager->loadMultiple($nids);
      $views = $node_builder->viewMultiple($nodes, 'teaser');
      $data['nodes'] = render($views);
      global $pager_total_items;
      $page = empty($query['page']) ? 1 : $query['page']+1;
      if (reset($pager_total_items) >= (8*$page)) {
        $options = ['query' => ['type' => $type,'page' => $page], 'attributes' => ['class' => ['button'], 'data-type' => 'case_study']];
        $url = Url::fromRoute('validatek_structure.readmore', [], $options);
        $data['more'] = $url->toString();
      }
    }

    $response = new JsonResponse($data, 200, ['Content-Type' => 'application/vnd.api+json']);
    return $response;
  }
}
