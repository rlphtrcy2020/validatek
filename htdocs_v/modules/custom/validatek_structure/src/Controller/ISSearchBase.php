<?php

namespace Drupal\validatek_structure\Controller;

use Drupal\Core\Database\Database;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\validatek_structure\QueryBucket;

/**
 * Class EntityController.
 *
 * @package Drupal\validatek_structure\src
 */
class ISSearchBase extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  protected $facets = [];

  protected $sorting = [];

  protected $fullTextFields = [];

  protected $nodes;

  protected $activeFacets;

  protected $index;

  protected $search_index;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Main endpoint method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $index
   * @param int $limit
   * @param int $page
   * @return array|bool|\Symfony\Component\HttpFoundation\JsonResponse
   */
  public function search(Request $request, $index, $limit = 21, $page = 1) {
    if (empty($index)) {
      return FALSE;
    }

    // Load Search API Index.
    $query = $request->query->getIterator();
    $index_storage = $this->entityManager->getStorage('search_api_index');
    $search_index = $index_storage->load($index);
    $this->search_index = $search_index;

    $q = $search_index->query([
      'search id' => 'search_api:'.$index,
    ]);

    foreach ($query as $parameter => $value) {
      if (empty($value)) {
        continue;
      }

      if (in_array($parameter, array_keys($this->facets))) {
        $param = is_array($value) ? 'IN' : '=';
        $conditions = explode(',',$value);
        $group = $q->createConditionGroup('OR',[]);
        foreach ($conditions as $condition) {
          $group->addCondition($this->facets[$parameter], $condition, $param);
        }
        $q->addConditionGroup($group);

        if (is_array($value)) {
          foreach ($value as $val) {
            $this->activeFacets[$val] = TRUE;
          }
        }
        else {
          $this->activeFacets[$value] = TRUE;
        }
      }
    }

    // Pager.
    $offset = ($page-1) * $limit;

    // Set Range.
    $q->range($offset, $limit);

    // Sorting.
    if (!empty($this->sorting) && !empty($query['_sort'])) {
      foreach ($this->sorting as $sort) {
        if ($sort['value'] == $query['_sort']) {
          // Search API sorts by relevance by default, we don't need this then.
          if ($query['_sort'] != 'relevant') {
            $q->sort($query['_sort'], $sort['order']);
          }
        }
      }
    }

    // Keywords.
    if (!empty($query['keywords']) && !empty($this->fullTextFields)) {
      $q->keys($query['keywords']);
    }

    $results = $q->execute();

    if ($results->getResultCount() == 0) {
      // no reults.
      return ['results' => [], 'query' => $results];
    }

    foreach ($results->getIterator() as $key => $value) {
      // ID given by searchAPI = entity:node/8:en.
      $search_item_id = $value->getId();
      $exploded       = explode(':', $search_item_id);
      $nodepath       = $exploded[1];
      $node_exploded  = explode('/', $nodepath);

      $this->nodes[]  = $node_exploded[1];
    }

    if (empty($this->nodes)) {
      // No nodes.
      return ['results' => [], 'query' => $results];
    }

    return ['results' => $this->nodes, 'query' => $results];
  }

  /**
   * @param string $request
   * @param string $results
   * @param int $limit
   * @return array
   */
  protected function getMeta($request = '', $results = '', $limit = 21) {
    if (empty($request)) {
      return [];
    }
    $meta = [];

    // Start with facets.
    $facets = $this->buildFacets($request);
    $meta['facets'] = $facets;

    // Total Result Count.
    if (empty($results)) {
      $meta['totalResults'] = 0;
    } else {
      $meta['totalResults'] = $results->getResultCount();
    }
    // Total Pages.
    $meta['totalPages'] = ceil($meta['totalResults'] / $limit);

    // Sorting options.
    if (!empty($this->sorting)) {
      $meta['sorting'] = $this->sorting;
    }

    return $meta;
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function results(Request $request) {
    $query = $request->query->getIterator();
    $limit = empty($query['_limit']) ? 21 : $query['_limit'];
    $page = empty($query['_page']) ? 1 : $query['_page'];

    // Search for nodes
    $results = $this->search($request, $this->index, $limit, $page);

    if ($results == false || count($this->nodes) == 0) {
      $results['results'] = [];
      $results['query'] = [];
    }

    // Format search results
    $fr = $this->formatResults($results['results']);

    $cacheable = $fr['cacheable'];
    $formatted = [
      'results' => $fr['json'],
      'meta' => [],
    ];

    // Count query.
    $formatted['meta'] = $this->getMeta($request, $results['query'], $limit);

    // Response
    //$response = new JsonResponse($formatted, 200,
    //  ['Content-Type' => 'application/vnd.api+json']);
    //return $response;

    $response = new CacheableJsonResponse($formatted, 200, ['Content-Type' => 'application/vnd.api+json']);
    // Cache loaded nodes on the response
    if (!empty($cacheable)) {
      foreach ($cacheable as $thing) {
        $response->addCacheableDependency($thing);
      }
    }

    $queryParameters = new QueryBucket();
    $queryParameters->addCacheContexts(['url.query_args']);
    if (!empty($this->index)) {
      $queryParameters->addCacheTags(['api_list:'.$this->index]);
    }
    $response->addCacheableDependency($queryParameters);

    return $response;
  }

  /**
   * Lets build some facets.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return array
   */
  protected function buildFacets(Request $request) {
    $facets = [];

    // Count query.
    //foreach ($this->facets as $facet_key => $facet) {
    //  $counts = $this->countFacets($facet, $request);
    //}

    return $facets;
  }

  /**
   * @param $nids
   * @return array
   */
  protected function formatResults($nids) {
    if (empty($nids)) {
      return ['json' => [], 'cacheable' => []];
    }
    $objects = [];
    $cacheable = [];
    $nodes = $this->entityManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $nid => $node) {
      $temp = [
        'id' => $node->id(),
        'title' => $node->label(),
        'url' => $node->toUrl()->toString(TRUE)->getGeneratedUrl(),
      ];
      $objects[] = $temp;
      $cacheable[] = $node;
    }
    return ['json' => $objects, 'cacheable' => $cacheable];
  }

  /**
   * Count facets.
   */
  protected function countFacets($facet, $request) {
    if (empty($this->index)) {
      return [];
    }

    $q = db_select('search_api_db_'.$this->index.'_' . $facet, 'sfc_' . $facet);
    foreach ($this->facets as $facet_key => $extra_facet) {
      if ($extra_facet === $facet) {
        continue;
      }
      $q->leftJoin('search_api_db_'.$this->index.'_' . $extra_facet, 'sfc_' . $extra_facet, 'sfc_' . $facet . '.item_id = sfc_' . $extra_facet . '.item_id');
    }
    $q->addField('sfc_' . $facet, 'value', 'tid');
    $q->groupBy('sfc_' . $facet . '.value');
    $q->addExpression('COUNT(DISTINCT sfc_' . $facet . '.item_id)', 'count');

    // -- For now we're not going to worry about updating counts based on query string.
    // This is complicated by allowing multiple values on a facet.
    /*
    foreach ($request->query->getIterator() as $parameter => $value) {
      if (empty($value)) {
        continue;
      }

      if (in_array($parameter, array_keys($this->facets))) {
        $param = is_array($value) ? 'IN' : '=';
        $conditions = explode(',',$value);
        $group = $q->orConditionGroup();
        foreach ($conditions as $condition) {
          $group->condition('sfc_' . $this->facets[$parameter] . '.value', $condition, $param);
        }
        $q->condition($group);
      }
    }
    */

    $results = $q->execute();
    $counts = $results->fetchAll();
    if (empty($counts)) {
      return [];
    }

    foreach ($counts as $count) {
      // vocab_name: { tid_val: { count: count_int } }.
      $r[$count->tid] = ['count' => $count->count];
    }
    return $r;
  }

}
