<?php

namespace Drupal\validatek_structure\Controller;

use Drupal\validatek_structure\Controller\ISSearchBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EntityController.
 *
 * @package Drupal\nafcu_search\src
 */
class SearchController extends ISSearchBase {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  protected $facets = [];

  protected $sorting = [
    ['label' => 'Most Relevant','value' => 'relevant','order' => 'asc']
  ];

  protected $fullTextFields = ['title','body','summary_processed','field_first_name','field_last_name','field_job_title','contract_vehicle_title','client_name','service_name'];

  protected $nodes;

  protected $activeFacets;

  protected $index = 'site_index';

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return array
   */
  protected function buildFacets(Request $request) {
    $facets = [];
    $facets[] = ['type' => 'textfield', 'name' => 'keywords','title' => null,'enabled' => true];
    return $facets;
  }

  /**
   * Override format results function.
   *
   * @param $nids
   * @return array
   */
  protected function formatResults($nids) {
    if (empty($nids)) {
      return ['json' => [], 'cacheable' => []];
    }
    $objects = [];
    $cacheable = [];
    $node_manager = $this->entityManager->getStorage('node');
    $nodes = $node_manager->loadMultiple($nids);
    foreach ($nodes as $nid => $node) {
      $temp = [
        'id' => $node->id(),
        'title' => $node->label(),
        'url' => $node->toUrl()->toString(TRUE)->getGeneratedUrl(),
      ];
      if (!$node->body->isEmpty()) {
        $body = $node->body->view([
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'settings' => [
            'trim_length' => '250'
          ]
        ]);
        $temp['text'] = \Drupal::service('renderer')->renderRoot($body);
      }

      $objects[] = $temp;
      $cacheable[] = $node;
    }
    return ['json' => $objects, 'cacheable' => $cacheable];
  }

}
