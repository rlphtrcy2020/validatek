<?php

use Drupal\Core\Render\BubbleableMetadata;

function validatek_structure_token_info() {
  $types = [];
  $types['node'] = [
    'name' => t('Node'),
    'description' => t('Tokens related to nodes.'),
    'needs-data' => 'node',
  ];

  $subtypes = validatek_structure_subtype_values();
  $tokens = [
    'subtypes' => [],
  ];
  foreach ($subtypes as $key=>$subtype) {
    // Chained tokens for nodes.
    $tokens['node'][$key . '_subtype_path'] = [
      'name' => t('Subtype ' . $subtype . ' Menu Path'),
    ];
    $tokens['term'][$key . '_subtype_path'] = [
      'name' => t('Subtype ' . $subtype . ' Menu Path'),
    ];
  }

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function validatek_structure_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $subtypes = validatek_structure_subtype_values();
  $replacements = [];
  foreach($subtypes as $key=>$subtype) {
    if (!empty($tokens[$key . '_subtype_path'])) {
      $nid = validatek_structure_get_subtype($key);
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid);
      $original = $tokens[$key . '_subtype_path'];
      $replacements[$original] = $alias;
    }
  }

  return $replacements;

}
