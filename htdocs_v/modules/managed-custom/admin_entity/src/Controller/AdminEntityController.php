<?php

namespace Drupal\admin_entity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\admin_entity\Entity\AdminEntityInterface;

/**
 * Class AdminEntityController.
 *
 *  Returns responses for Admin Entity routes.
 *
 * @package Drupal\admin_entity\Controller
 */
class AdminEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Admin Entity  revision.
   *
   * @param int $admin_entity_revision
   *   The Admin Entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($admin_entity_revision) {
    $admin_entity = $this->entityManager()->getStorage('admin_entity')->loadRevision($admin_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('admin_entity');

    return $view_builder->view($admin_entity);
  }

  /**
   * Page title callback for a Admin Entity  revision.
   *
   * @param int $admin_entity_revision
   *   The Admin Entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($admin_entity_revision) {
    $admin_entity = $this->entityManager()->getStorage('admin_entity')->loadRevision($admin_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $admin_entity->label(), '%date' => format_date($admin_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Admin Entity .
   *
   * @param \Drupal\admin_entity\Entity\AdminEntityInterface $admin_entity
   *   A Admin Entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AdminEntityInterface $admin_entity) {
    $account = $this->currentUser();
    $langcode = $admin_entity->language()->getId();
    $langname = $admin_entity->language()->getName();
    $languages = $admin_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $admin_entity_storage = $this->entityManager()->getStorage('admin_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $admin_entity->label()]) : $this->t('Revisions for %title', ['%title' => $admin_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all admin entity revisions") || $account->hasPermission('administer admin entity entities')));
    $delete_permission = (($account->hasPermission("delete all admin entity revisions") || $account->hasPermission('administer admin entity entities')));

    $rows = [];

    $vids = $admin_entity_storage->revisionIds($admin_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\admin_entity\AdminEntityInterface $revision */
      $revision = $admin_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $admin_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.admin_entity.revision', ['admin_entity' => $admin_entity->id(), 'admin_entity_revision' => $vid]));
        }
        else {
          $link = $admin_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.admin_entity.translation_revert', ['admin_entity' => $admin_entity->id(), 'admin_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.admin_entity.revision_revert', ['admin_entity' => $admin_entity->id(), 'admin_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.admin_entity.revision_delete', ['admin_entity' => $admin_entity->id(), 'admin_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['admin_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
