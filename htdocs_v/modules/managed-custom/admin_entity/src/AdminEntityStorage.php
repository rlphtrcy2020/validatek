<?php

namespace Drupal\admin_entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\admin_entity\Entity\AdminEntityInterface;

/**
 * Defines the storage handler class for Admin Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Admin Entity entities.
 *
 * @ingroup admin_entity
 */
class AdminEntityStorage extends SqlContentEntityStorage implements AdminEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(AdminEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {admin_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {admin_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(AdminEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {admin_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('admin_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
