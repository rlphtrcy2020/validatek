<?php

namespace Drupal\admin_entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\admin_entity\Entity\AdminEntityInterface;

/**
 * Defines the storage handler class for Admin Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Admin Entity entities.
 *
 * @ingroup admin_entity
 */
interface AdminEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Admin Entity revision IDs for a specific Admin Entity.
   *
   * @param \Drupal\admin_entity\Entity\AdminEntityInterface $entity
   *   The Admin Entity entity.
   *
   * @return int[]
   *   Admin Entity revision IDs (in ascending order).
   */
  public function revisionIds(AdminEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Admin Entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Admin Entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\admin_entity\Entity\AdminEntityInterface $entity
   *   The Admin Entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(AdminEntityInterface $entity);

  /**
   * Unsets the language for all Admin Entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
