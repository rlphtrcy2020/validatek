<?php

namespace Drupal\admin_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Admin Entity entity.
 *
 * @see \Drupal\admin_entity\Entity\AdminEntity.
 */
class AdminEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\admin_entity\Entity\AdminEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished admin entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published admin entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit admin entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete admin entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add admin entity entities');
  }

}
