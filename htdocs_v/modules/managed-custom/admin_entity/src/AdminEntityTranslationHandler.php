<?php

namespace Drupal\admin_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for admin_entity.
 */
class AdminEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
