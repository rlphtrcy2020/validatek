<?php

namespace Drupal\admin_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Admin Entity entities.
 *
 * @ingroup admin_entity
 */
class AdminEntityDeleteForm extends ContentEntityDeleteForm {


}
