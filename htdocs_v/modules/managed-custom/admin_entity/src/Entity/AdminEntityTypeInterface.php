<?php

namespace Drupal\admin_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Admin Entity type entities.
 */
interface AdminEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
