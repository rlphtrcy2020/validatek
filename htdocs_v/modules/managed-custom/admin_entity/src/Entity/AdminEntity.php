<?php

namespace Drupal\admin_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Admin Entity entity.
 *
 * @ingroup admin_entity
 *
 * @ContentEntityType(
 *   id = "admin_entity",
 *   label = @Translation("Admin Entity"),
 *   bundle_label = @Translation("Admin Entity type"),
 *   handlers = {
 *     "storage" = "Drupal\admin_entity\AdminEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\admin_entity\AdminEntityListBuilder",
 *     "views_data" = "Drupal\admin_entity\Entity\AdminEntityViewsData",
 *     "translation" = "Drupal\admin_entity\AdminEntityTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\admin_entity\Form\AdminEntityForm",
 *       "add" = "Drupal\admin_entity\Form\AdminEntityForm",
 *       "edit" = "Drupal\admin_entity\Form\AdminEntityForm",
 *       "delete" = "Drupal\admin_entity\Form\AdminEntityDeleteForm",
 *     },
 *     "access" = "Drupal\admin_entity\AdminEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\admin_entity\AdminEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "admin_entity",
 *   data_table = "admin_entity_field_data",
 *   revision_table = "admin_entity_revision",
 *   revision_data_table = "admin_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer admin entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/admin_entity/{admin_entity}",
 *     "add-page" = "/admin/content/admin_entity/add",
 *     "add-form" = "/admin/content/admin_entity/add/{admin_entity_type}",
 *     "edit-form" = "/admin/content/admin_entity/{admin_entity}/edit",
 *     "delete-form" = "/admin/content/admin_entity/{admin_entity}/delete",
 *     "version-history" = "/admin/content/admin_entity/{admin_entity}/revisions",
 *     "revision" = "/admin/content/admin_entity/{admin_entity}/revisions/{admin_entity_revision}/view",
 *     "revision_revert" = "/admin/content/admin_entity/{admin_entity}/revisions/{admin_entity_revision}/revert",
 *     "translation_revert" = "/admin/content/admin_entity/{admin_entity}/revisions/{admin_entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/content/admin_entity/{admin_entity}/revisions/{admin_entity_revision}/delete",
 *     "collection" = "/admin/content/admin_entity",
 *   },
 *   bundle_entity_type = "admin_entity_type",
 *   field_ui_base_route = "entity.admin_entity_type.edit_form"
 * )
 */
class AdminEntity extends RevisionableContentEntityBase implements AdminEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Admin Entity entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
