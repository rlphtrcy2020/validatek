<?php

namespace Drupal\admin_entity\Entity;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Admin Entity entities.
 *
 * @ingroup admin_entity
 */
interface AdminEntityInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Denotes that the node is not published.
   */
  const NOT_PUBLISHED = 0;

  /**
   * Denotes that the node is published.
   */
  const PUBLISHED = 1;
  
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Admin Entity name.
   *
   * @return string
   *   Name of the Admin Entity.
   */
  public function getName();

  /**
   * Sets the Admin Entity name.
   *
   * @param string $name
   *   The Admin Entity name.
   *
   * @return \Drupal\admin_entity\Entity\AdminEntityInterface
   *   The called Admin Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Admin Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Admin Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Admin Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Admin Entity creation timestamp.
   *
   * @return \Drupal\admin_entity\Entity\AdminEntityInterface
   *   The called Admin Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Admin Entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Admin Entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\admin_entity\Entity\AdminEntityInterface
   *   The called Admin Entity entity.
   */
  public function setRevisionCreationTime($timestamp);

}
