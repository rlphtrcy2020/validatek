<?php

namespace Drupal\admin_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Admin Entity type entity.
 *
 * @ConfigEntityType(
 *   id = "admin_entity_type",
 *   label = @Translation("Admin Entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\admin_entity\AdminEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\admin_entity\Form\AdminEntityTypeForm",
 *       "edit" = "Drupal\admin_entity\Form\AdminEntityTypeForm",
 *       "delete" = "Drupal\admin_entity\Form\AdminEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\admin_entity\AdminEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "admin_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "admin_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/admin_entity_type/{admin_entity_type}",
 *     "add-form" = "/admin/structure/admin_entity_type/add",
 *     "edit-form" = "/admin/structure/admin_entity_type/{admin_entity_type}/edit",
 *     "delete-form" = "/admin/structure/admin_entity_type/{admin_entity_type}/delete",
 *     "collection" = "/admin/structure/admin_entity_type"
 *   }
 * )
 */
class AdminEntityType extends ConfigEntityBundleBase implements AdminEntityTypeInterface {

  /**
   * The Admin Entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Admin Entity type label.
   *
   * @var string
   */
  protected $label;

}
