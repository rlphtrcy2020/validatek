<?php

/**
 * @file
 * Contains admin_entity.page.inc.
 *
 * Page callback for Admin Entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Admin Entity templates.
 *
 * Default template: admin_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_admin_entity(array &$variables) {
  // Fetch AdminEntity Entity Object.
  $admin_entity = $variables['elements']['#admin_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
