<?php

/**
 * @file
 * Contains component.page.inc..
 *
 * Page callback for Component entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Prepares variables for Component templates.
 *
 * Default template: component.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_component(array &$variables) {
  // Fetch Component Entity Object.
  $component = $variables['elements']['#component'];
  $variables['type'] = $component->getType();

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  $variables['order'] = empty($variables['elements']['#ordinality']) ? '' : $variables['elements']['#ordinality'];
}

/**
 * Prepares variables for a custom entity type creation list templates.
 *
 * Default template: component-content-add-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of component-types.
 *
 * @see block_content_add_page()
 */
function template_preprocess_component_content_add_list(&$variables) {
  $variables['types'] = array();
  $query = \Drupal::request()->query->all();
  foreach ($variables['content'] as $type) {
    $variables['types'][$type->id()] = array(
      'link' => new Url(
        'entity.component.add_form',
        array(
          'component_type' => $type->id(),
        ),
        array(
          'query' => $query,
        )
      ),
      'description' => array(
        '#markup' => $type->getDescription(),
      ),
      'title' => $type->label(),
      'localized_options' => array(
        'query' => $query,
      ),
    );
  }
}
