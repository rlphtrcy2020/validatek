<?php

/**
 * @file
 * Contains \Drupal\components\ComponentListBuilder.
 */

namespace Drupal\components;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Component entities.
 *
 * @ingroup components
 */
class ComponentListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // $header['id'] = $this->t('Component ID');
    $header['name'] = $this->t('Name');
    $header['type'] = $this->t('Type');
    $header['appears_on'] = $this->t('Appears On');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\components\Entity\Component */
    // $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.component.edit_form', array(
          'component' => $entity->id(),
        )
      )
    );

    $row['type'] = $entity->getType();
    $row['appears_on'] = $this->buildAppearsOn($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * Find what entities a component appears on.
   * @param  EntityInterface $entity The entity in question.
   * @return array                  list of node view links
   */
  public function buildAppearsOn(EntityInterface $entity) {
    // We only want to check bundles that have the field on them.
    // If any node has either the component fields, the query will work.
    $field_map = \Drupal::entityManager()->getFieldMap();
    $node_bundles = [];
    if (!empty($field_map['node']['field_components'])) {
      $node_bundles += $field_map['node']['field_components']['bundles'];
    }
    if (!empty($field_map['node']['field_component_reference'])) {
      $node_bundles += $field_map['node']['field_component_reference']['bundles'];
    }

    $entity_query_service = \Drupal::service('entity.query');
    $nodes = array();
    if (!empty($node_bundles)) {
      $node_storage = \Drupal::entityManager()->getStorage('node');
      $query = $entity_query_service->get('node');

      // If nodes have field_component_reference || field_component.
      if (!empty($field_map['node']['field_component_reference']) || !empty($field_map['node']['field_components'])) {
        $group = $query->orConditionGroup();
        if (!empty($field_map['node']['field_component_reference'])) {
          $group->condition('field_component_reference', $entity->id());
        }
        if (!empty($field_map['node']['field_components'])) {
          $group->condition('field_components', $entity->id());
        }
        $query->condition($group);
        $nids = $query->execute();
        $nodes = $node_storage->loadMultiple($nids);
      }
    }

    $term_bundles = [];
    if (!empty($field_map['taxonomy_term']['field_components'])) {
      $term_bundles += $field_map['taxonomy_term']['field_components']['bundles'];
    }
    $terms = array();
    if (!empty($term_bundles)) {
      $term_storage = \Drupal::entityManager()->getStorage('taxonomy_term');
      $query_terms = $entity_query_service->get('taxonomy_term');
      $query_terms->condition('field_components', $entity->id());
      $tids = $query_terms->execute();
      $terms = $term_storage->loadMultiple($tids);
    }


    $results = array();
    foreach ($nodes as $node) {
      $url = Url::fromRoute('entity.node.canonical',
        array('node' => $node->id()));

      $result = \Drupal::l($node->getTitle(), $url);
      $results[] = render($result);
    }

    foreach ($terms as $term) {
      $url = Url::fromRoute('entity.taxonomy_term.canonical',
        array('taxonomy_term' => $term->id()));

      $result = \Drupal::l($term->getName(), $url);
      $results[] = render($result);
    }

    $render = array('#markup' => join(', ', $results));

    return render($render);
  }
}
