<?php

/**
 * @file
 * Contains \Drupal\components\ComponentSubscriber.
 */

namespace Drupal\components\EventSubscriber;

use Drupal\Core\Render\RenderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ComponentSubscriber.
 *
 * @package Drupal\components
 */
class ComponentSubscriber implements EventSubscriberInterface {

  // /**
  //  * Constructor.
  //  */
  // public function __construct() {
  //
  // }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[RenderEvents::SELECT_PAGE_DISPLAY_VARIANT][] = array('onSelectPageDisplayVariant');
    return $events;
  }

  /**
   * This method is called whenever the render.page_display_variant.select event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function onSelectPageDisplayVariant(Event $event) {
    $routename = \Drupal::routeMatch()->getRouteName();

    if (!in_array($routename,
      ['entity.node.canonical', 'entity.taxonomy_term.canonical']
    )) {
      return $event;
    }

    $event->setPluginId('component_page');
    //drupal_set_message('Event render.page_display_variant.select thrown by Subscriber in module components.', 'status', TRUE);
    return $event;
  }

}
