<?php

namespace Drupal\components\Plugin\DisplayVariant;

use Drupal\block\BlockRepositoryInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use \Drupal\block\Plugin\DisplayVariant\BlockPageVariant;

/**
 * Provides a page display variant that decorates the main content with blocks.
 *
 * To ensure essential information is displayed, each essential part of a page
 * has a corresponding block plugin interface, so that BlockPageVariant can
 * automatically provide a fallback in case no block for each of these
 * interfaces is placed.
 *
 * @PageDisplayVariant(
 *   id = "component_page",
 *   admin_label = @Translation("Page with components")
 * )
 */
class ComponentPageVariant extends BlockPageVariant {

  /**
   * We need this as a property of the class so we can store the entity.
   */
  protected $entity;

  /**
   * @inheritdoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BlockRepositoryInterface $block_repository, EntityViewBuilderInterface $block_view_builder, array $block_list_cache_tags) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $block_repository, $block_view_builder, $block_list_cache_tags);

    $routename = \Drupal::routeMatch()->getRouteName();
    $entityType = explode('.', $routename)[1];

    // This is the D8 version of menu_get_object();
    $this->entity = \Drupal::routeMatch()->getParameters()->get($entityType);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();

    if ($this->entity->hasField('field_components')) {
      // Entities aren't rendered with entity_view() anymore, they are rendered
      // using a viewBuilder. Each entity has one, and we use the
      // entityManager() to get it. We can pass the entity to the ViewBuilder's
      // view() function.
      $component_view_builder = \Drupal::entityTypeManager()->getViewBuilder('component');

      $counter = 0;

      // Load all region content assigned via the component_reference.
      foreach ($this->entity->get('field_components') as $key => $component) {
        // Check for component entity existing. Sometimes field data is stale.
        if ($componentEntity = $component->get('entity')->getValue()) {
          $region = $component->get('region')->getValue();
          $viewed = $component_view_builder->view($componentEntity);

          $viewed['#ordinality'] = $counter;

          $build[$region][] = $viewed;
          unset($build[$region]['#sorted']);

          $counter++;
        }
      }
    }
    return $build;
  }

}
