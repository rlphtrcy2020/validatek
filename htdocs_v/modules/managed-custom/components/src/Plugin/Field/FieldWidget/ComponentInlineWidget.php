<?php
namespace Drupal\components\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;

/**
 * Plugin implementation of the 'component_inline_widget' widget.
 *
 * @FieldWidget(
 *   id = "component_inline_widget",
 *   label = @Translation("Component Inline Reference"),
 *   field_types = {
 *     "component_reference"
 *   },
 *   multiple_values = true
 * )
 */
class ComponentInlineWidget extends InlineEntityFormComplex {

  /**
   * [prepareFormState description]
   * @param  FormStateInterface     $form_state [description]
   * @param  FieldItemListInterface $items      [description]
   * @return [type]                             [description]
   */
  protected function prepareFormState(FormStateInterface $form_state, FieldItemListInterface $items, $translating = FALSE) {
    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);
    if (empty($widget_state)) {
      $widget_state = [
        'instance' => $this->fieldDefinition,
        'form' => NULL,
        'delete' => [],
        'entities' => [],
      ];
      // Store the $items entities in the widget state, for futher manipulation.
      foreach ($items as $delta => $item) {
        $entity = $item->entity;
        // The $entity can be NULL if the reference is broken.
        if ($entity) {
          $widget_state['entities'][$delta] = [
            'entity' => $entity,
            'weight' => $delta,
            'form' => NULL,
            'needs_save' => $entity->isNew(),
            'region' => $item->get('region')->getValue(),
          ];
        }
      }

      $form_state->set(['inline_entity_form', $this->iefId], $widget_state);
    }
  }

  /**
   * [formElement description]
   * @param  FieldItemListInterface $items      [description]
   * @param  [type]                 $delta      [description]
   * @param  array                  $element    [description]
   * @param  array                  $form       [description]
   * @param  FormStateInterface     $form_state [description]
   * @return [type]                             [description]
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (empty($element['entities'])) {
      return $element;
    }

    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);

    foreach($element['entities'] as $key => &$entity) {

      if (!is_int($key)) {
        continue;
      }

      $entity['region'] = array(
        '#type' => 'select',
        '#title_display' => 'invisible',
        '#title' => $this->t('Region'),
        '#options' => $this->collectAvailableRegions(),
        '#default_value' => empty($widget_state['entities'][$key]['region']) ?
          'default' : $widget_state['entities'][$key]['region'],
        '#weight' => 10,
      );
    }

    $element['entities']['#theme'] = 'component_inline_entity_form_entity_table';
    return $element;
  }

  /**
   * Get current theme and return an array of available regions.
   * @return array of select options
   */
  public function collectAvailableRegions() {
    $themeHandler = \Drupal::service('theme_handler');
    $moduleHandler = \Drupal::moduleHandler();

    $theme = $themeHandler->getDefault();
    $regions = system_region_list($theme);

    $moduleHandler->alter('components_regions', $regions, $this->fieldDefinition);

    return $regions;
  }

  /**
   * @inheritdoc
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $items = array();

    $field_name = $this->fieldDefinition->getName();
    $field_state = $form_state->getValue($field_name);

    if (!empty($field_state['entities'])) {
      $entities = $field_state['entities'];
    } else {
      return $items;
    }

    // Convert form values to actual entity reference values.
    foreach ($values as $key => $value) {
      $item = $value;
      $item['region'] = empty($field_state['entities'][$key]['region']) ? '' :
        $field_state['entities'][$key]['region'];

      if (isset($item['entity'])) {
        $item['target_id'] = $item['entity']->id();
        $items[] = $item;
      }
      else {
        $item['target_id'] = NULL;
        $items[] = $item;
      }
    }

    // Sort items by _weight.
    usort($items, function ($a, $b) {
      return SortArray::sortByKeyInt($a, $b, 'weight');
    });

    return $items;
  }
}
