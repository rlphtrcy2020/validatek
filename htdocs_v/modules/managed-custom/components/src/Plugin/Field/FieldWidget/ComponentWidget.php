<?php

namespace Drupal\components\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'component_widget' widget.
 *
 * @FieldWidget(
 *   id = "component_widget",
 *   label = @Translation("Component Reference"),
 *   field_types = {
 *     "component_reference"
 *   }
 * )
 */
class ComponentWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // $items is the entire list of results, so make sure to use $delta
    // when getting a specific value.
    $region_element = array(
      '#type' => 'select',
      '#title' => $this->t('Region'),
      '#options' => $this->collectAvailableRegions(),
      '#default_value' => $items[$delta]->get('region')->getValue(),
      '#weight' => 10,
    );

    $element['region'] = $region_element;
    return $element;
  }

  /**
   * Get current theme and return an array of available regions.
   *
   * @return array
   *   An array of select options
   */
  public function collectAvailableRegions() {
    $themeHandler = \Drupal::service('theme_handler');
    $moduleHandler = \Drupal::moduleHandler();

    $theme = $themeHandler->getDefault();
    $regions = system_region_list($theme);

    $moduleHandler->alter('components_regions', $regions, $this->fieldDefinition);

    return $regions;
  }
}
