<?php

/**
 * @file
 * Contains \Drupal\components\Plugin\Field\FieldType\ComponentFieldType.
 */

namespace Drupal\components\Plugin\Field\FieldType;

// use Drupal\Component\Utility\Random;
// use Drupal\Core\Field\FieldDefinitionInterface;
// use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
// use Drupal\Core\Form\FormStateInterface;
// use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Plugin implementation of the 'component_reference' field type.
 *
 * @FieldType(
 *   id = "component_reference",
 *   label = @Translation("Component Field Item"),
 *   description = @Translation("My Field Type"),
 *   default_widget = "component_widget",
 *   default_formatter = "entity_reference_entity_view",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class ComponentFieldType extends EntityReferenceItem {

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['region'] = array(
      'description' => 'The region in which to display the entity',
      'type' => 'varchar_ascii',
      'length' => 255,
      'not null' => FALSE,
    );
    return $schema;
  }

  /**
   * [propertyDefinitions description]
   * @param  FieldStorageDefinitionInterface $field_definition [description]
   * @return [type]                                            [description]
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['region'] = DataDefinition::create('string')
      ->setLabel(t('Region'))
      ->setDescription(t('Region the component will live in'));
    return $properties;
  }

  /**
   * [isEmpty description]
   * @return boolean [description]
   */
  public function isEmpty() {
    $isEmpty = parent::isEmpty();
    if (!$isEmpty && $this->region !== NULL) {
      return FALSE;
    }
    return TRUE;
  }
}
