<?php

/**
 * @file
 * Contains \Drupal\components\ComponentTypeInterface.
 */

namespace Drupal\components;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Component type entities.
 */
interface ComponentTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Component Type description.
   *
   * @return string
   *   Description of the Component Type.
   */
  public function getDescription();

}
