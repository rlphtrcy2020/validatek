<?php

/**
 * @file
 * Contains \Drupal\components\Entity\ComponentType.
 */

namespace Drupal\components\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\components\ComponentTypeInterface;

/**
 * Defines the Component type entity.
 *
 * @ConfigEntityType(
 *   id = "component_type",
 *   label = @Translation("Component type"),
 *   handlers = {
 *     "list_builder" = "Drupal\components\ComponentTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\components\Form\ComponentTypeForm",
 *       "edit" = "Drupal\components\Form\ComponentTypeForm",
 *       "delete" = "Drupal\components\Form\ComponentTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\components\ComponentTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "component_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "component",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/component_type/{component_type}",
 *     "add-form" = "/admin/structure/component_type/add",
 *     "edit-form" = "/admin/structure/component_type/{component_type}/edit",
 *     "delete-form" = "/admin/structure/component_type/{component_type}/delete",
 *     "collection" = "/admin/structure/component_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ComponentType extends ConfigEntityBundleBase implements ComponentTypeInterface {
  /**
   * The Component type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Component type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Description of the component
   * @var string
   */
  protected $description;

  /**
   * Getter for the description of a component type.
   */
  public function getDescription() {
    return $this->description;
  }
  
}
