<?php

/**
 * @file
 * Contains \Drupal\components\Form\ComponentDeleteForm.
 */

namespace Drupal\components\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Component entities.
 *
 * @ingroup components
 */
class ComponentDeleteForm extends ContentEntityDeleteForm {

}
