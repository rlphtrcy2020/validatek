<?php

namespace Drupal\components\Form;

use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Generic entity inline form handler.
 */
class ComponentInlineForm extends EntityInlineForm {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = parent::getTableFields($bundles);

    $fields['region'] = [
      'type' => 'region',
      'label' => $this->t('Region'),
      'weight' => 100
    ];

    return $fields;
  }

}
