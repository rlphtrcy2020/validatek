<?php

// @file settings.development.php
// Example database information for Drupal 8 websites.
$databases['default']['default'] = array (
  'database' => 'development_validatek',
  'username' => $_SERVER['OS_USER'],
  'password' => base64_decode($_SERVER['OS_PASSWORD']),
  'prefix' => '',
  'host' => $_SERVER['OS_HOST'],
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

/**
 * Public Files Directory.
 */
$settings['file_public_base_url'] = 'https://' . $_SERVER['HTTP_HOST'] . '/sites/default/files';

/**
 * Trusted Host.
 */
$settings['trusted_host_patterns'] = [
  '^validatek.php7.os.interactive-strategies.com$'
];

// Additional settings.php configuration/overrides for the development environment.
// Private Files.
$settings['file_private_path'] = '/var/www/private_files/validatek';
