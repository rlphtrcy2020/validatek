<?php

$databases['default']['default'] = array (
  'database' => 'bitnami_drupal8',
  'username' => 'bn_drupal',
  'password' => '633357b4f0',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'unix_socket' => '/opt/bitnami/mysql/tmp/mysql.sock',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

/**
 * Trusted Host.
 */
$settings['trusted_host_patterns'] = [
  '^upgrade.validatekvsetlabs.com$',
  '^www.upgrade.validatekvsetlabs.com$',
  '^validatek.com$',
  '^validatekqa.com$',
  '^test.validatek.com$',
  '^www.test.validatek.com$',
  '^www.validatek.com$',
  '^www.validatekqa.com$'
];

$config['recaptcha.settings']['site_key'] = '6LeXFz0UAAAAAKGcfjH9tz57zO_BfJwgIOHyE2ku';
$config['recaptcha.settings']['secret_key'] = '6LeXFz0UAAAAAO8-V9IlEO_IriUhEGMbEBk8nBc_';
