<?php

/* themes/validatek/templates/block/block--validatek-structure-page-header.html.twig */
class __TwigTemplate_6e0054527f8b7873afb8af0a9d80b19e08b14560ae0721bd0a1b81858c8e9f00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 28, "if" => 29);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('block', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 28
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 29
        if ($this->getAttribute(($context["content"] ?? null), "slides", array())) {
            // line 30
            echo "<section class=\"hero-carousel\" data-slick>
  ";
            // line 31
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "slides", array()), "html", null, true));
            echo "
</section>
";
        } else {
            // line 34
            echo "<section class=\"angle-hero\">
  ";
            // line 35
            if ($this->getAttribute(($context["content"] ?? null), "image", array())) {
                // line 36
                echo "    <div class=\"angle-hero-image\" style=\"background-image: url('";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "image", array()), 0, array()), "#markup", array(), "array"), "html", null, true));
                echo "');\"></div>
  ";
            }
            // line 38
            echo "  <div class=\"angle-hero-content\">
    <div class=\"angle-hero-content-inner\">
      ";
            // line 40
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "back_link", array()), "html", null, true));
            echo "

      <h1 class=\"-accent-bar\">";
            // line 42
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "title", array()), "html", null, true));
            echo "</h1>
      ";
            // line 43
            if ($this->getAttribute(($context["content"] ?? null), "text", array())) {
                // line 44
                echo "        ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "text", array()), "html", null, true));
                echo "
      ";
            }
            // line 46
            echo "    </div>
  </div>
  <div class=\"angle-hero-right\"></div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/block/block--validatek-structure-page-header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  93 => 46,  87 => 44,  85 => 43,  81 => 42,  76 => 40,  72 => 38,  66 => 36,  64 => 35,  61 => 34,  55 => 31,  52 => 30,  50 => 29,  44 => 28,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/block/block--validatek-structure-page-header.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/block/block--validatek-structure-page-header.html.twig");
    }
}
