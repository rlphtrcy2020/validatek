<?php

/* themes/validatek/templates/content/node--news-item.html.twig */
class __TwigTemplate_5428cd6337431fa620eb6504a8898ab1eaa0bd20dc11959d9c19f6243b20df4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 74, "if" => 80);
        $filters = array("clean_class" => 75, "replace" => 95);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array('clean_class', 'replace'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 74
        $context["classes"] = array(0 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 75
($context["node"] ?? null), "bundle", array()))), 1 => (( !$this->getAttribute(        // line 76
($context["node"] ?? null), "isPublished", array(), "method")) ? ("node--unpublished") : ("")), 2 => ((        // line 77
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass(($context["view_mode"] ?? null)))) : ("")));
        // line 80
        if (($context["teaser"] ?? null)) {
            // line 81
            echo "  ";
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array())) {
                // line 82
                echo "    <a href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
                echo "\" class=\"image-content-card\">
      <div class=\"image-content-card-image\" style=\"background-image: url('";
                // line 83
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#markup", array(), "array"), "html", null, true));
                echo "');\"></div>
      <div class=\"image-content-card-content\">
        <span class=\"eyebrow\">";
                // line 85
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_news_type", array()), "html", null, true));
                echo "</span>
        <h4 class=\"-accent-bar -sky-blue\">";
                // line 86
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                echo "</h4>
        ";
                // line 87
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "
      </div>
    </a>
  ";
            } else {
                // line 91
                echo "    ";
                if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_news_type", array()), 0, array())) {
                    // line 92
                    echo "      ";
                    $context["news_class"] = ("-" . $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_news_type", array()), 0, array()), "#markup", array(), "array"));
                    // line 93
                    echo "    ";
                }
                // line 94
                echo "    <a href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
                echo "\" class=\"content-card ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["news_class"] ?? null), "html", null, true));
                echo "\">
      <span class=\"eyebrow\">";
                // line 95
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_news_type", array()), 0, array()), "#markup", array(), "array"), array("-" => " ")), "html", null, true));
                echo "</span>
      <h4 class=\"-accent-bar -sky-blue\">";
                // line 96
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                echo "</h4>
      ";
                // line 97
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "
    </a>
  ";
            }
        }
        // line 101
        if (($context["page"] ?? null)) {
            // line 102
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["content"] ?? null), "html", null, true));
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/content/node--news-item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 102,  109 => 101,  102 => 97,  98 => 96,  94 => 95,  87 => 94,  84 => 93,  81 => 92,  78 => 91,  71 => 87,  67 => 86,  63 => 85,  58 => 83,  53 => 82,  50 => 81,  48 => 80,  46 => 77,  45 => 76,  44 => 75,  43 => 74,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/content/node--news-item.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/content/node--news-item.html.twig");
    }
}
