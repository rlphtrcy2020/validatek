<?php

/* themes/validatek/templates/navigation/block--utility-footer-nav.html.twig */
class __TwigTemplate_759ebb9d0dbefc83a06e6d0a5d2ea6823dff07b73f13c7baa403ac41f8cfdfb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 28, "if" => 30);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('block', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 28
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "  <nav class=\"validatek-footer-nav\">
    ";
        // line 30
        if ($this->getAttribute(($context["content"] ?? null), "social", array())) {
            // line 31
            echo "      <div class=\"validatek-footer-nav-social-links\">
        ";
            // line 32
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "social", array()), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 35
        echo "    ";
        if ($this->getAttribute(($context["content"] ?? null), "links", array())) {
            // line 36
            echo "      <div class=\"validatek-footer-nav-links\">
        ";
            // line 37
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "links", array()), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 40
        echo "    <div class=\"validatek-footer-nav-utility\">
      ";
        // line 41
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "contact", array()), "telephone", array()), "html", null, true));
        echo "
      ";
        // line 42
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "contact", array()), "email", array()), "html", null, true));
        echo "
      <a class=\"address\">";
        // line 43
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "contact", array()), "address", array()), "html", null, true));
        echo "</a>
    </div>
  </nav>
";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/navigation/block--utility-footer-nav.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  87 => 43,  83 => 42,  79 => 41,  76 => 40,  70 => 37,  67 => 36,  64 => 35,  58 => 32,  55 => 31,  53 => 30,  50 => 29,  44 => 28,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/navigation/block--utility-footer-nav.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/navigation/block--utility-footer-nav.html.twig");
    }
}
