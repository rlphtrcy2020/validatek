<?php

/* themes/validatek/templates/content/node--person.html.twig */
class __TwigTemplate_a02fd9fc11d429ce4abf6ac45d838a5f09c88f986b6cfa0c062d27ca68975ba4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 74, "if" => 76);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 74
        $context["type"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_person_type", array()), 0, array()), "#markup", array(), "array");
        // line 76
        if (($context["teaser"] ?? null)) {
            // line 77
            echo "  ";
            if ((($context["type"] ?? null) == "leadership")) {
                // line 78
                echo "    <div href=\"#\" class=\"people-card\">
      <div class=\"people-card-image\">
        <img src=\"";
                // line 80
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#markup", array(), "array"), "html", null, true));
                echo "\" alt=\"\">
      </div>
      <h5 class=\"people-card-name\">";
                // line 82
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                echo "</h5>
      <p class=\"people-card-title\">";
                // line 83
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_job_title", array()), "html", null, true));
                echo "</p>
    </div>
  ";
            } else {
                // line 86
                echo "    ";
                if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array())) {
                    // line 87
                    echo "      <a href=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
                    echo "\" class=\"image-content-card\">
        <div class=\"image-content-card-image\" style=\"background-image: url('";
                    // line 88
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#markup", array(), "array"), "html", null, true));
                    echo "');\"></div>
        <div class=\"image-content-card-content\">
          ";
                    // line 90
                    if ((($context["type"] ?? null) == "featured")) {
                        // line 91
                        echo "            <span class=\"eyebrow\">Featured Employee</span>
          ";
                    }
                    // line 93
                    echo "          <h4 class=\"-accent-bar -sky-blue\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h4>
          <p>";
                    // line 94
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_job_title", array()), "html", null, true));
                    echo "</p>
        </div>
      </a>
    ";
                } else {
                    // line 98
                    echo "      <a href=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
                    echo "\" class=\"content-card\">
        ";
                    // line 99
                    if ((($context["type"] ?? null) == "featured")) {
                        // line 100
                        echo "          <span class=\"eyebrow\">Featured Employee</span>
        ";
                    }
                    // line 102
                    echo "        <h4 class=\"-accent-bar -sky-blue\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h4>
        <p>";
                    // line 103
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_job_title", array()), "html", null, true));
                    echo "</p>
      </a>
    ";
                }
                // line 106
                echo "  ";
            }
        }
        // line 108
        if (($context["page"] ?? null)) {
            // line 109
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["content"] ?? null), "html", null, true));
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/content/node--person.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 109,  126 => 108,  122 => 106,  116 => 103,  111 => 102,  107 => 100,  105 => 99,  100 => 98,  93 => 94,  88 => 93,  84 => 91,  82 => 90,  77 => 88,  72 => 87,  69 => 86,  63 => 83,  59 => 82,  54 => 80,  50 => 78,  47 => 77,  45 => 76,  43 => 74,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/content/node--person.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/content/node--person.html.twig");
    }
}
