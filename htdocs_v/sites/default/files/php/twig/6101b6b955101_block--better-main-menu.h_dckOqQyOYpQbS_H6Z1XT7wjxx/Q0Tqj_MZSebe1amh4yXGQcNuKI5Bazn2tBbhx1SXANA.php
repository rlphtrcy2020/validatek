<?php

/* themes/validatek/templates/navigation/block--better-main-menu.html.twig */
class __TwigTemplate_c2c11dd6542855f59654e3d6610789de4b8595a794157e4affd264485ed2a782 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 28, "if" => 37);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('block', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 28
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "  <a class=\"js-validatek-header-mobile-menu-trigger\" href=\"#\">Menu
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </a>
  <nav class=\"validatek-header-desktop-nav js-validatek-header-desktop-menu\" role=\"navigation\">
    ";
        // line 36
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "menu", array()), "html", null, true));
        echo "
    ";
        // line 37
        if ($this->getAttribute(($context["content"] ?? null), "search", array())) {
            // line 38
            echo "      <a href=\"#\" class=\"validatek-header-desktop-menu-search js-search-open\">Search</a>
      <div class=\"js-search-box\" role=\"search\">
        ";
            // line 40
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "search", array()), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 43
        echo "    ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "get_in_touch", array()), "html", null, true));
        echo "
  </nav>
";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/navigation/block--better-main-menu.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  75 => 43,  69 => 40,  65 => 38,  63 => 37,  59 => 36,  50 => 29,  44 => 28,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/navigation/block--better-main-menu.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/navigation/block--better-main-menu.html.twig");
    }
}
