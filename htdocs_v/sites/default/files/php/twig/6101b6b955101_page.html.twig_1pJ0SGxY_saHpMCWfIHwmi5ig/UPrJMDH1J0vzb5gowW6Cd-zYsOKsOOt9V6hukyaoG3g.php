<?php

/* themes/validatek/templates/layout/page.html.twig */
class __TwigTemplate_f5505614f5fdf5e02bfce3510aec932c1249175b295f8fefbfd3ce364249c7d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 58);
        $filters = array("without" => 81);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 45
        echo "<div class=\"js-mobile-nav\">
  <div class=\"js-mobile-nav-inner\"></div>
</div>
<div class=\"page-container\">

  <header class=\"validatek-header\">
    <div class=\"validatek-header-inner\">
      ";
        // line 52
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
        echo "
    </div>
  </header>

  <main role=\"main\">
    ";
        // line 57
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "pre_content", array()), "html", null, true));
        echo "
    ";
        // line 58
        if (((($context["use_content_grid"] ?? null) || ($context["use_services_grid"] ?? null)) || ($context["use_people_grid"] ?? null))) {
            // line 59
            echo "      ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
    ";
        } else {
            // line 61
            echo "      <section class=\"generic-content-panel\">
        <div class=\"generic-content-panel-inner\">
          <div class=\"generic-content-panel-content wysiwyg\">
            ";
            // line 64
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
          </div>

        ";
            // line 67
            if ($this->getAttribute(($context["page"] ?? null), "right_sidebar", array())) {
                // line 68
                echo "          <aside class=\"generic-content-panel-side-rail\">
            ";
                // line 69
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "right_sidebar", array()), "html", null, true));
                echo "
          </aside>
        ";
            }
            // line 72
            echo "        </div>
      </section>
    ";
        }
        // line 75
        echo "
    ";
        // line 76
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "post_content", array()), "html", null, true));
        echo "
  </main>

  ";
        // line 79
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "footer", array()), "validatekfootercertifications", array()), "html", null, true));
        echo "
  <footer class=\"validatek-footer\">
    ";
        // line 81
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_without($this->getAttribute(($context["page"] ?? null), "footer", array()), "validatekfootercertifications"), "html", null, true));
        echo "
  </footer>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 81,  108 => 79,  102 => 76,  99 => 75,  94 => 72,  88 => 69,  85 => 68,  83 => 67,  77 => 64,  72 => 61,  66 => 59,  64 => 58,  60 => 57,  52 => 52,  43 => 45,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/layout/page.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/layout/page.html.twig");
    }
}
