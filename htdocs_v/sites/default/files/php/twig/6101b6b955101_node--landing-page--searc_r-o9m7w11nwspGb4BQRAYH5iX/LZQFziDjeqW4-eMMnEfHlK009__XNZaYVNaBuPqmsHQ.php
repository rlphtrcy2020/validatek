<?php

/* themes/validatek/templates/content/node--landing-page--search--full.html.twig */
class __TwigTemplate_6d37cb96e2875a3bff600308c9742038d8f48121b80e9ff4f91a42753f562caf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 73
        echo "<div class=\"search-results-form\">
  <input type=\"search\" placeholder=\"Search by keyword...\" class=\"search-field\" />
  <button type=\"submit\" class=\"search-submit -plain\">Search</button>

  <div class=\"search-results-stats\"></div>
</div>
<section class=\"search-results-listing\">
  <div class='js-search-content-loading'>Loading...</div>
  <div class='search-landing-content'></div>
  <div class='js-search-landing-content-no-results' style='display: none'>
    Sorry, no results were found.
  </div>

  <div class=\"pagination-container\">
    <div class=\"pagination-actions\">
      <a href=\"#\" class=\"prev\">Previous</a>
      <a href=\"#\" class=\"next\">Next</a>
    </div>

    <ul class=\"pagination\"></ul>
  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/content/node--landing-page--search--full.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 73,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/content/node--landing-page--search--full.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/content/node--landing-page--search--full.html.twig");
    }
}
