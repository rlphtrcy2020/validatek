<?php

/* themes/validatek/templates/components/component--content-reference.html.twig */
class __TwigTemplate_11164fbfc8603c6533a4edd3dada9574c517d4525f03517d3f51b72ba62fe462 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 21);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 20
        echo "<section class=\"content-card-grid ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", array()), 0, array()), "#markup", array(), "array"), "html", null, true));
        echo "\">
  ";
        // line 21
        if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_title", array()), 0, array())) {
            // line 22
            echo "    <h2 class=\"-accent-bar -centered\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_title", array()), "html", null, true));
            echo "</h2>
  ";
        }
        // line 24
        echo "  <div class=\"content-card-grid-inner\">
    ";
        // line 25
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_content", array()), "html", null, true));
        echo "
  </div>
  ";
        // line 27
        if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array())) {
            // line 28
            echo "    <div class=\"content-card-grid-cta\">
      ";
            // line 29
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_link", array()), "html", null, true));
            echo "
    </div>
  ";
        }
        // line 32
        echo "</section>
";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/components/component--content-reference.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 32,  69 => 29,  66 => 28,  64 => 27,  59 => 25,  56 => 24,  50 => 22,  48 => 21,  43 => 20,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/components/component--content-reference.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/components/component--content-reference.html.twig");
    }
}
