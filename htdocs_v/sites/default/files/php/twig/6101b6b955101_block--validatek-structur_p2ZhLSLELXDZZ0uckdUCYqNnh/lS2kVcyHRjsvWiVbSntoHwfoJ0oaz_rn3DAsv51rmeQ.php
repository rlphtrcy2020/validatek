<?php

/* themes/validatek/templates/block/block--validatek-structure-footer-certifications.html.twig */
class __TwigTemplate_e59e03415d3db35eb591e8d54361c3601bc5f940f2c57f4b25bf9be52e6fce8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 30, "if" => 31);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('block', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 28
        echo "<div class=\"footer-logos\">
  <div class=\"footer-logos-inner\">
    ";
        // line 30
        $this->displayBlock('content', $context, $blocks);
        // line 42
        echo "  </div>
</div>
";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "      ";
        if ($this->getAttribute(($context["content"] ?? null), "cmmi", array())) {
            // line 32
            echo "        <div class=\"cmmi\">
          ";
            // line 33
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "cmmi", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 36
        echo "      ";
        if ($this->getAttribute(($context["content"] ?? null), "sri", array())) {
            // line 37
            echo "        <div class=\"sri\">
          ";
            // line 38
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "sri", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 41
        echo "    ";
    }

    public function getTemplateName()
    {
        return "themes/validatek/templates/block/block--validatek-structure-footer-certifications.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  83 => 41,  77 => 38,  74 => 37,  71 => 36,  65 => 33,  62 => 32,  59 => 31,  56 => 30,  50 => 42,  48 => 30,  44 => 28,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/validatek/templates/block/block--validatek-structure-footer-certifications.html.twig", "/opt/bitnami/apps/drupal/htdocs/themes/validatek/templates/block/block--validatek-structure-footer-certifications.html.twig");
    }
}
