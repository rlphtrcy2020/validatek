/*!
  * domready (c) Dustin Diaz 2014 - License MIT
  */
!function(e,t){typeof module!="undefined"?module.exports=t():typeof define=="function"&&typeof define.amd=="object"?define(t):this[e]=t()}("domready",function(){var e=[],t,n=document,r=n.documentElement.doScroll,i="DOMContentLoaded",s=(r?/^loaded|^c/:/^loaded|^i|^c/).test(n.readyState);return s||n.addEventListener(i,t=function(){n.removeEventListener(i,t),s=1;while(t=e.shift())t()}),function(t){s?setTimeout(t,0):e.push(t)}});
/*! jQuery v3.2.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(a,b){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){"use strict";var c=[],d=a.document,e=Object.getPrototypeOf,f=c.slice,g=c.concat,h=c.push,i=c.indexOf,j={},k=j.toString,l=j.hasOwnProperty,m=l.toString,n=m.call(Object),o={};function p(a,b){b=b||d;var c=b.createElement("script");c.text=a,b.head.appendChild(c).parentNode.removeChild(c)}var q="3.2.1",r=function(a,b){return new r.fn.init(a,b)},s=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,t=/^-ms-/,u=/-([a-z])/g,v=function(a,b){return b.toUpperCase()};r.fn=r.prototype={jquery:q,constructor:r,length:0,toArray:function(){return f.call(this)},get:function(a){return null==a?f.call(this):a<0?this[a+this.length]:this[a]},pushStack:function(a){var b=r.merge(this.constructor(),a);return b.prevObject=this,b},each:function(a){return r.each(this,a)},map:function(a){return this.pushStack(r.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(f.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(a<0?b:0);return this.pushStack(c>=0&&c<b?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:h,sort:c.sort,splice:c.splice},r.extend=r.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||r.isFunction(g)||(g={}),h===i&&(g=this,h--);h<i;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(r.isPlainObject(d)||(e=Array.isArray(d)))?(e?(e=!1,f=c&&Array.isArray(c)?c:[]):f=c&&r.isPlainObject(c)?c:{},g[b]=r.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},r.extend({expando:"jQuery"+(q+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===r.type(a)},isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){var b=r.type(a);return("number"===b||"string"===b)&&!isNaN(a-parseFloat(a))},isPlainObject:function(a){var b,c;return!(!a||"[object Object]"!==k.call(a))&&(!(b=e(a))||(c=l.call(b,"constructor")&&b.constructor,"function"==typeof c&&m.call(c)===n))},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?j[k.call(a)]||"object":typeof a},globalEval:function(a){p(a)},camelCase:function(a){return a.replace(t,"ms-").replace(u,v)},each:function(a,b){var c,d=0;if(w(a)){for(c=a.length;d<c;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(s,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(w(Object(a))?r.merge(c,"string"==typeof a?[a]:a):h.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:i.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;d<c;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;f<g;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,f=0,h=[];if(w(a))for(d=a.length;f<d;f++)e=b(a[f],f,c),null!=e&&h.push(e);else for(f in a)e=b(a[f],f,c),null!=e&&h.push(e);return g.apply([],h)},guid:1,proxy:function(a,b){var c,d,e;if("string"==typeof b&&(c=a[b],b=a,a=c),r.isFunction(a))return d=f.call(arguments,2),e=function(){return a.apply(b||this,d.concat(f.call(arguments)))},e.guid=a.guid=a.guid||r.guid++,e},now:Date.now,support:o}),"function"==typeof Symbol&&(r.fn[Symbol.iterator]=c[Symbol.iterator]),r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){j["[object "+b+"]"]=b.toLowerCase()});function w(a){var b=!!a&&"length"in a&&a.length,c=r.type(a);return"function"!==c&&!r.isWindow(a)&&("array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a)}var x=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ha(),z=ha(),A=ha(),B=function(a,b){return a===b&&(l=!0),0},C={}.hasOwnProperty,D=[],E=D.pop,F=D.push,G=D.push,H=D.slice,I=function(a,b){for(var c=0,d=a.length;c<d;c++)if(a[c]===b)return c;return-1},J="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",K="[\\x20\\t\\r\\n\\f]",L="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",M="\\["+K+"*("+L+")(?:"+K+"*([*^$|!~]?=)"+K+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+L+"))|)"+K+"*\\]",N=":("+L+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+M+")*)|.*)\\)|)",O=new RegExp(K+"+","g"),P=new RegExp("^"+K+"+|((?:^|[^\\\\])(?:\\\\.)*)"+K+"+$","g"),Q=new RegExp("^"+K+"*,"+K+"*"),R=new RegExp("^"+K+"*([>+~]|"+K+")"+K+"*"),S=new RegExp("="+K+"*([^\\]'\"]*?)"+K+"*\\]","g"),T=new RegExp(N),U=new RegExp("^"+L+"$"),V={ID:new RegExp("^#("+L+")"),CLASS:new RegExp("^\\.("+L+")"),TAG:new RegExp("^("+L+"|[*])"),ATTR:new RegExp("^"+M),PSEUDO:new RegExp("^"+N),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+K+"*(even|odd|(([+-]|)(\\d*)n|)"+K+"*(?:([+-]|)"+K+"*(\\d+)|))"+K+"*\\)|)","i"),bool:new RegExp("^(?:"+J+")$","i"),needsContext:new RegExp("^"+K+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+K+"*((?:-\\d)?\\d*)"+K+"*\\)|)(?=[^-]|$)","i")},W=/^(?:input|select|textarea|button)$/i,X=/^h\d$/i,Y=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,$=/[+~]/,_=new RegExp("\\\\([\\da-f]{1,6}"+K+"?|("+K+")|.)","ig"),aa=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:d<0?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},ba=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ca=function(a,b){return b?"\0"===a?"\ufffd":a.slice(0,-1)+"\\"+a.charCodeAt(a.length-1).toString(16)+" ":"\\"+a},da=function(){m()},ea=ta(function(a){return a.disabled===!0&&("form"in a||"label"in a)},{dir:"parentNode",next:"legend"});try{G.apply(D=H.call(v.childNodes),v.childNodes),D[v.childNodes.length].nodeType}catch(fa){G={apply:D.length?function(a,b){F.apply(a,H.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function ga(a,b,d,e){var f,h,j,k,l,o,r,s=b&&b.ownerDocument,w=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==w&&9!==w&&11!==w)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==w&&(l=Z.exec(a)))if(f=l[1]){if(9===w){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(s&&(j=s.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(l[2])return G.apply(d,b.getElementsByTagName(a)),d;if((f=l[3])&&c.getElementsByClassName&&b.getElementsByClassName)return G.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==w)s=b,r=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(ba,ca):b.setAttribute("id",k=u),o=g(a),h=o.length;while(h--)o[h]="#"+k+" "+sa(o[h]);r=o.join(","),s=$.test(a)&&qa(b.parentNode)||b}if(r)try{return G.apply(d,s.querySelectorAll(r)),d}catch(x){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(P,"$1"),b,d,e)}function ha(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ia(a){return a[u]=!0,a}function ja(a){var b=n.createElement("fieldset");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ka(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function la(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&a.sourceIndex-b.sourceIndex;if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function na(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function oa(a){return function(b){return"form"in b?b.parentNode&&b.disabled===!1?"label"in b?"label"in b.parentNode?b.parentNode.disabled===a:b.disabled===a:b.isDisabled===a||b.isDisabled!==!a&&ea(b)===a:b.disabled===a:"label"in b&&b.disabled===a}}function pa(a){return ia(function(b){return b=+b,ia(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function qa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=ga.support={},f=ga.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return!!b&&"HTML"!==b.nodeName},m=ga.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),v!==n&&(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ja(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ja(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Y.test(n.getElementsByClassName),c.getById=ja(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.filter.ID=function(a){var b=a.replace(_,aa);return function(a){return a.getAttribute("id")===b}},d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}}):(d.filter.ID=function(a){var b=a.replace(_,aa);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}},d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c,d,e,f=b.getElementById(a);if(f){if(c=f.getAttributeNode("id"),c&&c.value===a)return[f];e=b.getElementsByName(a),d=0;while(f=e[d++])if(c=f.getAttributeNode("id"),c&&c.value===a)return[f]}return[]}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){if("undefined"!=typeof b.getElementsByClassName&&p)return b.getElementsByClassName(a)},r=[],q=[],(c.qsa=Y.test(n.querySelectorAll))&&(ja(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+K+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+K+"*(?:value|"+J+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ja(function(a){a.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+K+"*[*^$|!~]?="),2!==a.querySelectorAll(":enabled").length&&q.push(":enabled",":disabled"),o.appendChild(a).disabled=!0,2!==a.querySelectorAll(":disabled").length&&q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Y.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ja(function(a){c.disconnectedMatch=s.call(a,"*"),s.call(a,"[s!='']:x"),r.push("!=",N)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Y.test(o.compareDocumentPosition),t=b||Y.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?I(k,a)-I(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?I(k,a)-I(k,b):0;if(e===f)return la(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?la(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},ga.matches=function(a,b){return ga(a,null,null,b)},ga.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(S,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return ga(b,n,null,[a]).length>0},ga.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},ga.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&C.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},ga.escape=function(a){return(a+"").replace(ba,ca)},ga.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},ga.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=ga.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=ga.selectors={cacheLength:50,createPseudo:ia,match:V,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(_,aa),a[3]=(a[3]||a[4]||a[5]||"").replace(_,aa),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||ga.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&ga.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return V.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&T.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(_,aa).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+K+")"+a+"("+K+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=ga.attr(d,a);return null==e?"!="===b:!b||(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(O," ")+" ").indexOf(c)>-1:"|="===b&&(e===c||e.slice(0,c.length+1)===c+"-"))}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||ga.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ia(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=I(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ia(function(a){var b=[],c=[],d=h(a.replace(P,"$1"));return d[u]?ia(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ia(function(a){return function(b){return ga(a,b).length>0}}),contains:ia(function(a){return a=a.replace(_,aa),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ia(function(a){return U.test(a||"")||ga.error("unsupported lang: "+a),a=a.replace(_,aa).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:oa(!1),disabled:oa(!0),checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return X.test(a.nodeName)},input:function(a){return W.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:pa(function(){return[0]}),last:pa(function(a,b){return[b-1]}),eq:pa(function(a,b,c){return[c<0?c+b:c]}),even:pa(function(a,b){for(var c=0;c<b;c+=2)a.push(c);return a}),odd:pa(function(a,b){for(var c=1;c<b;c+=2)a.push(c);return a}),lt:pa(function(a,b,c){for(var d=c<0?c+b:c;--d>=0;)a.push(d);return a}),gt:pa(function(a,b,c){for(var d=c<0?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=ma(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=na(b);function ra(){}ra.prototype=d.filters=d.pseudos,d.setFilters=new ra,g=ga.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=Q.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=R.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(P," ")}),h=h.slice(c.length));for(g in d.filter)!(e=V[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?ga.error(a):z(a,i).slice(0)};function sa(a){for(var b=0,c=a.length,d="";b<c;b++)d+=a[b].value;return d}function ta(a,b,c){var d=b.dir,e=b.next,f=e||d,g=c&&"parentNode"===f,h=x++;return b.first?function(b,c,e){while(b=b[d])if(1===b.nodeType||g)return a(b,c,e);return!1}:function(b,c,i){var j,k,l,m=[w,h];if(i){while(b=b[d])if((1===b.nodeType||g)&&a(b,c,i))return!0}else while(b=b[d])if(1===b.nodeType||g)if(l=b[u]||(b[u]={}),k=l[b.uniqueID]||(l[b.uniqueID]={}),e&&e===b.nodeName.toLowerCase())b=b[d]||b;else{if((j=k[f])&&j[0]===w&&j[1]===h)return m[2]=j[2];if(k[f]=m,m[2]=a(b,c,i))return!0}return!1}}function ua(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function va(a,b,c){for(var d=0,e=b.length;d<e;d++)ga(a,b[d],c);return c}function wa(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;h<i;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function xa(a,b,c,d,e,f){return d&&!d[u]&&(d=xa(d)),e&&!e[u]&&(e=xa(e,f)),ia(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||va(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:wa(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=wa(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?I(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=wa(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):G.apply(g,r)})}function ya(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ta(function(a){return a===b},h,!0),l=ta(function(a){return I(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];i<f;i++)if(c=d.relative[a[i].type])m=[ta(ua(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;e<f;e++)if(d.relative[a[e].type])break;return xa(i>1&&ua(m),i>1&&sa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(P,"$1"),c,i<e&&ya(a.slice(i,e)),e<f&&ya(a=a.slice(e)),e<f&&sa(a))}m.push(c)}return ua(m)}function za(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=E.call(i));u=wa(u)}G.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&ga.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ia(f):f}return h=ga.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=ya(b[c]),f[u]?d.push(f):e.push(f);f=A(a,za(e,d)),f.selector=a}return f},i=ga.select=function(a,b,c,e){var f,i,j,k,l,m="function"==typeof a&&a,n=!e&&g(a=m.selector||a);if(c=c||[],1===n.length){if(i=n[0]=n[0].slice(0),i.length>2&&"ID"===(j=i[0]).type&&9===b.nodeType&&p&&d.relative[i[1].type]){if(b=(d.find.ID(j.matches[0].replace(_,aa),b)||[])[0],!b)return c;m&&(b=b.parentNode),a=a.slice(i.shift().value.length)}f=V.needsContext.test(a)?0:i.length;while(f--){if(j=i[f],d.relative[k=j.type])break;if((l=d.find[k])&&(e=l(j.matches[0].replace(_,aa),$.test(i[0].type)&&qa(b.parentNode)||b))){if(i.splice(f,1),a=e.length&&sa(i),!a)return G.apply(c,e),c;break}}}return(m||h(a,n))(e,b,!p,c,!b||$.test(a)&&qa(b.parentNode)||b),c},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ja(function(a){return 1&a.compareDocumentPosition(n.createElement("fieldset"))}),ja(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ka("type|href|height|width",function(a,b,c){if(!c)return a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ja(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ka("value",function(a,b,c){if(!c&&"input"===a.nodeName.toLowerCase())return a.defaultValue}),ja(function(a){return null==a.getAttribute("disabled")})||ka(J,function(a,b,c){var d;if(!c)return a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),ga}(a);r.find=x,r.expr=x.selectors,r.expr[":"]=r.expr.pseudos,r.uniqueSort=r.unique=x.uniqueSort,r.text=x.getText,r.isXMLDoc=x.isXML,r.contains=x.contains,r.escapeSelector=x.escape;var y=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&r(a).is(c))break;d.push(a)}return d},z=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},A=r.expr.match.needsContext;function B(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()}var C=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,D=/^.[^:#\[\.,]*$/;function E(a,b,c){return r.isFunction(b)?r.grep(a,function(a,d){return!!b.call(a,d,a)!==c}):b.nodeType?r.grep(a,function(a){return a===b!==c}):"string"!=typeof b?r.grep(a,function(a){return i.call(b,a)>-1!==c}):D.test(b)?r.filter(b,a,c):(b=r.filter(b,a),r.grep(a,function(a){return i.call(b,a)>-1!==c&&1===a.nodeType}))}r.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?r.find.matchesSelector(d,a)?[d]:[]:r.find.matches(a,r.grep(b,function(a){return 1===a.nodeType}))},r.fn.extend({find:function(a){var b,c,d=this.length,e=this;if("string"!=typeof a)return this.pushStack(r(a).filter(function(){for(b=0;b<d;b++)if(r.contains(e[b],this))return!0}));for(c=this.pushStack([]),b=0;b<d;b++)r.find(a,e[b],c);return d>1?r.uniqueSort(c):c},filter:function(a){return this.pushStack(E(this,a||[],!1))},not:function(a){return this.pushStack(E(this,a||[],!0))},is:function(a){return!!E(this,"string"==typeof a&&A.test(a)?r(a):a||[],!1).length}});var F,G=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,H=r.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||F,"string"==typeof a){if(e="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:G.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof r?b[0]:b,r.merge(this,r.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),C.test(e[1])&&r.isPlainObject(b))for(e in b)r.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}return f=d.getElementById(e[2]),f&&(this[0]=f,this.length=1),this}return a.nodeType?(this[0]=a,this.length=1,this):r.isFunction(a)?void 0!==c.ready?c.ready(a):a(r):r.makeArray(a,this)};H.prototype=r.fn,F=r(d);var I=/^(?:parents|prev(?:Until|All))/,J={children:!0,contents:!0,next:!0,prev:!0};r.fn.extend({has:function(a){var b=r(a,this),c=b.length;return this.filter(function(){for(var a=0;a<c;a++)if(r.contains(this,b[a]))return!0})},closest:function(a,b){var c,d=0,e=this.length,f=[],g="string"!=typeof a&&r(a);if(!A.test(a))for(;d<e;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&r.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?r.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?i.call(r(a),this[0]):i.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(r.uniqueSort(r.merge(this.get(),r(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function K(a,b){while((a=a[b])&&1!==a.nodeType);return a}r.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return y(a,"parentNode")},parentsUntil:function(a,b,c){return y(a,"parentNode",c)},next:function(a){return K(a,"nextSibling")},prev:function(a){return K(a,"previousSibling")},nextAll:function(a){return y(a,"nextSibling")},prevAll:function(a){return y(a,"previousSibling")},nextUntil:function(a,b,c){return y(a,"nextSibling",c)},prevUntil:function(a,b,c){return y(a,"previousSibling",c)},siblings:function(a){return z((a.parentNode||{}).firstChild,a)},children:function(a){return z(a.firstChild)},contents:function(a){return B(a,"iframe")?a.contentDocument:(B(a,"template")&&(a=a.content||a),r.merge([],a.childNodes))}},function(a,b){r.fn[a]=function(c,d){var e=r.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=r.filter(d,e)),this.length>1&&(J[a]||r.uniqueSort(e),I.test(a)&&e.reverse()),this.pushStack(e)}});var L=/[^\x20\t\r\n\f]+/g;function M(a){var b={};return r.each(a.match(L)||[],function(a,c){b[c]=!0}),b}r.Callbacks=function(a){a="string"==typeof a?M(a):r.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=e||a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){r.each(b,function(b,c){r.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==r.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return r.each(arguments,function(a,b){var c;while((c=r.inArray(b,f,c))>-1)f.splice(c,1),c<=h&&h--}),this},has:function(a){return a?r.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=g=[],c||b||(f=c=""),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j};function N(a){return a}function O(a){throw a}function P(a,b,c,d){var e;try{a&&r.isFunction(e=a.promise)?e.call(a).done(b).fail(c):a&&r.isFunction(e=a.then)?e.call(a,b,c):b.apply(void 0,[a].slice(d))}catch(a){c.apply(void 0,[a])}}r.extend({Deferred:function(b){var c=[["notify","progress",r.Callbacks("memory"),r.Callbacks("memory"),2],["resolve","done",r.Callbacks("once memory"),r.Callbacks("once memory"),0,"resolved"],["reject","fail",r.Callbacks("once memory"),r.Callbacks("once memory"),1,"rejected"]],d="pending",e={state:function(){return d},always:function(){return f.done(arguments).fail(arguments),this},"catch":function(a){return e.then(null,a)},pipe:function(){var a=arguments;return r.Deferred(function(b){r.each(c,function(c,d){var e=r.isFunction(a[d[4]])&&a[d[4]];f[d[1]](function(){var a=e&&e.apply(this,arguments);a&&r.isFunction(a.promise)?a.promise().progress(b.notify).done(b.resolve).fail(b.reject):b[d[0]+"With"](this,e?[a]:arguments)})}),a=null}).promise()},then:function(b,d,e){var f=0;function g(b,c,d,e){return function(){var h=this,i=arguments,j=function(){var a,j;if(!(b<f)){if(a=d.apply(h,i),a===c.promise())throw new TypeError("Thenable self-resolution");j=a&&("object"==typeof a||"function"==typeof a)&&a.then,r.isFunction(j)?e?j.call(a,g(f,c,N,e),g(f,c,O,e)):(f++,j.call(a,g(f,c,N,e),g(f,c,O,e),g(f,c,N,c.notifyWith))):(d!==N&&(h=void 0,i=[a]),(e||c.resolveWith)(h,i))}},k=e?j:function(){try{j()}catch(a){r.Deferred.exceptionHook&&r.Deferred.exceptionHook(a,k.stackTrace),b+1>=f&&(d!==O&&(h=void 0,i=[a]),c.rejectWith(h,i))}};b?k():(r.Deferred.getStackHook&&(k.stackTrace=r.Deferred.getStackHook()),a.setTimeout(k))}}return r.Deferred(function(a){c[0][3].add(g(0,a,r.isFunction(e)?e:N,a.notifyWith)),c[1][3].add(g(0,a,r.isFunction(b)?b:N)),c[2][3].add(g(0,a,r.isFunction(d)?d:O))}).promise()},promise:function(a){return null!=a?r.extend(a,e):e}},f={};return r.each(c,function(a,b){var g=b[2],h=b[5];e[b[1]]=g.add,h&&g.add(function(){d=h},c[3-a][2].disable,c[0][2].lock),g.add(b[3].fire),f[b[0]]=function(){return f[b[0]+"With"](this===f?void 0:this,arguments),this},f[b[0]+"With"]=g.fireWith}),e.promise(f),b&&b.call(f,f),f},when:function(a){var b=arguments.length,c=b,d=Array(c),e=f.call(arguments),g=r.Deferred(),h=function(a){return function(c){d[a]=this,e[a]=arguments.length>1?f.call(arguments):c,--b||g.resolveWith(d,e)}};if(b<=1&&(P(a,g.done(h(c)).resolve,g.reject,!b),"pending"===g.state()||r.isFunction(e[c]&&e[c].then)))return g.then();while(c--)P(e[c],h(c),g.reject);return g.promise()}});var Q=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;r.Deferred.exceptionHook=function(b,c){a.console&&a.console.warn&&b&&Q.test(b.name)&&a.console.warn("jQuery.Deferred exception: "+b.message,b.stack,c)},r.readyException=function(b){a.setTimeout(function(){throw b})};var R=r.Deferred();r.fn.ready=function(a){return R.then(a)["catch"](function(a){r.readyException(a)}),this},r.extend({isReady:!1,readyWait:1,ready:function(a){(a===!0?--r.readyWait:r.isReady)||(r.isReady=!0,a!==!0&&--r.readyWait>0||R.resolveWith(d,[r]))}}),r.ready.then=R.then;function S(){d.removeEventListener("DOMContentLoaded",S),
a.removeEventListener("load",S),r.ready()}"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll?a.setTimeout(r.ready):(d.addEventListener("DOMContentLoaded",S),a.addEventListener("load",S));var T=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===r.type(c)){e=!0;for(h in c)T(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,r.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(r(a),c)})),b))for(;h<i;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},U=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function V(){this.expando=r.expando+V.uid++}V.uid=1,V.prototype={cache:function(a){var b=a[this.expando];return b||(b={},U(a)&&(a.nodeType?a[this.expando]=b:Object.defineProperty(a,this.expando,{value:b,configurable:!0}))),b},set:function(a,b,c){var d,e=this.cache(a);if("string"==typeof b)e[r.camelCase(b)]=c;else for(d in b)e[r.camelCase(d)]=b[d];return e},get:function(a,b){return void 0===b?this.cache(a):a[this.expando]&&a[this.expando][r.camelCase(b)]},access:function(a,b,c){return void 0===b||b&&"string"==typeof b&&void 0===c?this.get(a,b):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d=a[this.expando];if(void 0!==d){if(void 0!==b){Array.isArray(b)?b=b.map(r.camelCase):(b=r.camelCase(b),b=b in d?[b]:b.match(L)||[]),c=b.length;while(c--)delete d[b[c]]}(void 0===b||r.isEmptyObject(d))&&(a.nodeType?a[this.expando]=void 0:delete a[this.expando])}},hasData:function(a){var b=a[this.expando];return void 0!==b&&!r.isEmptyObject(b)}};var W=new V,X=new V,Y=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Z=/[A-Z]/g;function $(a){return"true"===a||"false"!==a&&("null"===a?null:a===+a+""?+a:Y.test(a)?JSON.parse(a):a)}function _(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(Z,"-$&").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c=$(c)}catch(e){}X.set(a,b,c)}else c=void 0;return c}r.extend({hasData:function(a){return X.hasData(a)||W.hasData(a)},data:function(a,b,c){return X.access(a,b,c)},removeData:function(a,b){X.remove(a,b)},_data:function(a,b,c){return W.access(a,b,c)},_removeData:function(a,b){W.remove(a,b)}}),r.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=X.get(f),1===f.nodeType&&!W.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=r.camelCase(d.slice(5)),_(f,d,e[d])));W.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){X.set(this,a)}):T(this,function(b){var c;if(f&&void 0===b){if(c=X.get(f,a),void 0!==c)return c;if(c=_(f,a),void 0!==c)return c}else this.each(function(){X.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){X.remove(this,a)})}}),r.extend({queue:function(a,b,c){var d;if(a)return b=(b||"fx")+"queue",d=W.get(a,b),c&&(!d||Array.isArray(c)?d=W.access(a,b,r.makeArray(c)):d.push(c)),d||[]},dequeue:function(a,b){b=b||"fx";var c=r.queue(a,b),d=c.length,e=c.shift(),f=r._queueHooks(a,b),g=function(){r.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return W.get(a,c)||W.access(a,c,{empty:r.Callbacks("once memory").add(function(){W.remove(a,[b+"queue",c])})})}}),r.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?r.queue(this[0],a):void 0===b?this:this.each(function(){var c=r.queue(this,a,b);r._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&r.dequeue(this,a)})},dequeue:function(a){return this.each(function(){r.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=r.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=W.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var aa=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ba=new RegExp("^(?:([+-])=|)("+aa+")([a-z%]*)$","i"),ca=["Top","Right","Bottom","Left"],da=function(a,b){return a=b||a,"none"===a.style.display||""===a.style.display&&r.contains(a.ownerDocument,a)&&"none"===r.css(a,"display")},ea=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};function fa(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return r.css(a,b,"")},i=h(),j=c&&c[3]||(r.cssNumber[b]?"":"px"),k=(r.cssNumber[b]||"px"!==j&&+i)&&ba.exec(r.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,r.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var ga={};function ha(a){var b,c=a.ownerDocument,d=a.nodeName,e=ga[d];return e?e:(b=c.body.appendChild(c.createElement(d)),e=r.css(b,"display"),b.parentNode.removeChild(b),"none"===e&&(e="block"),ga[d]=e,e)}function ia(a,b){for(var c,d,e=[],f=0,g=a.length;f<g;f++)d=a[f],d.style&&(c=d.style.display,b?("none"===c&&(e[f]=W.get(d,"display")||null,e[f]||(d.style.display="")),""===d.style.display&&da(d)&&(e[f]=ha(d))):"none"!==c&&(e[f]="none",W.set(d,"display",c)));for(f=0;f<g;f++)null!=e[f]&&(a[f].style.display=e[f]);return a}r.fn.extend({show:function(){return ia(this,!0)},hide:function(){return ia(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){da(this)?r(this).show():r(this).hide()})}});var ja=/^(?:checkbox|radio)$/i,ka=/<([a-z][^\/\0>\x20\t\r\n\f]+)/i,la=/^$|\/(?:java|ecma)script/i,ma={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ma.optgroup=ma.option,ma.tbody=ma.tfoot=ma.colgroup=ma.caption=ma.thead,ma.th=ma.td;function na(a,b){var c;return c="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):[],void 0===b||b&&B(a,b)?r.merge([a],c):c}function oa(a,b){for(var c=0,d=a.length;c<d;c++)W.set(a[c],"globalEval",!b||W.get(b[c],"globalEval"))}var pa=/<|&#?\w+;/;function qa(a,b,c,d,e){for(var f,g,h,i,j,k,l=b.createDocumentFragment(),m=[],n=0,o=a.length;n<o;n++)if(f=a[n],f||0===f)if("object"===r.type(f))r.merge(m,f.nodeType?[f]:f);else if(pa.test(f)){g=g||l.appendChild(b.createElement("div")),h=(ka.exec(f)||["",""])[1].toLowerCase(),i=ma[h]||ma._default,g.innerHTML=i[1]+r.htmlPrefilter(f)+i[2],k=i[0];while(k--)g=g.lastChild;r.merge(m,g.childNodes),g=l.firstChild,g.textContent=""}else m.push(b.createTextNode(f));l.textContent="",n=0;while(f=m[n++])if(d&&r.inArray(f,d)>-1)e&&e.push(f);else if(j=r.contains(f.ownerDocument,f),g=na(l.appendChild(f),"script"),j&&oa(g),c){k=0;while(f=g[k++])la.test(f.type||"")&&c.push(f)}return l}!function(){var a=d.createDocumentFragment(),b=a.appendChild(d.createElement("div")),c=d.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),o.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",o.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var ra=d.documentElement,sa=/^key/,ta=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,ua=/^([^.]*)(?:\.(.+)|)/;function va(){return!0}function wa(){return!1}function xa(){try{return d.activeElement}catch(a){}}function ya(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)ya(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=wa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return r().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=r.guid++)),a.each(function(){r.event.add(this,b,e,d,c)})}r.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,n,o,p,q=W.get(a);if(q){c.handler&&(f=c,c=f.handler,e=f.selector),e&&r.find.matchesSelector(ra,e),c.guid||(c.guid=r.guid++),(i=q.events)||(i=q.events={}),(g=q.handle)||(g=q.handle=function(b){return"undefined"!=typeof r&&r.event.triggered!==b.type?r.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(L)||[""],j=b.length;while(j--)h=ua.exec(b[j])||[],n=p=h[1],o=(h[2]||"").split(".").sort(),n&&(l=r.event.special[n]||{},n=(e?l.delegateType:l.bindType)||n,l=r.event.special[n]||{},k=r.extend({type:n,origType:p,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&r.expr.match.needsContext.test(e),namespace:o.join(".")},f),(m=i[n])||(m=i[n]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,o,g)!==!1||a.addEventListener&&a.addEventListener(n,g)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),r.event.global[n]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,n,o,p,q=W.hasData(a)&&W.get(a);if(q&&(i=q.events)){b=(b||"").match(L)||[""],j=b.length;while(j--)if(h=ua.exec(b[j])||[],n=p=h[1],o=(h[2]||"").split(".").sort(),n){l=r.event.special[n]||{},n=(d?l.delegateType:l.bindType)||n,m=i[n]||[],h=h[2]&&new RegExp("(^|\\.)"+o.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&p!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,o,q.handle)!==!1||r.removeEvent(a,n,q.handle),delete i[n])}else for(n in i)r.event.remove(a,n+b[j],c,d,!0);r.isEmptyObject(i)&&W.remove(a,"handle events")}},dispatch:function(a){var b=r.event.fix(a),c,d,e,f,g,h,i=new Array(arguments.length),j=(W.get(this,"events")||{})[b.type]||[],k=r.event.special[b.type]||{};for(i[0]=b,c=1;c<arguments.length;c++)i[c]=arguments[c];if(b.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,b)!==!1){h=r.event.handlers.call(this,b,j),c=0;while((f=h[c++])&&!b.isPropagationStopped()){b.currentTarget=f.elem,d=0;while((g=f.handlers[d++])&&!b.isImmediatePropagationStopped())b.rnamespace&&!b.rnamespace.test(g.namespace)||(b.handleObj=g,b.data=g.data,e=((r.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==e&&(b.result=e)===!1&&(b.preventDefault(),b.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,b),b.result}},handlers:function(a,b){var c,d,e,f,g,h=[],i=b.delegateCount,j=a.target;if(i&&j.nodeType&&!("click"===a.type&&a.button>=1))for(;j!==this;j=j.parentNode||this)if(1===j.nodeType&&("click"!==a.type||j.disabled!==!0)){for(f=[],g={},c=0;c<i;c++)d=b[c],e=d.selector+" ",void 0===g[e]&&(g[e]=d.needsContext?r(e,this).index(j)>-1:r.find(e,this,null,[j]).length),g[e]&&f.push(d);f.length&&h.push({elem:j,handlers:f})}return j=this,i<b.length&&h.push({elem:j,handlers:b.slice(i)}),h},addProp:function(a,b){Object.defineProperty(r.Event.prototype,a,{enumerable:!0,configurable:!0,get:r.isFunction(b)?function(){if(this.originalEvent)return b(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[a]},set:function(b){Object.defineProperty(this,a,{enumerable:!0,configurable:!0,writable:!0,value:b})}})},fix:function(a){return a[r.expando]?a:new r.Event(a)},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==xa()&&this.focus)return this.focus(),!1},delegateType:"focusin"},blur:{trigger:function(){if(this===xa()&&this.blur)return this.blur(),!1},delegateType:"focusout"},click:{trigger:function(){if("checkbox"===this.type&&this.click&&B(this,"input"))return this.click(),!1},_default:function(a){return B(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}}},r.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)},r.Event=function(a,b){return this instanceof r.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?va:wa,this.target=a.target&&3===a.target.nodeType?a.target.parentNode:a.target,this.currentTarget=a.currentTarget,this.relatedTarget=a.relatedTarget):this.type=a,b&&r.extend(this,b),this.timeStamp=a&&a.timeStamp||r.now(),void(this[r.expando]=!0)):new r.Event(a,b)},r.Event.prototype={constructor:r.Event,isDefaultPrevented:wa,isPropagationStopped:wa,isImmediatePropagationStopped:wa,isSimulated:!1,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=va,a&&!this.isSimulated&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=va,a&&!this.isSimulated&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=va,a&&!this.isSimulated&&a.stopImmediatePropagation(),this.stopPropagation()}},r.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(a){var b=a.button;return null==a.which&&sa.test(a.type)?null!=a.charCode?a.charCode:a.keyCode:!a.which&&void 0!==b&&ta.test(a.type)?1&b?1:2&b?3:4&b?2:0:a.which}},r.event.addProp),r.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){r.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||r.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),r.fn.extend({on:function(a,b,c,d){return ya(this,a,b,c,d)},one:function(a,b,c,d){return ya(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,r(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=wa),this.each(function(){r.event.remove(this,a,c,b)})}});var za=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,Aa=/<script|<style|<link/i,Ba=/checked\s*(?:[^=]|=\s*.checked.)/i,Ca=/^true\/(.*)/,Da=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Ea(a,b){return B(a,"table")&&B(11!==b.nodeType?b:b.firstChild,"tr")?r(">tbody",a)[0]||a:a}function Fa(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function Ga(a){var b=Ca.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Ha(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(W.hasData(a)&&(f=W.access(a),g=W.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;c<d;c++)r.event.add(b,e,j[e][c])}X.hasData(a)&&(h=X.access(a),i=r.extend({},h),X.set(b,i))}}function Ia(a,b){var c=b.nodeName.toLowerCase();"input"===c&&ja.test(a.type)?b.checked=a.checked:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}function Ja(a,b,c,d){b=g.apply([],b);var e,f,h,i,j,k,l=0,m=a.length,n=m-1,q=b[0],s=r.isFunction(q);if(s||m>1&&"string"==typeof q&&!o.checkClone&&Ba.test(q))return a.each(function(e){var f=a.eq(e);s&&(b[0]=q.call(this,e,f.html())),Ja(f,b,c,d)});if(m&&(e=qa(b,a[0].ownerDocument,!1,a,d),f=e.firstChild,1===e.childNodes.length&&(e=f),f||d)){for(h=r.map(na(e,"script"),Fa),i=h.length;l<m;l++)j=e,l!==n&&(j=r.clone(j,!0,!0),i&&r.merge(h,na(j,"script"))),c.call(a[l],j,l);if(i)for(k=h[h.length-1].ownerDocument,r.map(h,Ga),l=0;l<i;l++)j=h[l],la.test(j.type||"")&&!W.access(j,"globalEval")&&r.contains(k,j)&&(j.src?r._evalUrl&&r._evalUrl(j.src):p(j.textContent.replace(Da,""),k))}return a}function Ka(a,b,c){for(var d,e=b?r.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||r.cleanData(na(d)),d.parentNode&&(c&&r.contains(d.ownerDocument,d)&&oa(na(d,"script")),d.parentNode.removeChild(d));return a}r.extend({htmlPrefilter:function(a){return a.replace(za,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=r.contains(a.ownerDocument,a);if(!(o.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||r.isXMLDoc(a)))for(g=na(h),f=na(a),d=0,e=f.length;d<e;d++)Ia(f[d],g[d]);if(b)if(c)for(f=f||na(a),g=g||na(h),d=0,e=f.length;d<e;d++)Ha(f[d],g[d]);else Ha(a,h);return g=na(h,"script"),g.length>0&&oa(g,!i&&na(a,"script")),h},cleanData:function(a){for(var b,c,d,e=r.event.special,f=0;void 0!==(c=a[f]);f++)if(U(c)){if(b=c[W.expando]){if(b.events)for(d in b.events)e[d]?r.event.remove(c,d):r.removeEvent(c,d,b.handle);c[W.expando]=void 0}c[X.expando]&&(c[X.expando]=void 0)}}}),r.fn.extend({detach:function(a){return Ka(this,a,!0)},remove:function(a){return Ka(this,a)},text:function(a){return T(this,function(a){return void 0===a?r.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=a)})},null,a,arguments.length)},append:function(){return Ja(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ea(this,a);b.appendChild(a)}})},prepend:function(){return Ja(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ea(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ja(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ja(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(r.cleanData(na(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null!=a&&a,b=null==b?a:b,this.map(function(){return r.clone(this,a,b)})},html:function(a){return T(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!Aa.test(a)&&!ma[(ka.exec(a)||["",""])[1].toLowerCase()]){a=r.htmlPrefilter(a);try{for(;c<d;c++)b=this[c]||{},1===b.nodeType&&(r.cleanData(na(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ja(this,arguments,function(b){var c=this.parentNode;r.inArray(this,a)<0&&(r.cleanData(na(this)),c&&c.replaceChild(b,this))},a)}}),r.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){r.fn[a]=function(a){for(var c,d=[],e=r(a),f=e.length-1,g=0;g<=f;g++)c=g===f?this:this.clone(!0),r(e[g])[b](c),h.apply(d,c.get());return this.pushStack(d)}});var La=/^margin/,Ma=new RegExp("^("+aa+")(?!px)[a-z%]+$","i"),Na=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)};!function(){function b(){if(i){i.style.cssText="box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",i.innerHTML="",ra.appendChild(h);var b=a.getComputedStyle(i);c="1%"!==b.top,g="2px"===b.marginLeft,e="4px"===b.width,i.style.marginRight="50%",f="4px"===b.marginRight,ra.removeChild(h),i=null}}var c,e,f,g,h=d.createElement("div"),i=d.createElement("div");i.style&&(i.style.backgroundClip="content-box",i.cloneNode(!0).style.backgroundClip="",o.clearCloneStyle="content-box"===i.style.backgroundClip,h.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",h.appendChild(i),r.extend(o,{pixelPosition:function(){return b(),c},boxSizingReliable:function(){return b(),e},pixelMarginRight:function(){return b(),f},reliableMarginLeft:function(){return b(),g}}))}();function Oa(a,b,c){var d,e,f,g,h=a.style;return c=c||Na(a),c&&(g=c.getPropertyValue(b)||c[b],""!==g||r.contains(a.ownerDocument,a)||(g=r.style(a,b)),!o.pixelMarginRight()&&Ma.test(g)&&La.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0!==g?g+"":g}function Pa(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Qa=/^(none|table(?!-c[ea]).+)/,Ra=/^--/,Sa={position:"absolute",visibility:"hidden",display:"block"},Ta={letterSpacing:"0",fontWeight:"400"},Ua=["Webkit","Moz","ms"],Va=d.createElement("div").style;function Wa(a){if(a in Va)return a;var b=a[0].toUpperCase()+a.slice(1),c=Ua.length;while(c--)if(a=Ua[c]+b,a in Va)return a}function Xa(a){var b=r.cssProps[a];return b||(b=r.cssProps[a]=Wa(a)||a),b}function Ya(a,b,c){var d=ba.exec(b);return d?Math.max(0,d[2]-(c||0))+(d[3]||"px"):b}function Za(a,b,c,d,e){var f,g=0;for(f=c===(d?"border":"content")?4:"width"===b?1:0;f<4;f+=2)"margin"===c&&(g+=r.css(a,c+ca[f],!0,e)),d?("content"===c&&(g-=r.css(a,"padding"+ca[f],!0,e)),"margin"!==c&&(g-=r.css(a,"border"+ca[f]+"Width",!0,e))):(g+=r.css(a,"padding"+ca[f],!0,e),"padding"!==c&&(g+=r.css(a,"border"+ca[f]+"Width",!0,e)));return g}function $a(a,b,c){var d,e=Na(a),f=Oa(a,b,e),g="border-box"===r.css(a,"boxSizing",!1,e);return Ma.test(f)?f:(d=g&&(o.boxSizingReliable()||f===a.style[b]),"auto"===f&&(f=a["offset"+b[0].toUpperCase()+b.slice(1)]),f=parseFloat(f)||0,f+Za(a,b,c||(g?"border":"content"),d,e)+"px")}r.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Oa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=r.camelCase(b),i=Ra.test(b),j=a.style;return i||(b=Xa(h)),g=r.cssHooks[b]||r.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:j[b]:(f=typeof c,"string"===f&&(e=ba.exec(c))&&e[1]&&(c=fa(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(r.cssNumber[h]?"":"px")),o.clearCloneStyle||""!==c||0!==b.indexOf("background")||(j[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i?j.setProperty(b,c):j[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=r.camelCase(b),i=Ra.test(b);return i||(b=Xa(h)),g=r.cssHooks[b]||r.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=Oa(a,b,d)),"normal"===e&&b in Ta&&(e=Ta[b]),""===c||c?(f=parseFloat(e),c===!0||isFinite(f)?f||0:e):e}}),r.each(["height","width"],function(a,b){r.cssHooks[b]={get:function(a,c,d){if(c)return!Qa.test(r.css(a,"display"))||a.getClientRects().length&&a.getBoundingClientRect().width?$a(a,b,d):ea(a,Sa,function(){return $a(a,b,d)})},set:function(a,c,d){var e,f=d&&Na(a),g=d&&Za(a,b,d,"border-box"===r.css(a,"boxSizing",!1,f),f);return g&&(e=ba.exec(c))&&"px"!==(e[3]||"px")&&(a.style[b]=c,c=r.css(a,b)),Ya(a,c,g)}}}),r.cssHooks.marginLeft=Pa(o.reliableMarginLeft,function(a,b){if(b)return(parseFloat(Oa(a,"marginLeft"))||a.getBoundingClientRect().left-ea(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}))+"px"}),r.each({margin:"",padding:"",border:"Width"},function(a,b){r.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];d<4;d++)e[a+ca[d]+b]=f[d]||f[d-2]||f[0];return e}},La.test(a)||(r.cssHooks[a+b].set=Ya)}),r.fn.extend({css:function(a,b){return T(this,function(a,b,c){var d,e,f={},g=0;if(Array.isArray(b)){for(d=Na(a),e=b.length;g<e;g++)f[b[g]]=r.css(a,b[g],!1,d);return f}return void 0!==c?r.style(a,b,c):r.css(a,b)},a,b,arguments.length>1)}});function _a(a,b,c,d,e){return new _a.prototype.init(a,b,c,d,e)}r.Tween=_a,_a.prototype={constructor:_a,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||r.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(r.cssNumber[c]?"":"px")},cur:function(){var a=_a.propHooks[this.prop];return a&&a.get?a.get(this):_a.propHooks._default.get(this)},run:function(a){var b,c=_a.propHooks[this.prop];return this.options.duration?this.pos=b=r.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):_a.propHooks._default.set(this),this}},_a.prototype.init.prototype=_a.prototype,_a.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=r.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){r.fx.step[a.prop]?r.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[r.cssProps[a.prop]]&&!r.cssHooks[a.prop]?a.elem[a.prop]=a.now:r.style(a.elem,a.prop,a.now+a.unit)}}},_a.propHooks.scrollTop=_a.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},r.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},r.fx=_a.prototype.init,r.fx.step={};var ab,bb,cb=/^(?:toggle|show|hide)$/,db=/queueHooks$/;function eb(){bb&&(d.hidden===!1&&a.requestAnimationFrame?a.requestAnimationFrame(eb):a.setTimeout(eb,r.fx.interval),r.fx.tick())}function fb(){return a.setTimeout(function(){ab=void 0}),ab=r.now()}function gb(a,b){var c,d=0,e={height:a};for(b=b?1:0;d<4;d+=2-b)c=ca[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function hb(a,b,c){for(var d,e=(kb.tweeners[b]||[]).concat(kb.tweeners["*"]),f=0,g=e.length;f<g;f++)if(d=e[f].call(c,b,a))return d}function ib(a,b,c){var d,e,f,g,h,i,j,k,l="width"in b||"height"in b,m=this,n={},o=a.style,p=a.nodeType&&da(a),q=W.get(a,"fxshow");c.queue||(g=r._queueHooks(a,"fx"),null==g.unqueued&&(g.unqueued=0,h=g.empty.fire,g.empty.fire=function(){g.unqueued||h()}),g.unqueued++,m.always(function(){m.always(function(){g.unqueued--,r.queue(a,"fx").length||g.empty.fire()})}));for(d in b)if(e=b[d],cb.test(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}n[d]=q&&q[d]||r.style(a,d)}if(i=!r.isEmptyObject(b),i||!r.isEmptyObject(n)){l&&1===a.nodeType&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=q&&q.display,null==j&&(j=W.get(a,"display")),k=r.css(a,"display"),"none"===k&&(j?k=j:(ia([a],!0),j=a.style.display||j,k=r.css(a,"display"),ia([a]))),("inline"===k||"inline-block"===k&&null!=j)&&"none"===r.css(a,"float")&&(i||(m.done(function(){o.display=j}),null==j&&(k=o.display,j="none"===k?"":k)),o.display="inline-block")),c.overflow&&(o.overflow="hidden",m.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]})),i=!1;for(d in n)i||(q?"hidden"in q&&(p=q.hidden):q=W.access(a,"fxshow",{display:j}),f&&(q.hidden=!p),p&&ia([a],!0),m.done(function(){p||ia([a]),W.remove(a,"fxshow");for(d in n)r.style(a,d,n[d])})),i=hb(p?q[d]:0,d,m),d in q||(q[d]=i.start,p&&(i.end=i.start,i.start=0))}}function jb(a,b){var c,d,e,f,g;for(c in a)if(d=r.camelCase(c),e=b[d],f=a[c],Array.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=r.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function kb(a,b,c){var d,e,f=0,g=kb.prefilters.length,h=r.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=ab||fb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;g<i;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),f<1&&i?c:(i||h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:r.extend({},b),opts:r.extend(!0,{specialEasing:{},easing:r.easing._default},c),originalProperties:b,originalOptions:c,startTime:ab||fb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=r.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;c<d;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(jb(k,j.opts.specialEasing);f<g;f++)if(d=kb.prefilters[f].call(j,a,k,j.opts))return r.isFunction(d.stop)&&(r._queueHooks(j.elem,j.opts.queue).stop=r.proxy(d.stop,d)),d;return r.map(k,hb,j),r.isFunction(j.opts.start)&&j.opts.start.call(a,j),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always),r.fx.timer(r.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j}r.Animation=r.extend(kb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return fa(c.elem,a,ba.exec(b),c),c}]},tweener:function(a,b){r.isFunction(a)?(b=a,a=["*"]):a=a.match(L);for(var c,d=0,e=a.length;d<e;d++)c=a[d],kb.tweeners[c]=kb.tweeners[c]||[],kb.tweeners[c].unshift(b)},prefilters:[ib],prefilter:function(a,b){b?kb.prefilters.unshift(a):kb.prefilters.push(a)}}),r.speed=function(a,b,c){var d=a&&"object"==typeof a?r.extend({},a):{complete:c||!c&&b||r.isFunction(a)&&a,duration:a,easing:c&&b||b&&!r.isFunction(b)&&b};return r.fx.off?d.duration=0:"number"!=typeof d.duration&&(d.duration in r.fx.speeds?d.duration=r.fx.speeds[d.duration]:d.duration=r.fx.speeds._default),null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){r.isFunction(d.old)&&d.old.call(this),d.queue&&r.dequeue(this,d.queue)},d},r.fn.extend({fadeTo:function(a,b,c,d){return this.filter(da).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=r.isEmptyObject(a),f=r.speed(b,c,d),g=function(){var b=kb(this,r.extend({},a),f);(e||W.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=r.timers,g=W.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&db.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||r.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=W.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=r.timers,g=d?d.length:0;for(c.finish=!0,r.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;b<g;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),r.each(["toggle","show","hide"],function(a,b){var c=r.fn[b];r.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(gb(b,!0),a,d,e)}}),r.each({slideDown:gb("show"),slideUp:gb("hide"),slideToggle:gb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){r.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),r.timers=[],r.fx.tick=function(){var a,b=0,c=r.timers;for(ab=r.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||r.fx.stop(),ab=void 0},r.fx.timer=function(a){r.timers.push(a),r.fx.start()},r.fx.interval=13,r.fx.start=function(){bb||(bb=!0,eb())},r.fx.stop=function(){bb=null},r.fx.speeds={slow:600,fast:200,_default:400},r.fn.delay=function(b,c){return b=r.fx?r.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a=d.createElement("input"),b=d.createElement("select"),c=b.appendChild(d.createElement("option"));a.type="checkbox",o.checkOn=""!==a.value,o.optSelected=c.selected,a=d.createElement("input"),a.value="t",a.type="radio",o.radioValue="t"===a.value}();var lb,mb=r.expr.attrHandle;r.fn.extend({attr:function(a,b){return T(this,r.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){r.removeAttr(this,a)})}}),r.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?r.prop(a,b,c):(1===f&&r.isXMLDoc(a)||(e=r.attrHooks[b.toLowerCase()]||(r.expr.match.bool.test(b)?lb:void 0)),void 0!==c?null===c?void r.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=r.find.attr(a,b),
null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!o.radioValue&&"radio"===b&&B(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d=0,e=b&&b.match(L);if(e&&1===a.nodeType)while(c=e[d++])a.removeAttribute(c)}}),lb={set:function(a,b,c){return b===!1?r.removeAttr(a,c):a.setAttribute(c,c),c}},r.each(r.expr.match.bool.source.match(/\w+/g),function(a,b){var c=mb[b]||r.find.attr;mb[b]=function(a,b,d){var e,f,g=b.toLowerCase();return d||(f=mb[g],mb[g]=e,e=null!=c(a,b,d)?g:null,mb[g]=f),e}});var nb=/^(?:input|select|textarea|button)$/i,ob=/^(?:a|area)$/i;r.fn.extend({prop:function(a,b){return T(this,r.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[r.propFix[a]||a]})}}),r.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&r.isXMLDoc(a)||(b=r.propFix[b]||b,e=r.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=r.find.attr(a,"tabindex");return b?parseInt(b,10):nb.test(a.nodeName)||ob.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),o.optSelected||(r.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),r.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){r.propFix[this.toLowerCase()]=this});function pb(a){var b=a.match(L)||[];return b.join(" ")}function qb(a){return a.getAttribute&&a.getAttribute("class")||""}r.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(r.isFunction(a))return this.each(function(b){r(this).addClass(a.call(this,b,qb(this)))});if("string"==typeof a&&a){b=a.match(L)||[];while(c=this[i++])if(e=qb(c),d=1===c.nodeType&&" "+pb(e)+" "){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=pb(d),e!==h&&c.setAttribute("class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(r.isFunction(a))return this.each(function(b){r(this).removeClass(a.call(this,b,qb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(L)||[];while(c=this[i++])if(e=qb(c),d=1===c.nodeType&&" "+pb(e)+" "){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=pb(d),e!==h&&c.setAttribute("class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):r.isFunction(a)?this.each(function(c){r(this).toggleClass(a.call(this,c,qb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=r(this),f=a.match(L)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=qb(this),b&&W.set(this,"__className__",b),this.setAttribute&&this.setAttribute("class",b||a===!1?"":W.get(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+pb(qb(c))+" ").indexOf(b)>-1)return!0;return!1}});var rb=/\r/g;r.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=r.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,r(this).val()):a,null==e?e="":"number"==typeof e?e+="":Array.isArray(e)&&(e=r.map(e,function(a){return null==a?"":a+""})),b=r.valHooks[this.type]||r.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=r.valHooks[e.type]||r.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),r.extend({valHooks:{option:{get:function(a){var b=r.find.attr(a,"value");return null!=b?b:pb(r.text(a))}},select:{get:function(a){var b,c,d,e=a.options,f=a.selectedIndex,g="select-one"===a.type,h=g?null:[],i=g?f+1:e.length;for(d=f<0?i:g?f:0;d<i;d++)if(c=e[d],(c.selected||d===f)&&!c.disabled&&(!c.parentNode.disabled||!B(c.parentNode,"optgroup"))){if(b=r(c).val(),g)return b;h.push(b)}return h},set:function(a,b){var c,d,e=a.options,f=r.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=r.inArray(r.valHooks.option.get(d),f)>-1)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),r.each(["radio","checkbox"],function(){r.valHooks[this]={set:function(a,b){if(Array.isArray(b))return a.checked=r.inArray(r(a).val(),b)>-1}},o.checkOn||(r.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var sb=/^(?:focusinfocus|focusoutblur)$/;r.extend(r.event,{trigger:function(b,c,e,f){var g,h,i,j,k,m,n,o=[e||d],p=l.call(b,"type")?b.type:b,q=l.call(b,"namespace")?b.namespace.split("."):[];if(h=i=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!sb.test(p+r.event.triggered)&&(p.indexOf(".")>-1&&(q=p.split("."),p=q.shift(),q.sort()),k=p.indexOf(":")<0&&"on"+p,b=b[r.expando]?b:new r.Event(p,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=q.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:r.makeArray(c,[b]),n=r.event.special[p]||{},f||!n.trigger||n.trigger.apply(e,c)!==!1)){if(!f&&!n.noBubble&&!r.isWindow(e)){for(j=n.delegateType||p,sb.test(j+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),i=h;i===(e.ownerDocument||d)&&o.push(i.defaultView||i.parentWindow||a)}g=0;while((h=o[g++])&&!b.isPropagationStopped())b.type=g>1?j:n.bindType||p,m=(W.get(h,"events")||{})[b.type]&&W.get(h,"handle"),m&&m.apply(h,c),m=k&&h[k],m&&m.apply&&U(h)&&(b.result=m.apply(h,c),b.result===!1&&b.preventDefault());return b.type=p,f||b.isDefaultPrevented()||n._default&&n._default.apply(o.pop(),c)!==!1||!U(e)||k&&r.isFunction(e[p])&&!r.isWindow(e)&&(i=e[k],i&&(e[k]=null),r.event.triggered=p,e[p](),r.event.triggered=void 0,i&&(e[k]=i)),b.result}},simulate:function(a,b,c){var d=r.extend(new r.Event,c,{type:a,isSimulated:!0});r.event.trigger(d,null,b)}}),r.fn.extend({trigger:function(a,b){return this.each(function(){r.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];if(c)return r.event.trigger(a,b,c,!0)}}),r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(a,b){r.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),r.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),o.focusin="onfocusin"in a,o.focusin||r.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){r.event.simulate(b,a.target,r.event.fix(a))};r.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=W.access(d,b);e||d.addEventListener(a,c,!0),W.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=W.access(d,b)-1;e?W.access(d,b,e):(d.removeEventListener(a,c,!0),W.remove(d,b))}}});var tb=a.location,ub=r.now(),vb=/\?/;r.parseXML=function(b){var c;if(!b||"string"!=typeof b)return null;try{c=(new a.DOMParser).parseFromString(b,"text/xml")}catch(d){c=void 0}return c&&!c.getElementsByTagName("parsererror").length||r.error("Invalid XML: "+b),c};var wb=/\[\]$/,xb=/\r?\n/g,yb=/^(?:submit|button|image|reset|file)$/i,zb=/^(?:input|select|textarea|keygen)/i;function Ab(a,b,c,d){var e;if(Array.isArray(b))r.each(b,function(b,e){c||wb.test(a)?d(a,e):Ab(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==r.type(b))d(a,b);else for(e in b)Ab(a+"["+e+"]",b[e],c,d)}r.param=function(a,b){var c,d=[],e=function(a,b){var c=r.isFunction(b)?b():b;d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(null==c?"":c)};if(Array.isArray(a)||a.jquery&&!r.isPlainObject(a))r.each(a,function(){e(this.name,this.value)});else for(c in a)Ab(c,a[c],b,e);return d.join("&")},r.fn.extend({serialize:function(){return r.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=r.prop(this,"elements");return a?r.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!r(this).is(":disabled")&&zb.test(this.nodeName)&&!yb.test(a)&&(this.checked||!ja.test(a))}).map(function(a,b){var c=r(this).val();return null==c?null:Array.isArray(c)?r.map(c,function(a){return{name:b.name,value:a.replace(xb,"\r\n")}}):{name:b.name,value:c.replace(xb,"\r\n")}}).get()}});var Bb=/%20/g,Cb=/#.*$/,Db=/([?&])_=[^&]*/,Eb=/^(.*?):[ \t]*([^\r\n]*)$/gm,Fb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Gb=/^(?:GET|HEAD)$/,Hb=/^\/\//,Ib={},Jb={},Kb="*/".concat("*"),Lb=d.createElement("a");Lb.href=tb.href;function Mb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(L)||[];if(r.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Nb(a,b,c,d){var e={},f=a===Jb;function g(h){var i;return e[h]=!0,r.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Ob(a,b){var c,d,e=r.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&r.extend(!0,a,d),a}function Pb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}if(f)return f!==i[0]&&i.unshift(f),c[f]}function Qb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}r.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:tb.href,type:"GET",isLocal:Fb.test(tb.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Kb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":r.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Ob(Ob(a,r.ajaxSettings),b):Ob(r.ajaxSettings,a)},ajaxPrefilter:Mb(Ib),ajaxTransport:Mb(Jb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var e,f,g,h,i,j,k,l,m,n,o=r.ajaxSetup({},c),p=o.context||o,q=o.context&&(p.nodeType||p.jquery)?r(p):r.event,s=r.Deferred(),t=r.Callbacks("once memory"),u=o.statusCode||{},v={},w={},x="canceled",y={readyState:0,getResponseHeader:function(a){var b;if(k){if(!h){h={};while(b=Eb.exec(g))h[b[1].toLowerCase()]=b[2]}b=h[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return k?g:null},setRequestHeader:function(a,b){return null==k&&(a=w[a.toLowerCase()]=w[a.toLowerCase()]||a,v[a]=b),this},overrideMimeType:function(a){return null==k&&(o.mimeType=a),this},statusCode:function(a){var b;if(a)if(k)y.always(a[y.status]);else for(b in a)u[b]=[u[b],a[b]];return this},abort:function(a){var b=a||x;return e&&e.abort(b),A(0,b),this}};if(s.promise(y),o.url=((b||o.url||tb.href)+"").replace(Hb,tb.protocol+"//"),o.type=c.method||c.type||o.method||o.type,o.dataTypes=(o.dataType||"*").toLowerCase().match(L)||[""],null==o.crossDomain){j=d.createElement("a");try{j.href=o.url,j.href=j.href,o.crossDomain=Lb.protocol+"//"+Lb.host!=j.protocol+"//"+j.host}catch(z){o.crossDomain=!0}}if(o.data&&o.processData&&"string"!=typeof o.data&&(o.data=r.param(o.data,o.traditional)),Nb(Ib,o,c,y),k)return y;l=r.event&&o.global,l&&0===r.active++&&r.event.trigger("ajaxStart"),o.type=o.type.toUpperCase(),o.hasContent=!Gb.test(o.type),f=o.url.replace(Cb,""),o.hasContent?o.data&&o.processData&&0===(o.contentType||"").indexOf("application/x-www-form-urlencoded")&&(o.data=o.data.replace(Bb,"+")):(n=o.url.slice(f.length),o.data&&(f+=(vb.test(f)?"&":"?")+o.data,delete o.data),o.cache===!1&&(f=f.replace(Db,"$1"),n=(vb.test(f)?"&":"?")+"_="+ub++ +n),o.url=f+n),o.ifModified&&(r.lastModified[f]&&y.setRequestHeader("If-Modified-Since",r.lastModified[f]),r.etag[f]&&y.setRequestHeader("If-None-Match",r.etag[f])),(o.data&&o.hasContent&&o.contentType!==!1||c.contentType)&&y.setRequestHeader("Content-Type",o.contentType),y.setRequestHeader("Accept",o.dataTypes[0]&&o.accepts[o.dataTypes[0]]?o.accepts[o.dataTypes[0]]+("*"!==o.dataTypes[0]?", "+Kb+"; q=0.01":""):o.accepts["*"]);for(m in o.headers)y.setRequestHeader(m,o.headers[m]);if(o.beforeSend&&(o.beforeSend.call(p,y,o)===!1||k))return y.abort();if(x="abort",t.add(o.complete),y.done(o.success),y.fail(o.error),e=Nb(Jb,o,c,y)){if(y.readyState=1,l&&q.trigger("ajaxSend",[y,o]),k)return y;o.async&&o.timeout>0&&(i=a.setTimeout(function(){y.abort("timeout")},o.timeout));try{k=!1,e.send(v,A)}catch(z){if(k)throw z;A(-1,z)}}else A(-1,"No Transport");function A(b,c,d,h){var j,m,n,v,w,x=c;k||(k=!0,i&&a.clearTimeout(i),e=void 0,g=h||"",y.readyState=b>0?4:0,j=b>=200&&b<300||304===b,d&&(v=Pb(o,y,d)),v=Qb(o,v,y,j),j?(o.ifModified&&(w=y.getResponseHeader("Last-Modified"),w&&(r.lastModified[f]=w),w=y.getResponseHeader("etag"),w&&(r.etag[f]=w)),204===b||"HEAD"===o.type?x="nocontent":304===b?x="notmodified":(x=v.state,m=v.data,n=v.error,j=!n)):(n=x,!b&&x||(x="error",b<0&&(b=0))),y.status=b,y.statusText=(c||x)+"",j?s.resolveWith(p,[m,x,y]):s.rejectWith(p,[y,x,n]),y.statusCode(u),u=void 0,l&&q.trigger(j?"ajaxSuccess":"ajaxError",[y,o,j?m:n]),t.fireWith(p,[y,x]),l&&(q.trigger("ajaxComplete",[y,o]),--r.active||r.event.trigger("ajaxStop")))}return y},getJSON:function(a,b,c){return r.get(a,b,c,"json")},getScript:function(a,b){return r.get(a,void 0,b,"script")}}),r.each(["get","post"],function(a,b){r[b]=function(a,c,d,e){return r.isFunction(c)&&(e=e||d,d=c,c=void 0),r.ajax(r.extend({url:a,type:b,dataType:e,data:c,success:d},r.isPlainObject(a)&&a))}}),r._evalUrl=function(a){return r.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},r.fn.extend({wrapAll:function(a){var b;return this[0]&&(r.isFunction(a)&&(a=a.call(this[0])),b=r(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this},wrapInner:function(a){return r.isFunction(a)?this.each(function(b){r(this).wrapInner(a.call(this,b))}):this.each(function(){var b=r(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=r.isFunction(a);return this.each(function(c){r(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(a){return this.parent(a).not("body").each(function(){r(this).replaceWith(this.childNodes)}),this}}),r.expr.pseudos.hidden=function(a){return!r.expr.pseudos.visible(a)},r.expr.pseudos.visible=function(a){return!!(a.offsetWidth||a.offsetHeight||a.getClientRects().length)},r.ajaxSettings.xhr=function(){try{return new a.XMLHttpRequest}catch(b){}};var Rb={0:200,1223:204},Sb=r.ajaxSettings.xhr();o.cors=!!Sb&&"withCredentials"in Sb,o.ajax=Sb=!!Sb,r.ajaxTransport(function(b){var c,d;if(o.cors||Sb&&!b.crossDomain)return{send:function(e,f){var g,h=b.xhr();if(h.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(g in b.xhrFields)h[g]=b.xhrFields[g];b.mimeType&&h.overrideMimeType&&h.overrideMimeType(b.mimeType),b.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest");for(g in e)h.setRequestHeader(g,e[g]);c=function(a){return function(){c&&(c=d=h.onload=h.onerror=h.onabort=h.onreadystatechange=null,"abort"===a?h.abort():"error"===a?"number"!=typeof h.status?f(0,"error"):f(h.status,h.statusText):f(Rb[h.status]||h.status,h.statusText,"text"!==(h.responseType||"text")||"string"!=typeof h.responseText?{binary:h.response}:{text:h.responseText},h.getAllResponseHeaders()))}},h.onload=c(),d=h.onerror=c("error"),void 0!==h.onabort?h.onabort=d:h.onreadystatechange=function(){4===h.readyState&&a.setTimeout(function(){c&&d()})},c=c("abort");try{h.send(b.hasContent&&b.data||null)}catch(i){if(c)throw i}},abort:function(){c&&c()}}}),r.ajaxPrefilter(function(a){a.crossDomain&&(a.contents.script=!1)}),r.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return r.globalEval(a),a}}}),r.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),r.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(e,f){b=r("<script>").prop({charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&f("error"===a.type?404:200,a.type)}),d.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Tb=[],Ub=/(=)\?(?=&|$)|\?\?/;r.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Tb.pop()||r.expando+"_"+ub++;return this[a]=!0,a}}),r.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Ub.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Ub.test(b.data)&&"data");if(h||"jsonp"===b.dataTypes[0])return e=b.jsonpCallback=r.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Ub,"$1"+e):b.jsonp!==!1&&(b.url+=(vb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||r.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?r(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Tb.push(e)),g&&r.isFunction(f)&&f(g[0]),g=f=void 0}),"script"}),o.createHTMLDocument=function(){var a=d.implementation.createHTMLDocument("").body;return a.innerHTML="<form></form><form></form>",2===a.childNodes.length}(),r.parseHTML=function(a,b,c){if("string"!=typeof a)return[];"boolean"==typeof b&&(c=b,b=!1);var e,f,g;return b||(o.createHTMLDocument?(b=d.implementation.createHTMLDocument(""),e=b.createElement("base"),e.href=d.location.href,b.head.appendChild(e)):b=d),f=C.exec(a),g=!c&&[],f?[b.createElement(f[1])]:(f=qa([a],b,g),g&&g.length&&r(g).remove(),r.merge([],f.childNodes))},r.fn.load=function(a,b,c){var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=pb(a.slice(h)),a=a.slice(0,h)),r.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&r.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?r("<div>").append(r.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},r.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){r.fn[b]=function(a){return this.on(b,a)}}),r.expr.pseudos.animated=function(a){return r.grep(r.timers,function(b){return a===b.elem}).length},r.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=r.css(a,"position"),l=r(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=r.css(a,"top"),i=r.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),r.isFunction(b)&&(b=b.call(a,c,r.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},r.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){r.offset.setOffset(this,a,b)});var b,c,d,e,f=this[0];if(f)return f.getClientRects().length?(d=f.getBoundingClientRect(),b=f.ownerDocument,c=b.documentElement,e=b.defaultView,{top:d.top+e.pageYOffset-c.clientTop,left:d.left+e.pageXOffset-c.clientLeft}):{top:0,left:0}},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===r.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),B(a[0],"html")||(d=a.offset()),d={top:d.top+r.css(a[0],"borderTopWidth",!0),left:d.left+r.css(a[0],"borderLeftWidth",!0)}),{top:b.top-d.top-r.css(c,"marginTop",!0),left:b.left-d.left-r.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&"static"===r.css(a,"position"))a=a.offsetParent;return a||ra})}}),r.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c="pageYOffset"===b;r.fn[a]=function(d){return T(this,function(a,d,e){var f;return r.isWindow(a)?f=a:9===a.nodeType&&(f=a.defaultView),void 0===e?f?f[b]:a[d]:void(f?f.scrollTo(c?f.pageXOffset:e,c?e:f.pageYOffset):a[d]=e)},a,d,arguments.length)}}),r.each(["top","left"],function(a,b){r.cssHooks[b]=Pa(o.pixelPosition,function(a,c){if(c)return c=Oa(a,b),Ma.test(c)?r(a).position()[b]+"px":c})}),r.each({Height:"height",Width:"width"},function(a,b){r.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){r.fn[d]=function(e,f){var g=arguments.length&&(c||"boolean"!=typeof e),h=c||(e===!0||f===!0?"margin":"border");return T(this,function(b,c,e){var f;return r.isWindow(b)?0===d.indexOf("outer")?b["inner"+a]:b.document.documentElement["client"+a]:9===b.nodeType?(f=b.documentElement,Math.max(b.body["scroll"+a],f["scroll"+a],b.body["offset"+a],f["offset"+a],f["client"+a])):void 0===e?r.css(b,c,h):r.style(b,c,e,h)},b,g?e:void 0,g)}})}),r.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),r.holdReady=function(a){a?r.readyWait++:r.ready(!0)},r.isArray=Array.isArray,r.parseJSON=JSON.parse,r.nodeName=B,"function"==typeof define&&define.amd&&define("jquery",[],function(){return r});var Vb=a.jQuery,Wb=a.$;return r.noConflict=function(b){return a.$===r&&(a.$=Wb),b&&a.jQuery===r&&(a.jQuery=Vb),r},b||(a.jQuery=a.$=r),r});
;
/*!
 * jQuery Once v2.2.0 - http://github.com/robloach/jquery-once
 * @license MIT, GPL-2.0
 *   http://opensource.org/licenses/MIT
 *   http://opensource.org/licenses/GPL-2.0
 */
(function(e){"use strict";if(typeof exports==="object"){e(require("jquery"))}else if(typeof define==="function"&&define.amd){define(["jquery"],e)}else{e(jQuery)}})(function(e){"use strict";var n=function(e){e=e||"once";if(typeof e!=="string"){throw new TypeError("The jQuery Once id parameter must be a string")}return e};e.fn.once=function(t){var r="jquery-once-"+n(t);return this.filter(function(){return e(this).data(r)!==true}).data(r,true)};e.fn.removeOnce=function(e){return this.findOnce(e).removeData("jquery-once-"+n(e))};e.fn.findOnce=function(t){var r="jquery-once-"+n(t);return this.filter(function(){return e(this).data(r)===true})}});

/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function () {
  var settingsElement = document.querySelector('head > script[type="application/json"][data-drupal-selector="drupal-settings-json"], body > script[type="application/json"][data-drupal-selector="drupal-settings-json"]');

  window.drupalSettings = {};

  if (settingsElement !== null) {
    window.drupalSettings = JSON.parse(settingsElement.textContent);
  }
})();;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

window.Drupal = { behaviors: {}, locale: {} };

(function (Drupal, drupalSettings, drupalTranslations) {
  Drupal.throwError = function (error) {
    setTimeout(function () {
      throw error;
    }, 0);
  };

  Drupal.attachBehaviors = function (context, settings) {
    context = context || document;
    settings = settings || drupalSettings;
    var behaviors = Drupal.behaviors;

    Object.keys(behaviors || {}).forEach(function (i) {
      if (typeof behaviors[i].attach === 'function') {
        try {
          behaviors[i].attach(context, settings);
        } catch (e) {
          Drupal.throwError(e);
        }
      }
    });
  };

  Drupal.detachBehaviors = function (context, settings, trigger) {
    context = context || document;
    settings = settings || drupalSettings;
    trigger = trigger || 'unload';
    var behaviors = Drupal.behaviors;

    Object.keys(behaviors || {}).forEach(function (i) {
      if (typeof behaviors[i].detach === 'function') {
        try {
          behaviors[i].detach(context, settings, trigger);
        } catch (e) {
          Drupal.throwError(e);
        }
      }
    });
  };

  Drupal.checkPlain = function (str) {
    str = str.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
    return str;
  };

  Drupal.formatString = function (str, args) {
    var processedArgs = {};

    Object.keys(args || {}).forEach(function (key) {
      switch (key.charAt(0)) {
        case '@':
          processedArgs[key] = Drupal.checkPlain(args[key]);
          break;

        case '!':
          processedArgs[key] = args[key];
          break;

        default:
          processedArgs[key] = Drupal.theme('placeholder', args[key]);
          break;
      }
    });

    return Drupal.stringReplace(str, processedArgs, null);
  };

  Drupal.stringReplace = function (str, args, keys) {
    if (str.length === 0) {
      return str;
    }

    if (!Array.isArray(keys)) {
      keys = Object.keys(args || {});

      keys.sort(function (a, b) {
        return a.length - b.length;
      });
    }

    if (keys.length === 0) {
      return str;
    }

    var key = keys.pop();
    var fragments = str.split(key);

    if (keys.length) {
      for (var i = 0; i < fragments.length; i++) {
        fragments[i] = Drupal.stringReplace(fragments[i], args, keys.slice(0));
      }
    }

    return fragments.join(args[key]);
  };

  Drupal.t = function (str, args, options) {
    options = options || {};
    options.context = options.context || '';

    if (typeof drupalTranslations !== 'undefined' && drupalTranslations.strings && drupalTranslations.strings[options.context] && drupalTranslations.strings[options.context][str]) {
      str = drupalTranslations.strings[options.context][str];
    }

    if (args) {
      str = Drupal.formatString(str, args);
    }
    return str;
  };

  Drupal.url = function (path) {
    return drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + path;
  };

  Drupal.url.toAbsolute = function (url) {
    var urlParsingNode = document.createElement('a');

    try {
      url = decodeURIComponent(url);
    } catch (e) {}

    urlParsingNode.setAttribute('href', url);

    return urlParsingNode.cloneNode(false).href;
  };

  Drupal.url.isLocal = function (url) {
    var absoluteUrl = Drupal.url.toAbsolute(url);
    var protocol = window.location.protocol;

    if (protocol === 'http:' && absoluteUrl.indexOf('https:') === 0) {
      protocol = 'https:';
    }
    var baseUrl = protocol + '//' + window.location.host + drupalSettings.path.baseUrl.slice(0, -1);

    try {
      absoluteUrl = decodeURIComponent(absoluteUrl);
    } catch (e) {}
    try {
      baseUrl = decodeURIComponent(baseUrl);
    } catch (e) {}

    return absoluteUrl === baseUrl || absoluteUrl.indexOf(baseUrl + '/') === 0;
  };

  Drupal.formatPlural = function (count, singular, plural, args, options) {
    args = args || {};
    args['@count'] = count;

    var pluralDelimiter = drupalSettings.pluralDelimiter;
    var translations = Drupal.t(singular + pluralDelimiter + plural, args, options).split(pluralDelimiter);
    var index = 0;

    if (typeof drupalTranslations !== 'undefined' && drupalTranslations.pluralFormula) {
      index = count in drupalTranslations.pluralFormula ? drupalTranslations.pluralFormula[count] : drupalTranslations.pluralFormula.default;
    } else if (args['@count'] !== 1) {
      index = 1;
    }

    return translations[index];
  };

  Drupal.encodePath = function (item) {
    return window.encodeURIComponent(item).replace(/%2F/g, '/');
  };

  Drupal.theme = function (func) {
    if (func in Drupal.theme) {
      var _Drupal$theme;

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      return (_Drupal$theme = Drupal.theme)[func].apply(_Drupal$theme, args);
    }
  };

  Drupal.theme.placeholder = function (str) {
    return '<em class="placeholder">' + Drupal.checkPlain(str) + '</em>';
  };
})(Drupal, window.drupalSettings, window.drupalTranslations);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

if (window.jQuery) {
  jQuery.noConflict();
}

document.documentElement.className += ' js';

(function (domready, Drupal, drupalSettings) {
  domready(function () {
    Drupal.attachBehaviors(document, drupalSettings);
  });
})(domready, Drupal, window.drupalSettings);;
(function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == "function" && require;
        if (!u && a) return a(o, !0);
        if (i) return i(o, !0);
        var f = new Error("Cannot find module '" + o + "'");
        throw f.code = "MODULE_NOT_FOUND", f
      }
      var l = n[o] = {
        exports: {}
      };
      t[o][0].call(l.exports, function(e) {
        var n = t[o][1][e];
        return s(n ? n : e)
      }, l, l.exports, e, t, n, r)
    }
    return n[o].exports
  }
  var i = typeof require == "function" && require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s
})({
  1: [
    function(require, module, exports) {
      /*! hyperform.js.org */
      'use strict';

      var registry = Object.create(null);

      /**
       * run all actions registered for a hook
       *
       * Every action gets called with a state object as `this` argument and with the
       * hook's call arguments as call arguments.
       *
       * @return mixed the returned value of the action calls or undefined
       */
      function call_hook(hook) {
        var result;
        var call_args = Array.prototype.slice.call(arguments, 1);

        if (hook in registry) {
          result = registry[hook].reduce(function(args) {

            return function(previousResult, currentAction) {
              var interimResult = currentAction.apply({
                state: previousResult,
                hook: hook
              }, args);
              return interimResult !== undefined ? interimResult : previousResult;
            };
          }(call_args), result);
        }

        return result;
      }

      /**
       * Filter a value through hooked functions
       *
       * Allows for additional parameters:
       * js> do_filter('foo', null, current_element)
       */
      function do_filter(hook, initial_value) {
        var result = initial_value;
        var call_args = Array.prototype.slice.call(arguments, 1);

        if (hook in registry) {
          result = registry[hook].reduce(function(previousResult, currentAction) {
            call_args[0] = previousResult;
            var interimResult = currentAction.apply({
              state: previousResult,
              hook: hook
            }, call_args);
            return interimResult !== undefined ? interimResult : previousResult;
          }, result);
        }

        return result;
      }

      /**
       * remove an action again
       */
      function remove_hook(hook, action) {
        if (hook in registry) {
          for (var i = 0; i < registry[hook].length; i++) {
            if (registry[hook][i] === action) {
              registry[hook].splice(i, 1);
              break;
            }
          }
        }
      }
      /**
       * add an action to a hook
       */
      function add_hook(hook, action, position) {
        if (!(hook in registry)) {
          registry[hook] = [];
        }
        if (position === undefined) {
          position = registry[hook].length;
        }
        registry[hook].splice(position, 0, action);
      }

      /**
       * return either the data of a hook call or the result of action, if the
       * former is undefined
       *
       * @return function a function wrapper around action
       */
      function return_hook_or(hook, action) {
        return function() {
          var data = call_hook(hook, Array.prototype.slice.call(arguments));

          if (data !== undefined) {
            return data;
          }

          return action.apply(this, arguments);
        };
      }

      /* the following code is borrowed from the WebComponents project, licensed
       * under the BSD license. Source:
       * <https://github.com/webcomponents/webcomponentsjs/blob/5283db1459fa2323e5bfc8b9b5cc1753ed85e3d0/src/WebComponents/dom.js#L53-L78>
       */
      // defaultPrevented is broken in IE.
      // https://connect.microsoft.com/IE/feedback/details/790389/event-defaultprevented-returns-false-after-preventdefault-was-called

      var workingDefaultPrevented = function() {
        var e = document.createEvent('Event');
        e.initEvent('foo', true, true);
        e.preventDefault();
        return e.defaultPrevented;
      }();

      if (!workingDefaultPrevented) {
        (function() {
          var origPreventDefault = window.Event.prototype.preventDefault;
          window.Event.prototype.preventDefault = function() {
            if (!this.cancelable) {
              return;
            }

            origPreventDefault.call(this);

            Object.defineProperty(this, 'defaultPrevented', {
              get: function get() {
                return true;
              },
              configurable: true
            });
          };
        })();
      }
      /* end of borrowed code */

      function create_event(name) {
        var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var _ref$bubbles = _ref.bubbles;
        var bubbles = _ref$bubbles === undefined ? true : _ref$bubbles;
        var _ref$cancelable = _ref.cancelable;
        var cancelable = _ref$cancelable === undefined ? false : _ref$cancelable;

        var event = document.createEvent('Event');
        event.initEvent(name, bubbles, cancelable);
        return event;
      }

      function trigger_event(element, event) {
        var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        var _ref2$bubbles = _ref2.bubbles;
        var bubbles = _ref2$bubbles === undefined ? true : _ref2$bubbles;
        var _ref2$cancelable = _ref2.cancelable;
        var cancelable = _ref2$cancelable === undefined ? false : _ref2$cancelable;
        var payload = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

        if (!(event instanceof window.Event)) {
          event = create_event(event, {
            bubbles: bubbles,
            cancelable: cancelable
          });
        }

        for (var key in payload) {
          if (payload.hasOwnProperty(key)) {
            event[key] = payload[key];
          }
        }

        element.dispatchEvent(event);

        return event;
      }

      /* and datetime-local? Spec says “Nah!” */

      var dates = ['datetime', 'date', 'month', 'week', 'time'];

      var plain_numbers = ['number', 'range'];

      /* everything that returns something meaningful for valueAsNumber and
       * can have the step attribute */
      var numbers = dates.concat(plain_numbers, 'datetime-local');

      /* the spec says to only check those for syntax in validity.typeMismatch.
       * ¯\_(ツ)_/¯ */
      var type_checked = ['email', 'url'];

      /* check these for validity.badInput */
      var input_checked = ['email', 'date', 'month', 'week', 'time', 'datetime', 'datetime-local', 'number', 'range', 'color'];

      var text_types = ['text', 'search', 'tel', 'password'].concat(type_checked);

      /* input element types, that are candidates for the validation API.
       * Missing from this set are: button, hidden, menu (from <button>), reset and
       * the types for non-<input> elements. */
      var validation_candidates = ['checkbox', 'color', 'file', 'image', 'radio', 'submit'].concat(numbers, text_types);

      /* all known types of <input> */
      var inputs = ['button', 'hidden', 'reset'].concat(validation_candidates);

      /* apparently <select> and <textarea> have types of their own */
      var non_inputs = ['select-one', 'select-multiple', 'textarea'];

      /* shim layer for the Element.matches method */

      var ep = window.Element.prototype;
      var native_matches = ep.matches || ep.matchesSelector || ep.msMatchesSelector || ep.webkitMatchesSelector;

      function matches(element, selector) {
        return native_matches.call(element, selector);
      }

      /**
       * mark an object with a '__hyperform=true' property
       *
       * We use this to distinguish our properties from the native ones. Usage:
       * js> mark(obj);
       * js> assert(obj.__hyperform === true)
       */

      function mark(obj) {
        if (['object', 'function'].indexOf(typeof obj) > -1) {
          delete obj.__hyperform;
          Object.defineProperty(obj, '__hyperform', {
            configurable: true,
            enumerable: false,
            value: true
          });
        }

        return obj;
      }

      /**
       * the internal storage for messages
       */
      var store = new WeakMap();

      /* jshint -W053 */
      var message_store = {
        set: function set(element, message) {
          var is_custom = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

          if (element instanceof window.HTMLFieldSetElement) {
            var wrapped_form = get_wrapper(element);
            if (wrapped_form && !wrapped_form.settings.extendFieldset) {
              /* make this a no-op for <fieldset> in strict mode */
              return message_store;
            }
          }

          if (typeof message === 'string') {
            message = new String(message);
          }
          if (is_custom) {
            message.is_custom = true;
          }
          mark(message);
          store.set(element, message);

          /* allow the :invalid selector to match */
          if ('_original_setCustomValidity' in element) {
            element._original_setCustomValidity(message.toString());
          }

          return message_store;
        },
        get: function get(element) {
          var message = store.get(element);
          if (message === undefined && '_original_validationMessage' in element) {
            /* get the browser's validation message, if we have none. Maybe it
             * knows more than we. */
            message = new String(element._original_validationMessage);
          }
          return message ? message : new String('');
        },
        delete: function _delete(element) {
          if ('_original_setCustomValidity' in element) {
            element._original_setCustomValidity('');
          }
          return store.delete(element);
        }
      };

      /**
       * counter that will be incremented with every call
       *
       * Will enforce uniqueness, as long as no more than 1 hyperform scripts
       * are loaded. (In that case we still have the "random" part below.)
       */

      var uid = 0;

      /**
       * generate a random ID
       *
       * @see https://gist.github.com/gordonbrander/2230317
       */
      function generate_id() {
        var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'hf_';

        return prefix + uid+++Math.random().toString(36).substr(2);
      }

      var warningsCache = new WeakMap();

      var DefaultRenderer = {

        /**
         * called when a warning should become visible
         */
        attachWarning: function attachWarning(warning, element) {
          /* should also work, if element is last,
           * http://stackoverflow.com/a/4793630/113195 */
          element.parentNode.insertBefore(warning, element.nextSibling);
        },

        /**
         * called when a warning should vanish
         */
        detachWarning: function detachWarning(warning, element) {
          warning.parentNode.removeChild(warning);
        },

        /**
         * called when feedback to an element's state should be handled
         *
         * i.e., showing and hiding warnings
         */
        showWarning: function showWarning(element) {
          var sub_radio = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

          var msg = message_store.get(element).toString();
          var warning = warningsCache.get(element);

          if (msg) {
            if (!warning) {
              var wrapper = get_wrapper(element);
              warning = document.createElement('div');
              warning.className = wrapper && wrapper.settings.classes.warning || 'hf-warning';
              warning.id = generate_id();
              warning.setAttribute('aria-live', 'polite');
              warningsCache.set(element, warning);
            }

            element.setAttribute('aria-errormessage', warning.id);
            warning.textContent = msg;
            Renderer.attachWarning(warning, element);
          } else if (warning && warning.parentNode) {
            element.removeAttribute('aria-errormessage');
            Renderer.detachWarning(warning, element);
          }

          if (!sub_radio && element.type === 'radio' && element.form) {
            /* render warnings for all other same-name radios, too */
            Array.prototype.filter.call(document.getElementsByName(element.name), function(radio) {
              return radio.name === element.name && radio.form === element.form;
            }).map(function(radio) {
              return Renderer.showWarning(radio, 'sub_radio');
            });
          }
        }

      };

      var Renderer = {

        attachWarning: DefaultRenderer.attachWarning,
        detachWarning: DefaultRenderer.detachWarning,
        showWarning: DefaultRenderer.showWarning,

        set: function set(renderer, action) {
          if (renderer.indexOf('_') > -1) {
            /* global console */
            // TODO delete before next non-patch version
            console.log('Renderer.set: please use camelCase names. ' + renderer + ' will be removed in the next non-patch release.');
            renderer = renderer.replace(/_([a-z])/g, function(g) {
              return g[1].toUpperCase();
            });
          }
          if (!action) {
            action = DefaultRenderer[renderer];
          }
          Renderer[renderer] = action;
        }

      };

      /**
       * check element's validity and report an error back to the user
       */
      function reportValidity(element) {
        /* if this is a <form>, report validity of all child inputs */
        if (element instanceof window.HTMLFormElement) {
          return Array.prototype.map.call(element.elements, reportValidity).every(function(b) {
            return b;
          });
        }

        /* we copy checkValidity() here, b/c we have to check if the "invalid"
         * event was canceled. */
        var valid = ValidityState(element).valid;
        var event;
        if (valid) {
          var wrapped_form = get_wrapper(element);
          if (wrapped_form && wrapped_form.settings.validEvent) {
            event = trigger_event(element, 'valid', {
              cancelable: true
            });
          }
        } else {
          event = trigger_event(element, 'invalid', {
            cancelable: true
          });
        }

        if (!event || !event.defaultPrevented) {
          Renderer.showWarning(element);
        }

        return valid;
      }

      /**
       * submit a form, because `element` triggered it
       *
       * This method also dispatches a submit event on the form prior to the
       * submission. The event contains the trigger element as `submittedVia`.
       *
       * If the element is a button with a name, the name=value pair will be added
       * to the submitted data.
       */
      function submit_form_via(element) {
        /* apparently, the submit event is not triggered in most browsers on
         * the submit() method, so we do it manually here to model a natural
         * submit as closely as possible.
         * Now to the fun fact: If you trigger a submit event from a form, what
         * do you think should happen?
         * 1) the form will be automagically submitted by the browser, or
         * 2) nothing.
         * And as you already suspected, the correct answer is: both! Firefox
         * opts for 1), Chrome for 2). Yay! */
        var event_got_cancelled;

        var submit_event = create_event('submit', {
          cancelable: true
        });
        /* force Firefox to not submit the form, then fake preventDefault() */
        submit_event.preventDefault();
        Object.defineProperty(submit_event, 'defaultPrevented', {
          value: false,
          writable: true
        });
        Object.defineProperty(submit_event, 'preventDefault', {
          value: function value() {
            return submit_event.defaultPrevented = event_got_cancelled = true;
          },
          writable: true
        });
        trigger_event(element.form, submit_event, {}, {
          submittedVia: element
        });

        if (!event_got_cancelled) {
          add_submit_field(element);
          window.HTMLFormElement.prototype.submit.call(element.form);
          window.setTimeout(function() {
            return remove_submit_field(element);
          });
        }
      }

      /**
       * if a submit button was clicked, add its name=value by means of a type=hidden
       * input field
       */
      function add_submit_field(button) {
        if (['image', 'submit'].indexOf(button.type) > -1 && button.name) {
          var wrapper = get_wrapper(button.form) || {};
          var submit_helper = wrapper.submit_helper;
          if (submit_helper) {
            if (submit_helper.parentNode) {
              submit_helper.parentNode.removeChild(submit_helper);
            }
          } else {
            submit_helper = document.createElement('input');
            submit_helper.type = 'hidden';
            wrapper.submit_helper = submit_helper;
          }
          submit_helper.name = button.name;
          submit_helper.value = button.value;
          button.form.appendChild(submit_helper);
        }
      }

      /**
       * remove a possible helper input, that was added by `add_submit_field`
       */
      function remove_submit_field(button) {
        if (['image', 'submit'].indexOf(button.type) > -1 && button.name) {
          var wrapper = get_wrapper(button.form) || {};
          var submit_helper = wrapper.submit_helper;
          if (submit_helper && submit_helper.parentNode) {
            submit_helper.parentNode.removeChild(submit_helper);
          }
        }
      }

      /**
       * check a form's validity and submit it
       *
       * The method triggers a cancellable `validate` event on the form. If the
       * event is cancelled, form submission will be aborted, too.
       *
       * If the form is found to contain invalid fields, focus the first field.
       */
      function check(button) {
        /* trigger a "validate" event on the form to be submitted */
        var val_event = trigger_event(button.form, 'validate', {
          cancelable: true
        });
        if (val_event.defaultPrevented) {
          /* skip the whole submit thing, if the validation is canceled. A user
           * can still call form.submit() afterwards. */
          return;
        }

        var valid = true;
        var first_invalid;
        Array.prototype.map.call(button.form.elements, function(element) {
          if (!reportValidity(element)) {
            valid = false;
            if (!first_invalid && 'focus' in element) {
              first_invalid = element;
            }
          }
        });

        if (valid) {
          submit_form_via(button);
        } else if (first_invalid) {
          /* focus the first invalid element, if validation went south */
          first_invalid.focus();
          /* tell the tale, if anyone wants to react to it */
          trigger_event(button.form, 'forminvalid');
        }
      }

      /**
       * test if node is a submit button
       */
      function is_submit_button(node) {
        return (
          /* must be an input or button element... */
          (node.nodeName === 'INPUT' || node.nodeName === 'BUTTON') && (

            /* ...and have a submitting type */
            node.type === 'image' || node.type === 'submit')
        );
      }

      /**
       * test, if the click event would trigger a submit
       */
      function is_submitting_click(event, button) {
        return (
          /* prevented default: won't trigger a submit */
          !event.defaultPrevented && (

            /* left button or middle button (submits in Chrome) */
            !('button' in event) || event.button < 2) &&

          /* must be a submit button... */
          is_submit_button(button) &&

          /* the button needs a form, that's going to be submitted */
          button.form &&

          /* again, if the form should not be validated, we're out of the game */
          !button.form.hasAttribute('novalidate')
        );
      }

      /**
       * test, if the keypress event would trigger a submit
       */
      function is_submitting_keypress(event) {
        return (
          /* prevented default: won't trigger a submit */
          !event.defaultPrevented && (
            /* ...and <Enter> was pressed... */
            event.keyCode === 13 &&

            /* ...on an <input> that is... */
            event.target.nodeName === 'INPUT' &&

            /* ...a standard text input field (not checkbox, ...) */
            text_types.indexOf(event.target.type) > -1 ||
            /* or <Enter> or <Space> was pressed... */
            (event.keyCode === 13 || event.keyCode === 32) &&

            /* ...on a submit button */
            is_submit_button(event.target)) &&

          /* there's a form... */
          event.target.form &&

          /* ...and the form allows validation */
          !event.target.form.hasAttribute('novalidate')
        );
      }

      /**
       * catch clicks to children of <button>s
       */
      function get_clicked_button(element) {
        if (is_submit_button(element)) {
          return element;
        } else if (matches(element, 'button:not([type]) *, button[type="submit"] *')) {
          return get_clicked_button(element.parentNode);
        } else {
          return null;
        }
      }

      /**
       * return event handler to catch explicit submission by click on a button
       */
      function get_click_handler() {
        var ignore = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        return function(event) {
          var button = get_clicked_button(event.target);
          if (button && is_submitting_click(event, button)) {
            event.preventDefault();
            if (ignore || button.hasAttribute('formnovalidate')) {
              /* if validation should be ignored, we're not interested in any checks */
              submit_form_via(button);
            } else {
              check(button);
            }
          }
        };
      }
      var click_handler = get_click_handler();
      var ignored_click_handler = get_click_handler(true);

      /**
       * catch implicit submission by pressing <Enter> in some situations
       */
      function get_keypress_handler(ignore) {
        return function keypress_handler(event) {
          if (is_submitting_keypress(event)) {
            event.preventDefault();

            var wrapper = get_wrapper(event.target.form) || {
              settings: {}
            };
            if (wrapper.settings.preventImplicitSubmit) {
              /* user doesn't want an implicit submit. Cancel here. */
              return;
            }

            /* check, that there is no submit button in the form. Otherwise
             * that should be clicked. */
            var el = event.target.form.elements.length;
            var submit;
            for (var i = 0; i < el; i++) {
              if (['image', 'submit'].indexOf(event.target.form.elements[i].type) > -1) {
                submit = event.target.form.elements[i];
                break;
              }
            }

            if (submit) {
              submit.click();
            } else if (ignore) {
              submit_form_via(event.target);
            } else {
              check(event.target);
            }
          }
        };
      }
      var keypress_handler = get_keypress_handler();
      var ignored_keypress_handler = get_keypress_handler(true);

      /**
       * catch all relevant events _prior_ to a form being submitted
       *
       * @param bool ignore bypass validation, when an attempt to submit the
       *                    form is detected. True, when the wrapper's revalidate
       *                    setting is 'never'.
       */
      function catch_submit(listening_node) {
        var ignore = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

        if (ignore) {
          listening_node.addEventListener('click', ignored_click_handler);
          listening_node.addEventListener('keypress', ignored_keypress_handler);
        } else {
          listening_node.addEventListener('click', click_handler);
          listening_node.addEventListener('keypress', keypress_handler);
        }
      }

      /**
       * decommission the event listeners from catch_submit() again
       */
      function uncatch_submit(listening_node) {
        listening_node.removeEventListener('click', ignored_click_handler);
        listening_node.removeEventListener('keypress', ignored_keypress_handler);
        listening_node.removeEventListener('click', click_handler);
        listening_node.removeEventListener('keypress', keypress_handler);
      }

      /**
       * remove `property` from element and restore _original_property, if present
       */
      function uninstall_property(element, property) {
        try {
          delete element[property];
        } catch (e) {
          /* Safari <= 9 and PhantomJS will end up here :-( Nothing to do except
           * warning */
          var wrapper = get_wrapper(element);
          if (wrapper && wrapper.settings.debug) {
            /* global console */
            console.log('[hyperform] cannot uninstall custom property ' + property);
          }
          return false;
        }

        var original_descriptor = Object.getOwnPropertyDescriptor(element, '_original_' + property);

        if (original_descriptor) {
          Object.defineProperty(element, property, original_descriptor);
        }
      }

      /**
       * add `property` to an element
       *
       * js> installer(element, 'foo', { value: 'bar' });
       * js> assert(element.foo === 'bar');
       */
      function install_property(element, property, descriptor) {
        descriptor.configurable = true;
        descriptor.enumerable = true;
        if ('value' in descriptor) {
          descriptor.writable = true;
        }

        var original_descriptor = Object.getOwnPropertyDescriptor(element, property);

        if (original_descriptor) {

          if (original_descriptor.configurable === false) {
            /* Safari <= 9 and PhantomJS will end up here :-( Nothing to do except
             * warning */
            var wrapper = get_wrapper(element);
            if (wrapper && wrapper.settings.debug) {
              /* global console */
              console.log('[hyperform] cannot install custom property ' + property);
            }
            return false;
          }

          /* we already installed that property... */
          if (original_descriptor.get && original_descriptor.get.__hyperform || original_descriptor.value && original_descriptor.value.__hyperform) {
            return;
          }

          /* publish existing property under new name, if it's not from us */
          Object.defineProperty(element, '_original_' + property, original_descriptor);
        }

        delete element[property];
        Object.defineProperty(element, property, descriptor);

        return true;
      }

      function is_field(element) {
        return element instanceof window.HTMLButtonElement || element instanceof window.HTMLInputElement || element instanceof window.HTMLSelectElement || element instanceof window.HTMLTextAreaElement || element instanceof window.HTMLFieldSetElement || element === window.HTMLButtonElement.prototype || element === window.HTMLInputElement.prototype || element === window.HTMLSelectElement.prototype || element === window.HTMLTextAreaElement.prototype || element === window.HTMLFieldSetElement.prototype;
      }

      /**
       * set a custom validity message or delete it with an empty string
       */
      function setCustomValidity(element, msg) {
        message_store.set(element, msg, true);
      }

      function sprintf(str) {
        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        var args_length = args.length;
        var global_index = 0;

        return str.replace(/%([0-9]+\$)?([sl])/g, function(match, position, type) {
          var local_index = global_index;
          if (position) {
            local_index = Number(position.replace(/\$$/, '')) - 1;
          }
          global_index += 1;

          var arg = '';
          if (args_length > local_index) {
            arg = args[local_index];
          }

          if (arg instanceof Date || typeof arg === 'number' || arg instanceof Number) {
            /* try getting a localized representation of dates and numbers, if the
             * browser supports this */
            if (type === 'l') {
              arg = (arg.toLocaleString || arg.toString).call(arg);
            } else {
              arg = arg.toString();
            }
          }

          return arg;
        });
      }

      /* For a given date, get the ISO week number
       *
       * Source: http://stackoverflow.com/a/6117889/113195
       *
       * Based on information at:
       *
       *    http://www.merlyn.demon.co.uk/weekcalc.htm#WNR
       *
       * Algorithm is to find nearest thursday, it's year
       * is the year of the week number. Then get weeks
       * between that date and the first day of that year.
       *
       * Note that dates in one year can be weeks of previous
       * or next year, overlap is up to 3 days.
       *
       * e.g. 2014/12/29 is Monday in week  1 of 2015
       *      2012/1/1   is Sunday in week 52 of 2011
       */

      function get_week_of_year(d) {
        /* Copy date so don't modify original */
        d = new Date(+d);
        d.setUTCHours(0, 0, 0);
        /* Set to nearest Thursday: current date + 4 - current day number
         * Make Sunday's day number 7 */
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
        /* Get first day of year */
        var yearStart = new Date(d.getUTCFullYear(), 0, 1);
        /* Calculate full weeks to nearest Thursday */
        var weekNo = Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
        /* Return array of year and week number */
        return [d.getUTCFullYear(), weekNo];
      }

      function pad(num) {
        var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

        var s = num + '';
        while (s.length < size) {
          s = '0' + s;
        }
        return s;
      }

      /**
       * calculate a string from a date according to HTML5
       */
      function date_to_string(date, element_type) {
        if (!(date instanceof Date)) {
          return null;
        }

        switch (element_type) {
          case 'datetime':
            return date_to_string(date, 'date') + 'T' + date_to_string(date, 'time');

          case 'datetime-local':
            return sprintf('%s-%s-%sT%s:%s:%s.%s', date.getFullYear(), pad(date.getMonth() + 1), pad(date.getDate()), pad(date.getHours()), pad(date.getMinutes()), pad(date.getSeconds()), pad(date.getMilliseconds(), 3)).replace(/(:00)?\.000$/, '');

          case 'date':
            return sprintf('%s-%s-%s', date.getUTCFullYear(), pad(date.getUTCMonth() + 1), pad(date.getUTCDate()));

          case 'month':
            return sprintf('%s-%s', date.getUTCFullYear(), pad(date.getUTCMonth() + 1));

          case 'week':
            var params = get_week_of_year(date);
            return sprintf.call(null, '%s-W%s', params[0], pad(params[1]));

          case 'time':
            return sprintf('%s:%s:%s.%s', pad(date.getUTCHours()), pad(date.getUTCMinutes()), pad(date.getUTCSeconds()), pad(date.getUTCMilliseconds(), 3)).replace(/(:00)?\.000$/, '');
        }

        return null;
      }

      /**
       * return a new Date() representing the ISO date for a week number
       *
       * @see http://stackoverflow.com/a/16591175/113195
       */

      function get_date_from_week(week, year) {
        var date = new Date(Date.UTC(year, 0, 1 + (week - 1) * 7));

        if (date.getUTCDay() <= 4 /* thursday */ ) {
          date.setUTCDate(date.getUTCDate() - date.getUTCDay() + 1);
        } else {
          date.setUTCDate(date.getUTCDate() + 8 - date.getUTCDay());
        }

        return date;
      }

      /**
       * calculate a date from a string according to HTML5
       */
      function string_to_date(string, element_type) {
        var date = new Date(0);
        var ms;
        switch (element_type) {
          case 'datetime':
            if (!/^([0-9]{4,})-([0-9]{2})-([0-9]{2})T([01][0-9]|2[0-3]):([0-5][0-9])(?::([0-5][0-9])(?:\.([0-9]{1,3}))?)?$/.test(string)) {
              return null;
            }
            ms = RegExp.$7 || '000';
            while (ms.length < 3) {
              ms += '0';
            }
            date.setUTCFullYear(Number(RegExp.$1));
            date.setUTCMonth(Number(RegExp.$2) - 1, Number(RegExp.$3));
            date.setUTCHours(Number(RegExp.$4), Number(RegExp.$5), Number(RegExp.$6 || 0), Number(ms));
            return date;

          case 'date':
            if (!/^([0-9]{4,})-([0-9]{2})-([0-9]{2})$/.test(string)) {
              return null;
            }
            date.setUTCFullYear(Number(RegExp.$1));
            date.setUTCMonth(Number(RegExp.$2) - 1, Number(RegExp.$3));
            return date;

          case 'month':
            if (!/^([0-9]{4,})-([0-9]{2})$/.test(string)) {
              return null;
            }
            date.setUTCFullYear(Number(RegExp.$1));
            date.setUTCMonth(Number(RegExp.$2) - 1, 1);
            return date;

          case 'week':
            if (!/^([0-9]{4,})-W(0[1-9]|[1234][0-9]|5[0-3])$/.test(string)) {
              return null;
            }
            return get_date_from_week(Number(RegExp.$2), Number(RegExp.$1));

          case 'time':
            if (!/^([01][0-9]|2[0-3]):([0-5][0-9])(?::([0-5][0-9])(?:\.([0-9]{1,3}))?)?$/.test(string)) {
              return null;
            }
            ms = RegExp.$4 || '000';
            while (ms.length < 3) {
              ms += '0';
            }
            date.setUTCHours(Number(RegExp.$1), Number(RegExp.$2), Number(RegExp.$3 || 0), Number(ms));
            return date;
        }

        return null;
      }

      /**
       * calculate a date from a string according to HTML5
       */
      function string_to_number(string, element_type) {
        var rval = string_to_date(string, element_type);
        if (rval !== null) {
          return +rval;
        }
        /* not parseFloat, because we want NaN for invalid values like "1.2xxy" */
        return Number(string);
      }

      /**
       * get the element's type in a backwards-compatible way
       */
      function get_type(element) {
        if (element instanceof window.HTMLTextAreaElement) {
          return 'textarea';
        } else if (element instanceof window.HTMLSelectElement) {
          return element.hasAttribute('multiple') ? 'select-multiple' : 'select-one';
        } else if (element instanceof window.HTMLButtonElement) {
          return (element.getAttribute('type') || 'submit').toLowerCase();
        } else if (element instanceof window.HTMLInputElement) {
          var attr = (element.getAttribute('type') || '').toLowerCase();
          if (attr && inputs.indexOf(attr) > -1) {
            return attr;
          } else {
            /* perhaps the DOM has in-depth knowledge. Take that before returning
             * 'text'. */
            return element.type || 'text';
          }
        }

        return '';
      }

      /**
       * the following validation messages are from Firefox source,
       * http://mxr.mozilla.org/mozilla-central/source/dom/locales/en-US/chrome/dom/dom.properties
       * released under MPL license, http://mozilla.org/MPL/2.0/.
       */

      var catalog = {
        en: {
          TextTooLong: 'Please shorten this text to %l characters or less (you are currently using %l characters).',
          ValueMissing: 'Please fill out this field.',
          CheckboxMissing: 'Please check this box if you want to proceed.',
          RadioMissing: 'Please select one of these options.',
          FileMissing: 'Please select a file.',
          SelectMissing: 'Please select an item in the list.',
          InvalidEmail: 'Please enter an email address.',
          InvalidURL: 'Please enter a URL.',
          PatternMismatch: 'Please match the requested format.',
          PatternMismatchWithTitle: 'Please match the requested format: %l.',
          NumberRangeOverflow: 'Please select a value that is no more than %l.',
          DateRangeOverflow: 'Please select a value that is no later than %l.',
          TimeRangeOverflow: 'Please select a value that is no later than %l.',
          NumberRangeUnderflow: 'Please select a value that is no less than %l.',
          DateRangeUnderflow: 'Please select a value that is no earlier than %l.',
          TimeRangeUnderflow: 'Please select a value that is no earlier than %l.',
          StepMismatch: 'Please select a valid value. The two nearest valid values are %l and %l.',
          StepMismatchOneValue: 'Please select a valid value. The nearest valid value is %l.',
          BadInputNumber: 'Please enter a number.'
        }
      };

      /**
       * the global language Hyperform will use
       */
      var language = 'en';

      /**
       * the base language according to BCP47, i.e., only the piece before the first hyphen
       */
      var base_lang = 'en';

      /**
       * set the language for Hyperform’s messages
       */
      function set_language(newlang) {
        language = newlang;
        base_lang = newlang.replace(/[-_].*/, '');
      }

      /**
       * add a lookup catalog "string: translation" for a language
       */
      function add_translation(lang, new_catalog) {
        if (!(lang in catalog)) {
          catalog[lang] = {};
        }
        for (var key in new_catalog) {
          if (new_catalog.hasOwnProperty(key)) {
            catalog[lang][key] = new_catalog[key];
          }
        }
      }

      /**
       * return `s` translated into the current language
       *
       * Defaults to the base language and then English if the former has no
       * translation for `s`.
       */
      function _(s) {
        if (language in catalog && s in catalog[language]) {
          return catalog[language][s];
        } else if (base_lang in catalog && s in catalog[base_lang]) {
          return catalog[base_lang][s];
        } else if (s in catalog.en) {
          return catalog.en[s];
        }
        return s;
      }

      var default_step = {
        'datetime-local': 60,
        datetime: 60,
        time: 60
      };

      var step_scale_factor = {
        'datetime-local': 1000,
        datetime: 1000,
        date: 86400000,
        week: 604800000,
        time: 1000
      };

      var default_step_base = {
        week: -259200000
      };

      var default_min = {
        range: 0
      };

      var default_max = {
        range: 100
      };

      /**
       * get previous and next valid values for a stepped input element
       */
      function get_next_valid(element) {
        var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

        var type = get_type(element);

        var aMin = element.getAttribute('min');
        var min = default_min[type] || NaN;
        if (aMin) {
          var pMin = string_to_number(aMin, type);
          if (!isNaN(pMin)) {
            min = pMin;
          }
        }

        var aMax = element.getAttribute('max');
        var max = default_max[type] || NaN;
        if (aMax) {
          var pMax = string_to_number(aMax, type);
          if (!isNaN(pMax)) {
            max = pMax;
          }
        }

        var aStep = element.getAttribute('step');
        var step = default_step[type] || 1;
        if (aStep && aStep.toLowerCase() === 'any') {
          /* quick return: we cannot calculate prev and next */
          return [_('any value'), _('any value')];
        } else if (aStep) {
          var pStep = string_to_number(aStep, type);
          if (!isNaN(pStep)) {
            step = pStep;
          }
        }

        var default_value = string_to_number(element.getAttribute('value'), type);

        var value = string_to_number(element.value || element.getAttribute('value'), type);

        if (isNaN(value)) {
          /* quick return: we cannot calculate without a solid base */
          return [_('any valid value'), _('any valid value')];
        }

        var step_base = !isNaN(min) ? min : !isNaN(default_value) ? default_value : default_step_base[type] || 0;

        var scale = step_scale_factor[type] || 1;

        var prev = step_base + Math.floor((value - step_base) / (step * scale)) * (step * scale) * n;
        var next = step_base + (Math.floor((value - step_base) / (step * scale)) + 1) * (step * scale) * n;

        if (prev < min) {
          prev = null;
        } else if (prev > max) {
          prev = max;
        }

        if (next > max) {
          next = null;
        } else if (next < min) {
          next = min;
        }

        /* convert to date objects, if appropriate */
        if (dates.indexOf(type) > -1) {
          prev = date_to_string(new Date(prev), type);
          next = date_to_string(new Date(next), type);
        }

        return [prev, next];
      }

      /**
       * implement the valueAsDate functionality
       *
       * @see https://html.spec.whatwg.org/multipage/forms.html#dom-input-valueasdate
       */
      function valueAsDate(element) {
        var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

        var type = get_type(element);
        if (dates.indexOf(type) > -1) {
          if (value !== undefined) {
            /* setter: value must be null or a Date() */
            if (value === null) {
              element.value = '';
            } else if (value instanceof Date) {
              if (isNaN(value.getTime())) {
                element.value = '';
              } else {
                element.value = date_to_string(value, type);
              }
            } else {
              throw new window.DOMException('valueAsDate setter encountered invalid value', 'TypeError');
            }
            return;
          }

          var value_date = string_to_date(element.value, type);
          return value_date instanceof Date ? value_date : null;
        } else if (value !== undefined) {
          /* trying to set a date on a not-date input fails */
          throw new window.DOMException('valueAsDate setter cannot set date on this element', 'InvalidStateError');
        }

        return null;
      }

      /**
       * implement the valueAsNumber functionality
       *
       * @see https://html.spec.whatwg.org/multipage/forms.html#dom-input-valueasnumber
       */
      function valueAsNumber(element) {
        var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

        var type = get_type(element);
        if (numbers.indexOf(type) > -1) {
          if (type === 'range' && element.hasAttribute('multiple')) {
            /* @see https://html.spec.whatwg.org/multipage/forms.html#do-not-apply */
            return NaN;
          }

          if (value !== undefined) {
            /* setter: value must be NaN or a finite number */
            if (isNaN(value)) {
              element.value = '';
            } else if (typeof value === 'number' && window.isFinite(value)) {
              try {
                /* try setting as a date, but... */
                valueAsDate(element, new Date(value));
              } catch (e) {
                /* ... when valueAsDate is not responsible, ... */
                if (!(e instanceof window.DOMException)) {
                  throw e;
                }
                /* ... set it via Number.toString(). */
                element.value = value.toString();
              }
            } else {
              throw new window.DOMException('valueAsNumber setter encountered invalid value', 'TypeError');
            }
            return;
          }

          return string_to_number(element.value, type);
        } else if (value !== undefined) {
          /* trying to set a number on a not-number input fails */
          throw new window.DOMException('valueAsNumber setter cannot set number on this element', 'InvalidStateError');
        }

        return NaN;
      }

      /**
       *
       */
      function stepDown(element) {
        var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

        if (numbers.indexOf(get_type(element)) === -1) {
          throw new window.DOMException('stepDown encountered invalid type', 'InvalidStateError');
        }
        if ((element.getAttribute('step') || '').toLowerCase() === 'any') {
          throw new window.DOMException('stepDown encountered step "any"', 'InvalidStateError');
        }

        var prev = get_next_valid(element, n)[0];

        if (prev !== null) {
          valueAsNumber(element, prev);
        }
      }

      /**
       *
       */
      function stepUp(element) {
        var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

        if (numbers.indexOf(get_type(element)) === -1) {
          throw new window.DOMException('stepUp encountered invalid type', 'InvalidStateError');
        }
        if ((element.getAttribute('step') || '').toLowerCase() === 'any') {
          throw new window.DOMException('stepUp encountered step "any"', 'InvalidStateError');
        }

        var next = get_next_valid(element, n)[1];

        if (next !== null) {
          valueAsNumber(element, next);
        }
      }

      /**
       * get the validation message for an element, empty string, if the element
       * satisfies all constraints.
       */
      function validationMessage(element) {
        var msg = message_store.get(element);
        if (!msg) {
          return '';
        }

        /* make it a primitive again, since message_store returns String(). */
        return msg.toString();
      }

      /**
       * check, if an element will be subject to HTML5 validation at all
       */
      function willValidate(element) {
        return is_validation_candidate(element);
      }

      var gA = function gA(prop) {
        return function() {
          return do_filter('attr_get_' + prop, this.getAttribute(prop), this);
        };
      };

      var sA = function sA(prop) {
        return function(value) {
          this.setAttribute(prop, do_filter('attr_set_' + prop, value, this));
        };
      };

      var gAb = function gAb(prop) {
        return function() {
          return do_filter('attr_get_' + prop, this.hasAttribute(prop), this);
        };
      };

      var sAb = function sAb(prop) {
        return function(value) {
          if (do_filter('attr_set_' + prop, value, this)) {
            this.setAttribute(prop, prop);
          } else {
            this.removeAttribute(prop);
          }
        };
      };

      var gAn = function gAn(prop) {
        return function() {
          return do_filter('attr_get_' + prop, Math.max(0, Number(this.getAttribute(prop))), this);
        };
      };

      var sAn = function sAn(prop) {
        return function(value) {
          value = do_filter('attr_set_' + prop, value, this);
          if (/^[0-9]+$/.test(value)) {
            this.setAttribute(prop, value);
          }
        };
      };

      function install_properties(element) {
        var _arr = ['accept', 'max', 'min', 'pattern', 'placeholder', 'step'];

        for (var _i = 0; _i < _arr.length; _i++) {
          var prop = _arr[_i];
          install_property(element, prop, {
            get: gA(prop),
            set: sA(prop)
          });
        }

        var _arr2 = ['multiple', 'required', 'readOnly'];
        for (var _i2 = 0; _i2 < _arr2.length; _i2++) {
          var _prop = _arr2[_i2];
          install_property(element, _prop, {
            get: gAb(_prop.toLowerCase()),
            set: sAb(_prop.toLowerCase())
          });
        }

        var _arr3 = ['minLength', 'maxLength'];
        for (var _i3 = 0; _i3 < _arr3.length; _i3++) {
          var _prop2 = _arr3[_i3];
          install_property(element, _prop2, {
            get: gAn(_prop2.toLowerCase()),
            set: sAn(_prop2.toLowerCase())
          });
        }
      }

      function uninstall_properties(element) {
        var _arr4 = ['accept', 'max', 'min', 'pattern', 'placeholder', 'step', 'multiple', 'required', 'readOnly', 'minLength', 'maxLength'];

        for (var _i4 = 0; _i4 < _arr4.length; _i4++) {
          var prop = _arr4[_i4];
          uninstall_property(element, prop);
        }
      }

      var polyfills = {
        checkValidity: {
          value: mark(function() {
            return checkValidity(this);
          })
        },
        reportValidity: {
          value: mark(function() {
            return reportValidity(this);
          })
        },
        setCustomValidity: {
          value: mark(function(msg) {
            return setCustomValidity(this, msg);
          })
        },
        stepDown: {
          value: mark(function() {
            var n = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            return stepDown(this, n);
          })
        },
        stepUp: {
          value: mark(function() {
            var n = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            return stepUp(this, n);
          })
        },
        validationMessage: {
          get: mark(function() {
            return validationMessage(this);
          })
        },
        validity: {
          get: mark(function() {
            return ValidityState(this);
          })
        },
        valueAsDate: {
          get: mark(function() {
            return valueAsDate(this);
          }),
          set: mark(function(value) {
            valueAsDate(this, value);
          })
        },
        valueAsNumber: {
          get: mark(function() {
            return valueAsNumber(this);
          }),
          set: mark(function(value) {
            valueAsNumber(this, value);
          })
        },
        willValidate: {
          get: mark(function() {
            return willValidate(this);
          })
        }
      };

      function polyfill(element) {
        if (is_field(element)) {

          for (var prop in polyfills) {
            install_property(element, prop, polyfills[prop]);
          }

          install_properties(element);
        } else if (element instanceof window.HTMLFormElement || element === window.HTMLFormElement.prototype) {
          install_property(element, 'checkValidity', polyfills.checkValidity);
          install_property(element, 'reportValidity', polyfills.reportValidity);
        }
      }

      function polyunfill(element) {
        if (is_field(element)) {

          uninstall_property(element, 'checkValidity');
          uninstall_property(element, 'reportValidity');
          uninstall_property(element, 'setCustomValidity');
          uninstall_property(element, 'stepDown');
          uninstall_property(element, 'stepUp');
          uninstall_property(element, 'validationMessage');
          uninstall_property(element, 'validity');
          uninstall_property(element, 'valueAsDate');
          uninstall_property(element, 'valueAsNumber');
          uninstall_property(element, 'willValidate');

          uninstall_properties(element);
        } else if (element instanceof window.HTMLFormElement) {
          uninstall_property(element, 'checkValidity');
          uninstall_property(element, 'reportValidity');
        }
      }

      var instances = new WeakMap();

      /**
       * wrap <form>s, window or document, that get treated with the global
       * hyperform()
       */
      function Wrapper(form, settings) {

        /* do not allow more than one instance per form. Otherwise we'd end
         * up with double event handlers, polyfills re-applied, ... */
        var existing = instances.get(form);
        if (existing) {
          existing.settings = settings;
          return existing;
        }

        this.form = form;
        this.settings = settings;
        this.revalidator = this.revalidate.bind(this);

        instances.set(form, this);

        catch_submit(form, settings.revalidate === 'never');

        if (form === window || form.nodeType === 9) {
          /* install on the prototypes, when called for the whole document */
          this.install([window.HTMLButtonElement.prototype, window.HTMLInputElement.prototype, window.HTMLSelectElement.prototype, window.HTMLTextAreaElement.prototype, window.HTMLFieldSetElement.prototype]);
          polyfill(window.HTMLFormElement);
        } else if (form instanceof window.HTMLFormElement || form instanceof window.HTMLFieldSetElement) {
          this.install(form.elements);
          if (form instanceof window.HTMLFormElement) {
            polyfill(form);
          }
        }

        if (settings.revalidate === 'oninput' || settings.revalidate === 'hybrid') {
          /* in a perfect world we'd just bind to "input", but support here is
           * abysmal: http://caniuse.com/#feat=input-event */
          form.addEventListener('keyup', this.revalidator);
          form.addEventListener('change', this.revalidator);
        }
        if (settings.revalidate === 'onblur' || settings.revalidate === 'hybrid') {
          /* useCapture=true, because `blur` doesn't bubble. See
           * https://developer.mozilla.org/en-US/docs/Web/Events/blur#Event_delegation
           * for a discussion */
          form.addEventListener('blur', this.revalidator, true);
        }
      }

      Wrapper.prototype = {
        destroy: function destroy() {
          uncatch_submit(this.form);
          instances.delete(this.form);
          this.form.removeEventListener('keyup', this.revalidator);
          this.form.removeEventListener('change', this.revalidator);
          this.form.removeEventListener('blur', this.revalidator, true);
          if (this.form === window || this.form.nodeType === 9) {
            this.uninstall([window.HTMLButtonElement.prototype, window.HTMLInputElement.prototype, window.HTMLSelectElement.prototype, window.HTMLTextAreaElement.prototype, window.HTMLFieldSetElement.prototype]);
            polyunfill(window.HTMLFormElement);
          } else if (this.form instanceof window.HTMLFormElement || this.form instanceof window.HTMLFieldSetElement) {
            this.uninstall(this.form.elements);
            if (this.form instanceof window.HTMLFormElement) {
              polyunfill(this.form);
            }
          }
        },


        /**
         * revalidate an input element
         */
        revalidate: function revalidate(event) {
          if (event.target instanceof window.HTMLButtonElement || event.target instanceof window.HTMLTextAreaElement || event.target instanceof window.HTMLSelectElement || event.target instanceof window.HTMLInputElement) {

            if (this.settings.revalidate === 'hybrid') {
              /* "hybrid" somewhat simulates what browsers do. See for example
               * Firefox's :-moz-ui-invalid pseudo-class:
               * https://developer.mozilla.org/en-US/docs/Web/CSS/:-moz-ui-invalid */
              if (event.type === 'blur' && event.target.value !== event.target.defaultValue || ValidityState(event.target).valid) {
                /* on blur, update the report when the value has changed from the
                 * default or when the element is valid (possibly removing a still
                 * standing invalidity report). */
                reportValidity(event.target);
              } else if (event.type === 'keyup' || event.type === 'change') {
                if (ValidityState(event.target).valid) {
                  // report instantly, when an element becomes valid,
                  // postpone report to blur event, when an element is invalid
                  reportValidity(event.target);
                }
              }
            } else {
              reportValidity(event.target);
            }
          }
        },


        /**
         * install the polyfills on each given element
         *
         * If you add elements dynamically, you have to call install() on them
         * yourself:
         *
         * js> var form = hyperform(document.forms[0]);
         * js> document.forms[0].appendChild(input);
         * js> form.install(input);
         *
         * You can skip this, if you called hyperform on window or document.
         */
        install: function install(els) {
          if (els instanceof window.Element) {
            els = [els];
          }

          var els_length = els.length;

          for (var i = 0; i < els_length; i++) {
            polyfill(els[i]);
          }
        },
        uninstall: function uninstall(els) {
          if (els instanceof window.Element) {
            els = [els];
          }

          var els_length = els.length;

          for (var i = 0; i < els_length; i++) {
            polyunfill(els[i]);
          }
        }
      };

      /**
       * try to get the appropriate wrapper for a specific element by looking up
       * its parent chain
       *
       * @return Wrapper | undefined
       */
      function get_wrapper(element) {
        var wrapped;

        if (element.form) {
          /* try a shortcut with the element's <form> */
          wrapped = instances.get(element.form);
        }

        /* walk up the parent nodes until document (including) */
        while (!wrapped && element) {
          wrapped = instances.get(element);
          element = element.parentNode;
        }

        if (!wrapped) {
          /* try the global instance, if exists. This may also be undefined. */
          wrapped = instances.get(window);
        }

        return wrapped;
      }

      /**
       * check if an element is a candidate for constraint validation
       *
       * @see https://html.spec.whatwg.org/multipage/forms.html#barred-from-constraint-validation
       */
      function is_validation_candidate(element) {

        /* allow a shortcut via filters, e.g. to validate type=hidden fields */
        var filtered = do_filter('is_validation_candidate', null, element);
        if (filtered !== null) {
          return !!filtered;
        }

        /* it must be any of those elements */
        if (element instanceof window.HTMLSelectElement || element instanceof window.HTMLTextAreaElement || element instanceof window.HTMLButtonElement || element instanceof window.HTMLInputElement) {

          var type = get_type(element);
          /* its type must be in the whitelist or missing (select, textarea) */
          if (!type || non_inputs.indexOf(type) > -1 || validation_candidates.indexOf(type) > -1) {

            /* it mustn't be disabled or readonly */
            if (!element.hasAttribute('disabled') && !element.hasAttribute('readonly')) {

              var wrapped_form = get_wrapper(element);
              /* the parent form doesn't allow non-standard "novalidate" attributes
               * or it doesn't have such an attribute/property */
              if (wrapped_form && !wrapped_form.settings.novalidateOnElements || !element.hasAttribute('novalidate') || !element.noValidate) {

                /* it isn't part of a <fieldset disabled> */
                var p = element.parentNode;
                while (p && p.nodeType === 1) {
                  if (p instanceof window.HTMLFieldSetElement && p.hasAttribute('disabled')) {
                    /* quick return, if it's a child of a disabled fieldset */
                    return false;
                  } else if (p.nodeName.toUpperCase() === 'DATALIST') {
                    /* quick return, if it's a child of a datalist
                     * Do not use HTMLDataListElement to support older browsers,
                     * too.
                     * @see https://html.spec.whatwg.org/multipage/forms.html#the-datalist-element:barred-from-constraint-validation
                     */
                    return false;
                  } else if (p === element.form) {
                    /* the outer boundary. We can stop looking for relevant
                     * fieldsets. */
                    break;
                  }
                  p = p.parentNode;
                }

                /* then it's a candidate */
                return true;
              }
            }
          }
        }

        /* this is no HTML5 validation candidate... */
        return false;
      }

      function format_date(date) {
        var part = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

        switch (part) {
          case 'date':
            return (date.toLocaleDateString || date.toDateString).call(date);
          case 'time':
            return (date.toLocaleTimeString || date.toTimeString).call(date);
          case 'month':
            return 'toLocaleDateString' in date ? date.toLocaleDateString(undefined, {
              year: 'numeric',
              month: '2-digit'
            }) : date.toDateString();
            // case 'week':
            // TODO
          default:
            return (date.toLocaleString || date.toString).call(date);
        }
      }

      /**
       * patch String.length to account for non-BMP characters
       *
       * @see https://mathiasbynens.be/notes/javascript-unicode
       * We do not use the simple [...str].length, because it needs a ton of
       * polyfills in older browsers.
       */

      function unicode_string_length(str) {
        return str.match(/[\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/g).length;
      }

      /**
       * internal storage for custom error messages
       */

      var store$1 = new WeakMap();

      /**
       * register custom error messages per element
       */
      var custom_messages = {
        set: function set(element, validator, message) {
          var messages = store$1.get(element) || {};
          messages[validator] = message;
          store$1.set(element, messages);
          return custom_messages;
        },
        get: function get(element, validator) {
          var _default = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;

          var messages = store$1.get(element);
          if (messages === undefined || !(validator in messages)) {
            var data_id = 'data-' + validator.replace(/[A-Z]/g, '-$&').toLowerCase();
            if (element.hasAttribute(data_id)) {
              /* if the element has a data-validator attribute, use this as fallback.
               * E.g., if validator == 'valueMissing', the element can specify a
               * custom validation message like this:
               *     <input data-value-missing="Oh noes!">
               */
              return element.getAttribute(data_id);
            }
            return _default;
          }
          return messages[validator];
        },
        delete: function _delete(element) {
          var validator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

          if (!validator) {
            return store$1.delete(element);
          }
          var messages = store$1.get(element) || {};
          if (validator in messages) {
            delete messages[validator];
            store$1.set(element, messages);
            return true;
          }
          return false;
        }
      };

      var internal_registry = new WeakMap();

      /**
       * A registry for custom validators
       *
       * slim wrapper around a WeakMap to ensure the values are arrays
       * (hence allowing > 1 validators per element)
       */
      var custom_validator_registry = {
        set: function set(element, validator) {
          var current = internal_registry.get(element) || [];
          current.push(validator);
          internal_registry.set(element, current);
          return custom_validator_registry;
        },
        get: function get(element) {
          return internal_registry.get(element) || [];
        },
        delete: function _delete(element) {
          return internal_registry.delete(element);
        }
      };

      /**
       * test whether the element suffers from bad input
       */
      function test_bad_input(element) {
        var type = get_type(element);

        if (!is_validation_candidate(element) || input_checked.indexOf(type) === -1) {
          /* we're not interested, thanks! */
          return true;
        }

        /* the browser hides some bad input from the DOM, e.g. malformed numbers,
         * email addresses with invalid punycode representation, ... We try to resort
         * to the original method here. The assumption is, that a browser hiding
         * bad input will hopefully also always support a proper
         * ValidityState.badInput */
        if (!element.value) {
          if ('_original_validity' in element && !element._original_validity.__hyperform) {
            return !element._original_validity.badInput;
          }
          /* no value and no original badInput: Assume all's right. */
          return true;
        }

        var result = true;
        switch (type) {
          case 'color':
            result = /^#[a-f0-9]{6}$/.test(element.value);
            break;
          case 'number':
          case 'range':
            result = !isNaN(Number(element.value));
            break;
          case 'datetime':
          case 'date':
          case 'month':
          case 'week':
          case 'time':
            result = string_to_date(element.value, type) !== null;
            break;
          case 'datetime-local':
            result = /^([0-9]{4,})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([01][0-9]|2[0-3]):([0-5][0-9])(?::([0-5][0-9])(?:\.([0-9]{1,3}))?)?$/.test(element.value);
            break;
          case 'tel':
            /* spec says No! Phone numbers can have all kinds of formats, so this
             * is expected to be a free-text field. */
            // TODO we could allow a setting 'phone_regex' to be evaluated here.
            break;
          case 'email':
            break;
        }

        return result;
      }

      /**
       * test the max attribute
       *
       * we use Number() instead of parseFloat(), because an invalid attribute
       * value like "123abc" should result in an error.
       */
      function test_max(element) {
        var type = get_type(element);

        if (!is_validation_candidate(element) || !element.value || !element.hasAttribute('max')) {
          /* we're not responsible here */
          return true;
        }

        var value = void 0,
          max = void 0;
        if (dates.indexOf(type) > -1) {
          value = 1 * string_to_date(element.value, type);
          max = 1 * (string_to_date(element.getAttribute('max'), type) || NaN);
        } else {
          value = Number(element.value);
          max = Number(element.getAttribute('max'));
        }

        return isNaN(max) || value <= max;
      }

      /**
       * test the maxlength attribute
       */
      function test_maxlength(element) {
        if (!is_validation_candidate(element) || !element.value || text_types.indexOf(get_type(element)) === -1 || !element.hasAttribute('maxlength') || !element.getAttribute('maxlength') // catch maxlength=""
        ) {
          return true;
        }

        var maxlength = parseInt(element.getAttribute('maxlength'), 10);

        /* check, if the maxlength value is usable at all.
         * We allow maxlength === 0 to basically disable input (Firefox does, too).
         */
        if (isNaN(maxlength) || maxlength < 0) {
          return true;
        }

        return unicode_string_length(element.value) <= maxlength;
      }

      /**
       * test the min attribute
       *
       * we use Number() instead of parseFloat(), because an invalid attribute
       * value like "123abc" should result in an error.
       */
      function test_min(element) {
        var type = get_type(element);

        if (!is_validation_candidate(element) || !element.value || !element.hasAttribute('min')) {
          /* we're not responsible here */
          return true;
        }

        var value = void 0,
          min = void 0;
        if (dates.indexOf(type) > -1) {
          value = 1 * string_to_date(element.value, type);
          min = 1 * (string_to_date(element.getAttribute('min'), type) || NaN);
        } else {
          value = Number(element.value);
          min = Number(element.getAttribute('min'));
        }

        return isNaN(min) || value >= min;
      }

      /**
       * test the minlength attribute
       */
      function test_minlength(element) {
        if (!is_validation_candidate(element) || !element.value || text_types.indexOf(get_type(element)) === -1 || !element.hasAttribute('minlength') || !element.getAttribute('minlength') // catch minlength=""
        ) {
          return true;
        }

        var minlength = parseInt(element.getAttribute('minlength'), 10);

        /* check, if the minlength value is usable at all. */
        if (isNaN(minlength) || minlength < 0) {
          return true;
        }

        return unicode_string_length(element.value) >= minlength;
      }

      /**
       * test the pattern attribute
       */
      function test_pattern(element) {
        return !is_validation_candidate(element) || !element.value || !element.hasAttribute('pattern') || new RegExp('^(?:' + element.getAttribute('pattern') + ')$').test(element.value);
      }

      /**
       * test the required attribute
       */
      function test_required(element) {
        if (!is_validation_candidate(element) || !element.hasAttribute('required')) {
          /* nothing to do */
          return true;
        }

        /* we don't need get_type() for element.type, because "checkbox" and "radio"
         * are well supported. */
        switch (element.type) {
          case 'checkbox':
            return element.checked;
            //break;
          case 'radio':
            /* radio inputs have "required" fulfilled, if _any_ other radio
             * with the same name in this form is checked. */
            return !!(element.checked || element.form && Array.prototype.filter.call(document.getElementsByName(element.name), function(radio) {
              return radio.name === element.name && radio.form === element.form && radio.checked;
            }).length > 0);
            //break;
          default:
            return !!element.value;
        }
      }

      /**
       * test the step attribute
       */
      function test_step(element) {
        var type = get_type(element);

        if (!is_validation_candidate(element) || !element.value || numbers.indexOf(type) === -1 || (element.getAttribute('step') || '').toLowerCase() === 'any') {
          /* we're not responsible here. Note: If no step attribute is given, we
           * need to validate against the default step as per spec. */
          return true;
        }

        var step = element.getAttribute('step');
        if (step) {
          step = string_to_number(step, type);
        } else {
          step = default_step[type] || 1;
        }

        if (step <= 0 || isNaN(step)) {
          /* error in specified "step". We cannot validate against it, so the value
           * is true. */
          return true;
        }

        var scale = step_scale_factor[type] || 1;

        var value = string_to_number(element.value, type);
        var min = string_to_number(element.getAttribute('min') || element.getAttribute('value') || '', type);

        if (isNaN(min)) {
          min = default_step_base[type] || 0;
        }

        if (type === 'month') {
          /* type=month has month-wide steps. See
           * https://html.spec.whatwg.org/multipage/forms.html#month-state-%28type=month%29
           */
          min = new Date(min).getUTCFullYear() * 12 + new Date(min).getUTCMonth();
          value = new Date(value).getUTCFullYear() * 12 + new Date(value).getUTCMonth();
        }

        var result = Math.abs(min - value) % (step * scale);

        return result < 0.00000001 ||
        /* crappy floating-point arithmetics! */
        result > step * scale - 0.00000001;
      }

      var ws_on_start_or_end = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

      /**
       * trim a string of whitespace
       *
       * We don't use String.trim() to remove the need to polyfill it.
       */
      function trim(str) {
        return str.replace(ws_on_start_or_end, '');
      }

      /**
       * split a string on comma and trim the components
       *
       * As specified at
       * https://html.spec.whatwg.org/multipage/infrastructure.html#split-a-string-on-commas
       * plus removing empty entries.
       */
      function comma_split(str) {
        return str.split(',').map(function(item) {
          return trim(item);
        }).filter(function(b) {
          return b;
        });
      }

      /* we use a dummy <a> where we set the href to test URL validity
       * The definition is out of the "global" scope so that JSDOM can be instantiated
       * after loading Hyperform for tests.
       */
      var url_canary;

      /* see https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address */
      var email_pattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

      /**
       * test the type-inherent syntax
       */
      function test_type(element) {
        var type = get_type(element);

        if (!is_validation_candidate(element) || type !== 'file' && !element.value || type !== 'file' && type_checked.indexOf(type) === -1) {
          /* we're not responsible for this element */
          return true;
        }

        var is_valid = true;

        switch (type) {
          case 'url':
            if (!url_canary) {
              url_canary = document.createElement('a');
            }
            var value = trim(element.value);
            url_canary.href = value;
            is_valid = url_canary.href === value || url_canary.href === value + '/';
            break;
          case 'email':
            if (element.hasAttribute('multiple')) {
              is_valid = comma_split(element.value).every(function(value) {
                return email_pattern.test(value);
              });
            } else {
              is_valid = email_pattern.test(trim(element.value));
            }
            break;
          case 'file':
            if ('files' in element && element.files.length && element.hasAttribute('accept')) {
              var patterns = comma_split(element.getAttribute('accept')).map(function(pattern) {
                if (/^(audio|video|image)\/\*$/.test(pattern)) {
                  pattern = new RegExp('^' + RegExp.$1 + '/.+$');
                }
                return pattern;
              });

              if (!patterns.length) {
                break;
              }

              fileloop: for (var i = 0; i < element.files.length; i++) {
                /* we need to match a whitelist, so pre-set with false */
                var file_valid = false;

                patternloop: for (var j = 0; j < patterns.length; j++) {
                  var file = element.files[i];
                  var pattern = patterns[j];

                  var fileprop = file.type;

                  if (typeof pattern === 'string' && pattern.substr(0, 1) === '.') {
                    if (file.name.search('.') === -1) {
                      /* no match with any file ending */
                      continue patternloop;
                    }

                    fileprop = file.name.substr(file.name.lastIndexOf('.'));
                  }

                  if (fileprop.search(pattern) === 0) {
                    /* we found one match and can quit looking */
                    file_valid = true;
                    break patternloop;
                  }
                }

                if (!file_valid) {
                  is_valid = false;
                  break fileloop;
                }
              }
            }
        }

        return is_valid;
      }

      /**
       * boilerplate function for all tests but customError
       */
      function check$1(test, react) {
        return function(element) {
          var invalid = !test(element);
          if (invalid) {
            react(element);
          }
          return invalid;
        };
      }

      /**
       * create a common function to set error messages
       */
      function set_msg(element, msgtype, _default) {
        message_store.set(element, custom_messages.get(element, msgtype, _default));
      }

      var badInput = check$1(test_bad_input, function(element) {
        return set_msg(element, 'badInput', _('Please match the requested type.'));
      });

      function customError(element) {
        /* check, if there are custom validators in the registry, and call
         * them. */
        var custom_validators = custom_validator_registry.get(element);
        var cvl = custom_validators.length;
        var valid = true;

        if (cvl) {
          for (var i = 0; i < cvl; i++) {
            var result = custom_validators[i](element);
            if (result !== undefined && !result) {
              valid = false;
              /* break on first invalid response */
              break;
            }
          }
        }

        /* check, if there are other validity messages already */
        if (valid) {
          var msg = message_store.get(element);
          valid = !(msg.toString() && 'is_custom' in msg);
        }

        return !valid;
      }

      var patternMismatch = check$1(test_pattern, function(element) {
        set_msg(element, 'patternMismatch', element.title ? sprintf(_('PatternMismatchWithTitle'), element.title) : _('PatternMismatch'));
      });

      /**
       * TODO: when rangeOverflow and rangeUnderflow are both called directly and
       * successful, the inRange and outOfRange classes won't get removed, unless
       * element.validityState.valid is queried, too.
       */
      var rangeOverflow = check$1(test_max, function(element) {
        var type = get_type(element);
        var wrapper = get_wrapper(element);
        var outOfRangeClass = wrapper && wrapper.settings.classes.outOfRange || 'hf-out-of-range';
        var inRangeClass = wrapper && wrapper.settings.classes.inRange || 'hf-in-range';

        var msg = void 0;

        switch (type) {
          case 'date':
          case 'datetime':
          case 'datetime-local':
            msg = sprintf(_('DateRangeOverflow'), format_date(string_to_date(element.getAttribute('max'), type), type));
            break;
          case 'time':
            msg = sprintf(_('TimeRangeOverflow'), format_date(string_to_date(element.getAttribute('max'), type), type));
            break;
            // case 'number':
          default:
            msg = sprintf(_('NumberRangeOverflow'), string_to_number(element.getAttribute('max'), type));
            break;
        }

        set_msg(element, 'rangeOverflow', msg);
        element.classList.add(outOfRangeClass);
        element.classList.remove(inRangeClass);
      });

      var rangeUnderflow = check$1(test_min, function(element) {
        var type = get_type(element);
        var wrapper = get_wrapper(element);
        var outOfRangeClass = wrapper && wrapper.settings.classes.outOfRange || 'hf-out-of-range';
        var inRangeClass = wrapper && wrapper.settings.classes.inRange || 'hf-in-range';

        var msg = void 0;

        switch (type) {
          case 'date':
          case 'datetime':
          case 'datetime-local':
            msg = sprintf(_('DateRangeUnderflow'), format_date(string_to_date(element.getAttribute('min'), type), type));
            break;
          case 'time':
            msg = sprintf(_('TimeRangeUnderflow'), format_date(string_to_date(element.getAttribute('min'), type), type));
            break;
            // case 'number':
          default:
            msg = sprintf(_('NumberRangeUnderflow'), string_to_number(element.getAttribute('min'), type));
            break;
        }

        set_msg(element, 'rangeUnderflow', msg);
        element.classList.add(outOfRangeClass);
        element.classList.remove(inRangeClass);
      });

      var stepMismatch = check$1(test_step, function(element) {
        var list = get_next_valid(element);
        var min = list[0];
        var max = list[1];
        var sole = false;
        var msg = void 0;

        if (min === null) {
          sole = max;
        } else if (max === null) {
          sole = min;
        }

        if (sole !== false) {
          msg = sprintf(_('StepMismatchOneValue'), sole);
        } else {
          msg = sprintf(_('StepMismatch'), min, max);
        }
        set_msg(element, 'stepMismatch', msg);
      });

      var tooLong = check$1(test_maxlength, function(element) {
        set_msg(element, 'tooLong', sprintf(_('TextTooLong'), element.getAttribute('maxlength'), unicode_string_length(element.value)));
      });

      var tooShort = check$1(test_minlength, function(element) {
        set_msg(element, 'tooShort', sprintf(_('Please lengthen this text to %l characters or more (you are currently using %l characters).'), element.getAttribute('minlength'), unicode_string_length(element.value)));
      });

      var typeMismatch = check$1(test_type, function(element) {
        var msg = _('Please use the appropriate format.');
        var type = get_type(element);

        if (type === 'email') {
          if (element.hasAttribute('multiple')) {
            msg = _('Please enter a comma separated list of email addresses.');
          } else {
            msg = _('InvalidEmail');
          }
        } else if (type === 'url') {
          msg = _('InvalidURL');
        } else if (type === 'file') {
          msg = _('Please select a file of the correct type.');
        }

        set_msg(element, 'typeMismatch', msg);
      });

      var valueMissing = check$1(test_required, function(element) {
        var msg = _('ValueMissing');
        var type = get_type(element);

        if (type === 'checkbox') {
          msg = _('CheckboxMissing');
        } else if (type === 'radio') {
          msg = _('RadioMissing');
        } else if (type === 'file') {
          if (element.hasAttribute('multiple')) {
            msg = _('Please select one or more files.');
          } else {
            msg = _('FileMissing');
          }
        } else if (element instanceof window.HTMLSelectElement) {
          msg = _('SelectMissing');
        }

        set_msg(element, 'valueMissing', msg);
      });

      var validity_state_checkers = {
        badInput: badInput,
        customError: customError,
        patternMismatch: patternMismatch,
        rangeOverflow: rangeOverflow,
        rangeUnderflow: rangeUnderflow,
        stepMismatch: stepMismatch,
        tooLong: tooLong,
        tooShort: tooShort,
        typeMismatch: typeMismatch,
        valueMissing: valueMissing
      };

      /**
       * the validity state constructor
       */
      var ValidityState = function ValidityState(element) {
        if (!(element instanceof window.HTMLElement)) {
          throw new Error('cannot create a ValidityState for a non-element');
        }

        var cached = ValidityState.cache.get(element);
        if (cached) {
          return cached;
        }

        if (!(this instanceof ValidityState)) {
          /* working around a forgotten `new` */
          return new ValidityState(element);
        }

        this.element = element;
        ValidityState.cache.set(element, this);
      };

      /**
       * the prototype for new validityState instances
       */
      var ValidityStatePrototype = {};
      ValidityState.prototype = ValidityStatePrototype;

      ValidityState.cache = new WeakMap();

      /**
       * copy functionality from the validity checkers to the ValidityState
       * prototype
       */
      for (var prop in validity_state_checkers) {
        Object.defineProperty(ValidityStatePrototype, prop, {
          configurable: true,
          enumerable: true,
          get: function(func) {
            return function() {
              return func(this.element);
            };
          }(validity_state_checkers[prop]),
          set: undefined
        });
      }

      /**
       * the "valid" property calls all other validity checkers and returns true,
       * if all those return false.
       *
       * This is the major access point for _all_ other API methods, namely
       * (check|report)Validity().
       */
      Object.defineProperty(ValidityStatePrototype, 'valid', {
        configurable: true,
        enumerable: true,
        get: function get() {
          var wrapper = get_wrapper(this.element);
          var validClass = wrapper && wrapper.settings.classes.valid || 'hf-valid';
          var invalidClass = wrapper && wrapper.settings.classes.invalid || 'hf-invalid';
          var userInvalidClass = wrapper && wrapper.settings.classes.userInvalid || 'hf-user-invalid';
          var userValidClass = wrapper && wrapper.settings.classes.userValid || 'hf-user-valid';
          var inRangeClass = wrapper && wrapper.settings.classes.inRange || 'hf-in-range';
          var outOfRangeClass = wrapper && wrapper.settings.classes.outOfRange || 'hf-out-of-range';
          var validatedClass = wrapper && wrapper.settings.classes.validated || 'hf-validated';

          this.element.classList.add(validatedClass);

          if (is_validation_candidate(this.element)) {
            for (var _prop in validity_state_checkers) {
              if (validity_state_checkers[_prop](this.element)) {
                this.element.classList.add(invalidClass);
                this.element.classList.remove(validClass);
                this.element.classList.remove(userValidClass);
                if (this.element.value !== this.element.defaultValue) {
                  this.element.classList.add(userInvalidClass);
                } else {
                  this.element.classList.remove(userInvalidClass);
                }
                this.element.setAttribute('aria-invalid', 'true');
                return false;
              }
            }
          }

          message_store.delete(this.element);
          this.element.classList.remove(invalidClass);
          this.element.classList.remove(userInvalidClass);
          this.element.classList.remove(outOfRangeClass);
          this.element.classList.add(validClass);
          this.element.classList.add(inRangeClass);
          if (this.element.value !== this.element.defaultValue) {
            this.element.classList.add(userValidClass);
          } else {
            this.element.classList.remove(userValidClass);
          }
          this.element.setAttribute('aria-invalid', 'false');
          return true;
        },
        set: undefined
      });

      /**
       * mark the validity prototype, because that is what the client-facing
       * code deals with mostly, not the property descriptor thing */
      mark(ValidityStatePrototype);

      /**
       * check an element's validity with respect to it's form
       */
      var checkValidity = return_hook_or('checkValidity', function(element) {
        /* if this is a <form>, check validity of all child inputs */
        if (element instanceof window.HTMLFormElement) {
          return Array.prototype.map.call(element.elements, checkValidity).every(function(b) {
            return b;
          });
        }

        /* default is true, also for elements that are no validation candidates */
        var valid = ValidityState(element).valid;
        if (valid) {
          var wrapped_form = get_wrapper(element);
          if (wrapped_form && wrapped_form.settings.validEvent) {
            trigger_event(element, 'valid');
          }
        } else {
          trigger_event(element, 'invalid', {
            cancelable: true
          });
        }

        return valid;
      });

      var version = '0.9.9';

      /* deprecate the old snake_case names
       * TODO: delme before next non-patch release
       */
      function w(name) {
        var deprecated_message = 'Please use camelCase method names! The name "%s" is deprecated and will be removed in the next non-patch release.';
        /* global console */
        console.log(sprintf(deprecated_message, name));
      }

      /**
       * public hyperform interface:
       */
      function hyperform(form) {
        var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var classes = _ref.classes;
        var _ref$debug = _ref.debug;
        var debug = _ref$debug === undefined ? false : _ref$debug;
        var extend_fieldset = _ref.extend_fieldset;
        var extendFieldset = _ref.extendFieldset;
        var novalidate_on_elements = _ref.novalidate_on_elements;
        var novalidateOnElements = _ref.novalidateOnElements;
        var prevent_implicit_submit = _ref.prevent_implicit_submit;
        var preventImplicitSubmit = _ref.preventImplicitSubmit;
        var revalidate = _ref.revalidate;
        var _ref$strict = _ref.strict;
        var strict = _ref$strict === undefined ? false : _ref$strict;
        var valid_event = _ref.valid_event;
        var validEvent = _ref.validEvent;


        if (!classes) {
          classes = {};
        }
        // TODO: clean up before next non-patch release
        if (extendFieldset === undefined) {
          if (extend_fieldset === undefined) {
            extendFieldset = !strict;
          } else {
            w('extend_fieldset');
            extendFieldset = extend_fieldset;
          }
        }
        if (novalidateOnElements === undefined) {
          if (novalidate_on_elements === undefined) {
            novalidateOnElements = !strict;
          } else {
            w('novalidate_on_elements');
            novalidateOnElements = novalidate_on_elements;
          }
        }
        if (preventImplicitSubmit === undefined) {
          if (prevent_implicit_submit === undefined) {
            preventImplicitSubmit = false;
          } else {
            w('prevent_implicit_submit');
            preventImplicitSubmit = prevent_implicit_submit;
          }
        }
        if (revalidate === undefined) {
          /* other recognized values: 'oninput', 'onblur', 'onsubmit' and 'never' */
          revalidate = strict ? 'onsubmit' : 'hybrid';
        }
        if (validEvent === undefined) {
          if (valid_event === undefined) {
            validEvent = !strict;
          } else {
            w('valid_event');
            validEvent = valid_event;
          }
        }

        var settings = {
          debug: debug,
          strict: strict,
          preventImplicitSubmit: preventImplicitSubmit,
          revalidate: revalidate,
          validEvent: validEvent,
          extendFieldset: extendFieldset,
          classes: classes,
          novalidateOnElements: novalidateOnElements
        };

        if (form instanceof window.NodeList || form instanceof window.HTMLCollection || form instanceof Array) {
          return Array.prototype.map.call(form, function(element) {
            return hyperform(element, settings);
          });
        }

        return new Wrapper(form, settings);
      }

      hyperform.version = version;

      hyperform.checkValidity = checkValidity;
      hyperform.reportValidity = reportValidity;
      hyperform.setCustomValidity = setCustomValidity;
      hyperform.stepDown = stepDown;
      hyperform.stepUp = stepUp;
      hyperform.validationMessage = validationMessage;
      hyperform.ValidityState = ValidityState;
      hyperform.valueAsDate = valueAsDate;
      hyperform.valueAsNumber = valueAsNumber;
      hyperform.willValidate = willValidate;

      hyperform.setLanguage = function(lang) {
        set_language(lang);
        return hyperform;
      };
      hyperform.addTranslation = function(lang, catalog) {
        add_translation(lang, catalog);
        return hyperform;
      };
      hyperform.setRenderer = function(renderer, action) {
        Renderer.set(renderer, action);
        return hyperform;
      };
      hyperform.addValidator = function(element, validator) {
        custom_validator_registry.set(element, validator);
        return hyperform;
      };
      hyperform.setMessage = function(element, validator, message) {
        custom_messages.set(element, validator, message);
        return hyperform;
      };
      hyperform.addHook = function(hook, action, position) {
        add_hook(hook, action, position);
        return hyperform;
      };
      hyperform.removeHook = function(hook, action) {
        remove_hook(hook, action);
        return hyperform;
      };

      // TODO: Remove in next non-patch version
      hyperform.set_language = function(lang) {
        w('set_language');
        set_language(lang);
        return hyperform;
      };
      hyperform.add_translation = function(lang, catalog) {
        w('add_translation');
        add_translation(lang, catalog);
        return hyperform;
      };
      hyperform.set_renderer = function(renderer, action) {
        w('set_renderer');
        Renderer.set(renderer, action);
        return hyperform;
      };
      hyperform.add_validator = function(element, validator) {
        w('add_validator');
        custom_validator_registry.set(element, validator);
        return hyperform;
      };
      hyperform.set_message = function(element, validator, message) {
        w('set_message');
        custom_messages.set(element, validator, message);
        return hyperform;
      };
      hyperform.add_hook = function(hook, action, position) {
        w('add_hook');
        add_hook(hook, action, position);
        return hyperform;
      };
      hyperform.remove_hook = function(hook, action) {
        w('remove_hook');
        remove_hook(hook, action);
        return hyperform;
      };

      if (document.currentScript && document.currentScript.hasAttribute('data-hf-autoload')) {
        hyperform(window);
      }

      module.exports = hyperform;
    }, {}
  ],
  2: [
    function(require, module, exports) {
      'use strict';

      var _promisePolyfill = require('promise-polyfill');

      var _promisePolyfill2 = _interopRequireDefault(_promisePolyfill);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      if (!window.Promise) {
        window.Promise = _promisePolyfill2.
        default;
      }

      exports.Config = require('./lib/Config');
      exports.Facets = require('./lib/Facets');
      exports.Pagination = require('./lib/Pagination');
      exports.Search = require('./lib/Search');

    }, {
      "./lib/Config": 3,
      "./lib/Facets": 4,
      "./lib/Pagination": 5,
      "./lib/Search": 6,
      "promise-polyfill": 8
    }
  ],
  3: [
    function(require, module, exports) {
      'use strict';

      var _createClass = function() {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }
        return function(Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      /**
       * http://yui.github.io/yuidoc/syntax/index.html
       */

      var Config = function() {
        /**
         * Primary constructor for the Pagination class.
         *
         * @class Config
         * @constructor
         */
        function Config() {
          _classCallCheck(this, Config);

          this._endpoint = null;
          this._searchURL = null;
          this._defaultQueryParams = {};
          this._defaultSortParams = {};
          this._facetOptions = [];
          this._keywordParam = 'q';
          this._orderParam = null;
          this._pagerParam = null;
          this._sortParam = null;
        }

        /**
         * Get & Set the endpoint use to make all Fetch requests.
         *
         * @property endpoint
         * @return { String }
         */


        _createClass(Config, [{
          key: 'endpoint',
          get: function get() {
            return this._endpoint;
          },
          set: function set(endpoint) {
            this._endpoint = endpoint;
          }

          /**
           * Get & Set the available facets being used to alter Fetch requests.
           *
           * @property facetOptions
           * @return { String }
           */

        }, {
          key: 'facetOptions',
          get: function get() {
            return this._facetOptions;
          },
          set: function set(facetOptions) {
            this._facetOptions = facetOptions;
          }

          /**
           * Get & Set the URL used on the frontend UI.
           *
           * @property searchURL
           * @return { String }
           */

        }, {
          key: 'searchURL',
          get: function get() {
            return this._searchURL;
          },
          set: function set(searchURL) {
            this._searchURL = searchURL;
          }

          /**
           * Get & Set the default query parameters when none are provided.
           *
           * @property defaultQueryParams
           * @return { Object }
           */

        }, {
          key: 'defaultQueryParams',
          get: function get() {
            return this._defaultQueryParams;
          },
          set: function set(defaultQueryParams) {
            this._defaultQueryParams = defaultQueryParams;
          }

          /**
           * Get & Set the default sort parameters when none are provided.
           *
           * @property defaultQueryParams
           * @return { Object }
           */

        }, {
          key: 'defaultSortParams',
          get: function get() {
            return this._defaultSortParams;
          },
          set: function set(defaultSortParams) {
            this._defaultSortParams = defaultSortParams;
          }

          /**
           * Get & Set targets the name of the keyword specific query parameter.
           *
           * @property keywordParam
           * @return { String }
           */

        }, {
          key: 'keywordParam',
          get: function get() {
            return this._keywordParam;
          },
          set: function set(keywordParam) {
            this._keywordParam = keywordParam;
          }

          /**
           * Get & Set targets the name of the order specific query parameter.
           *
           * @property orderParam
           * @return { String }
           */

        }, {
          key: 'orderParam',
          get: function get() {
            return this._orderParam;
          },
          set: function set(orderParam) {
            this._orderParam = orderParam;
          }

          /**
           * Get & Set targets the name of the page specific query parameter.
           *
           * @property pagerParam
           * @return { String }
           */

        }, {
          key: 'pagerParam',
          get: function get() {
            return this._pagerParam;
          },
          set: function set(pagerParam) {
            this._pagerParam = pagerParam;
          }

          /**
           * Get & Set targets the name of the sort specific query parameter.
           *
           * @property sortParam
           * @return { String }
           */

        }, {
          key: 'sortParam',
          get: function get() {
            return this._sortParam;
          },
          set: function set(sortParam) {
            this._sortParam = sortParam;
          }
        }]);

        return Config;
      }();

      module.exports = Config;

    }, {}
  ],
  4: [
    function(require, module, exports) {
      'use strict';

      var _extends = Object.assign || function(target) {
          for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];
            for (var key in source) {
              if (Object.prototype.hasOwnProperty.call(source, key)) {
                target[key] = source[key];
              }
            }
          }
          return target;
        };

      var _createClass = function() {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }
        return function(Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      var _Pagination2 = require('./Pagination');

      var _Pagination3 = _interopRequireDefault(_Pagination2);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      function _possibleConstructorReturn(self, call) {
        if (!self) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }
        return call && (typeof call === "object" || typeof call === "function") ? call : self;
      }

      function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
          throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }
        subClass.prototype = Object.create(superClass && superClass.prototype, {
          constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
          }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
      }

      /**
       * http://yui.github.io/yuidoc/syntax/index.html
       */

      var Facets = function(_Pagination) {
        _inherits(Facets, _Pagination);

        /**
         * Primary constructor for the Pagination class.
         *
         * @class Facets
         * @constructor
         * @param { Config } config
         */
        function Facets(config) {
          _classCallCheck(this, Facets);

          var _this = _possibleConstructorReturn(this, (Facets.__proto__ || Object.getPrototypeOf(Facets)).call(this, config));
          // Call the parent class constructor.


          var defaultSortParams = config.defaultSortParams,
            facetOptions = config.facetOptions,
            orderParam = config.orderParam,
            sortParam = config.sortParam;


          _this._defaultSortParams = defaultSortParams;
          _this._facetOptions = facetOptions;
          _this._orderParam = orderParam || null;
          _this._sortParam = sortParam || null;

          _this._initFacets();
          _this._initSorting();
          return _this;
        }

        /**
         * Get the default Sorting parameters that were setup when ethe Class instantiated.
         *
         * @property defaultSortParams
         * @return { Object }
         */


        _createClass(Facets, [{
          key: '_helperUpdateFacet',
          value: function _helperUpdateFacet(name, value) {
            var facetSettings = this.facetSettings,
              query = this.query,
              current = query[name] || null,
              settings = facetSettings[name];

            // If nothing is set the go ahead and set it.

            if (!current) {
              query[name] = value;
              return this._updatePushState();
            }

            // If the value exists go ahead and remove it.
            if (current === value) {
              delete query[name];
              return this._updatePushState();
            }

            // handle facets where isMultiCardinality is set to false.
            if (!settings.isMultiCardinality) {
              delete query[name];
              query[name] = value;
              return this._updatePushState();
            }

            var options = !current ? [] : current.split(','),
              index = options.indexOf(value);

            if (index >= 0) {
              delete options[index];
            } else {
              options.push(value);
            }

            query[name] = this.compact(options).join(',');
            return this._updatePushState();
          }

          /**
           * Helper method to allow for quickly and easily update sorting parameters.
           *
           * @private
           * @method _helperUpdateSorting
           * @param { String } sort
           * @param { String } order
           * @return undefined
           */

        }, {
          key: '_helperUpdateSorting',
          value: function _helperUpdateSorting(sort, order) {
            var orderParam = this.orderParam,
              query = this.query,
              sortParam = this.sortParam;


            query[orderParam] = order;
            query[sortParam] = sort;

            this._updatePushState();
          }

          /**
           * Merges the facet parameters into the available queryParams.
           *
           * @private
           * @method _initFacets
           * @return undefined
           */

        }, {
          key: '_initFacets',
          value: function _initFacets() {
            var facetSettings = this.facetSettings,
              query = this.query,
              queryParams = this.queryParams,
              facets = [];


            Object.keys(facetSettings).map(function(field) {
              query[field] = facetSettings[field].
              default;
              facets.push(field);
            });

            var options = queryParams.concat(facets),
              compact = this.compact(options),
              uniqe = this.uniq(compact);

            this.queryParams = uniqe;
          }

          /**
           * Merges the sorting parameters into the available queryParams.
           *
           * @private
           * @method _initSorting
           * @return undefined
           */

        }, {
          key: '_initSorting',
          value: function _initSorting() {
            var defaultSortParams = this.defaultSortParams,
              orderParam = this.orderParam,
              query = this.query,
              queryParams = this.queryParams,
              sortParam = this.sortParam;


            query[orderParam] = defaultSortParams[orderParam];
            query[sortParam] = defaultSortParams[sortParam];

            var options = queryParams.concat([orderParam, sortParam]),
              compact = this.compact(options),
              uniqe = this.uniq(compact);

            this.queryParams = uniqe;
          }

          /**
           * Determines if a facet or sorting option is currently set in the uri query.
           *
           * @private
           * @method _isFacetActive
           * @param { String } name
           * @param { String } value
           * @return { Boolean }
           */

        }, {
          key: '_isFacetActive',
          value: function _isFacetActive(name, value) {
            var facetSettings = this.facetSettings,
              query = this.query,
              settings = facetSettings[name] || {},
              isBoolean = settings.type === 'boolean',
              option = isBoolean ? Boolean(value) : value,
              queryParam = isBoolean ? Boolean(query[name]) : query[name];


            if (settings.isMultiCardinality) {
              return !queryParam ? false : queryParam.split(',').indexOf(value) >= 0;
            }

            return queryParam === option;
          }
        }, {
          key: 'defaultSortParams',
          get: function get() {
            return this._defaultSortParams;
          }

          /**
           * Get returned facet information from the API response.
           *
           * @property facets
           * @return { Array }
           */

        }, {
          key: 'facets',
          get: function get() {
            return this._meta ? this._meta.facets : [];
          }

          /**
           * Combine the API facet data with the default facetOptions to build relative
           * facet setttings.
           *
           * @property facetSettings
           * @return { Object }
           */

        }, {
          key: 'facetSettings',
          get: function get() {
            var facetOptions = this.facetOptions,
              facets = this.facets;

            // Setup settings using the defaul facetOptions.

            var settings = {};
            facetOptions.map(function(facet) {
              settings[facet.name] = facet;
            });

            // Include additional settings for facet from the API response.
            facets.map(function(response) {
              if (!settings[response.name]) {
                return;
              }
              settings[response.name] = _extends(settings[response.name], response);
            });

            return settings;
          }

          /**
           * An array of facet related information that was defined when the Class was
           * instantiated.
           *
           * @property facetOptions
           * @return { Array }
           */

        }, {
          key: 'facetOptions',
          get: function get() {
            return this._facetOptions;
          }

          /**
           * String that identifies the name of the order parameter in the uri.
           *
           * @property orderParam
           * @return { String }
           */

        }, {
          key: 'orderParam',
          get: function get() {
            return this._orderParam;
          }

          /**
           * String that identifies the name of the sort parameter in the uri.
           *
           * @property sortParam
           * @return { String }
           */

        }, {
          key: 'sortParam',
          get: function get() {
            return this._sortParam;
          }

          /**
           * An array of sorting options returned in the API response.
           *
           * @property sorts
           * @return { Array }
           */

        }, {
          key: 'sorts',
          get: function get() {
            return this._meta.sorting;
          }
        }]);

        return Facets;
      }(_Pagination3.
        default);

      module.exports = Facets;

    }, {
      "./Pagination": 5
    }
  ],
  5: [
    function(require, module, exports) {
      'use strict';

      var _createClass = function() {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }
        return function(Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      var _Search2 = require('./Search');

      var _Search3 = _interopRequireDefault(_Search2);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      function _possibleConstructorReturn(self, call) {
        if (!self) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }
        return call && (typeof call === "object" || typeof call === "function") ? call : self;
      }

      function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
          throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }
        subClass.prototype = Object.create(superClass && superClass.prototype, {
          constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
          }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
      }

      /**
       * http://yui.github.io/yuidoc/syntax/index.html
       */

      var Pagination = function(_Search) {
        _inherits(Pagination, _Search);

        /**
         * Primary constructor for the Pagination class.
         *
         * @class Pagination
         * @constructor
         * @param { Config } config
         */
        function Pagination(config) {
          _classCallCheck(this, Pagination);

          var _this = _possibleConstructorReturn(this, (Pagination.__proto__ || Object.getPrototypeOf(Pagination)).call(this, config));
          // Call the parent class constructor.


          var pagerParam = config.pagerParam;


          _this._meta = null;
          _this._pagerParam = pagerParam || null;
          _this._pagerSeparator = {
            current: false,
            text: '...',
            link: null,
            value: null
          };

          _this._pagerStates = [{
            default: 3,
            threshold: 5
          }, {
            default: 3,
            threshold: 5
          }, {
            default: 3,
            threshold: 5
          }];
          return _this;
        }

        /**
         * Gets the current page being viewed from the query parameters.
         *
         * @property currentPage
         * @return { Number }
         */


        _createClass(Pagination, [{
          key: 'buildPager',


          /**
           * Primary function that builds and returns the pagination results.
           *
           * @method buildPager
           * @return { Array }
           */
          value: function buildPager() {
            var pager = this._pagerGroup1();

            var p2 = this._pagerGroup2(),
              p3 = this._pagerGroup3();

            pager = pager.concat(p2);
            pager = pager.concat(p3);

            return pager;
          }

          /**
           * Helper method that allows for the search results to move to the next page.
           *
           * @method nextPage
           * @return undefined
           */

        }, {
          key: 'nextPage',
          value: function nextPage() {
            var currentPage = this.currentPage,
              totalPages = this.totalPages,
              page = currentPage !== totalPages ? currentPage + 1 : currentPage;


            this._updateQueryParam('_page', page);
          }

          /**
           * Helper method that allows for the search results to move to the previous page.
           *
           * @method previousPage
           * @return undefined
           */

        }, {
          key: 'previousPage',
          value: function previousPage() {
            var currentPage = this.currentPage,
              totalPages = this.totalPages,
              page = currentPage > 1 ? Number(currentPage) - 1 : currentPage;


            this._updateQueryParam('_page', page);
          }

          /**
           * Builds Group 1 of the 3 pager groups being used.
           *
           * @private
           * @method _pagerGroup1
           * @return { Array }
           */

        }, {
          key: '_pagerGroup1',
          value: function _pagerGroup1() {
            var currentPage = this.currentPage,
              pagerSeparator = this.pagerSeparator,
              pagerStates = this.pagerStates,
              totalPages = this.totalPages,
              p1 = pagerStates[1],
              variance = p1.threshold - p1.
            default,
            firstPage = 1;

            // Showing only first page.

            var lastPage = firstPage;

            // Showing first 3 pages only.
            if (currentPage < p1.
              default) {
              lastPage = p1.
              default;
            }

            // Showing the first 5 pages.
            if (currentPage >= p1.
              default && currentPage < p1.threshold) {
              lastPage = p1.threshold;
            }

            // Show all 5 if there are only 5 total pages.
            if (currentPage === p1.threshold && totalPages === p1.threshold) {
              lastPage = p1.threshold;
            }

            // Show up to 5 if there are 5 or less pages.
            if (totalPages <= p1.threshold) {
              lastPage = totalPages;
            }

            // Show the next page unless we have reached the end "totalPages".
            lastPage = lastPage > totalPages ? totalPages : lastPage;

            var pages = this._buildPagerResults(firstPage, lastPage);
            if (firstPage === lastPage && lastPage !== totalPages) {
              pages = pages.concat([pagerSeparator]);
            }

            return pages;
          }

          /**
           * Builds Group 2 of the 3 pager groups being used.
           *
           * @private
           * @method _pagerGroup2
           * @return { Array }
           */

        }, {
          key: '_pagerGroup2',
          value: function _pagerGroup2() {
            var currentPage = this.currentPage,
              isFullPager = this.isFullPager,
              pagerSeparator = this.pagerSeparator,
              pagerStates = this.pagerStates,
              totalPages = this.totalPages,
              p1 = pagerStates[1],
              p3 = pagerStates[2];


            var firstPage = currentPage - 1,
              lastPage = currentPage + 1,
              pages = [];

            // Specifically handling a total of 5 pages.
            if (totalPages <= 5) {
              return pages;
            }

            // Common default parameters for a total page count of 6 or 7.
            if (totalPages === 6 || totalPages === 7) {
              firstPage = totalPages;
              lastPage = totalPages;

              if (currentPage < 3) {
                pages.push(pagerSeparator);
              }
            }

            // Customization for only a total of 6 pages.
            if (totalPages === 6 && currentPage >= 5) {
              firstPage = totalPages - 2;
            }

            // Customization for only a total of 7 pages.
            if (totalPages === 7) {
              if (currentPage === 5) {
                firstPage = totalPages - 3;
              }
              if (currentPage > 5) {
                firstPage = totalPages - 2;
              }
              if (currentPage >= 3 && currentPage < 5) {
                firstPage = lastPage - 1;
              }
            }

            // Accounts for any page number over or equal to 9.
            if (totalPages >= 8) {
              // Don't show within the p1 threshold.
              if (totalPages === p1.threshold || totalPages < p1.threshold || currentPage < p1.threshold) {
                return pages;
              }

              // Don't show within the p3 threshold.
              if (currentPage > totalPages - p3.threshold + 1 && !isFullPager) {
                return pages;
              }
            }

            // Show the next page unless we have reached the end "totalPages".
            lastPage = lastPage > totalPages ? totalPages : lastPage;

            var response = this._buildPagerResults(firstPage, lastPage);
            pages = pages.concat(response);
            return pages;
          }

          /**
           * Builds Group 3 of the 3 pager groups being used.
           *
           * @private
           * @method _pagerGroup3
           * @return { Array }
           */

        }, {
          key: '_pagerGroup3',
          value: function _pagerGroup3() {
            var currentPage = this.currentPage,
              isFullPager = this.isFullPager,
              pagerSeparator = this.pagerSeparator,
              pagerStates = this.pagerStates,
              totalPages = this.totalPages,
              p1 = pagerStates[0],
              p2 = pagerStates[1],
              p3 = pagerStates[2],
              p3Default = totalPages - p3.
            default +1,
            p3Threshold = totalPages - p3.threshold + 1;

            // Only show group 3 if we have more then 8 pages.

            if (isFullPager) {
              return [];
            }

            // Showing only last page.
            var firstPage = totalPages,
              lastPage = totalPages;

            // Showing last 3 pages only.
            if (currentPage > p3Default) {
              firstPage = p3Default;
            }

            // Showing the last 5 pages.
            if (currentPage === p3Default || currentPage > p3Threshold && currentPage < p3Default) {
              firstPage = p3Threshold;
            }

            // Show the next page unless we have reached the end "totalPages".
            lastPage = lastPage > totalPages ? totalPages : lastPage;

            var response = firstPage === lastPage ? [pagerSeparator] : [],
              pages = this._buildPagerResults(firstPage, lastPage);

            response = response.concat(pages);
            return response;
          }

          /**
           * Formats an individual pager in the required Object.
           *
           * @private
           * @method _buildPagerObject
           * @return { Object }
           */

        }, {
          key: '_buildPagerObject',
          value: function _buildPagerObject(link, text, value) {
            return {
              current: Number(this.currentPage) === Number(value),
              link: link,
              text: text,
              value: value
            };
          }

          /**
           * Generates and builds an array containing all the numbers between the
           * firstPage and lastPage.
           *
           * @private
           * @method _buildPagerGroup
           * @param { Number } firstPage
           * @param { Number } lastPage
           * @return { Array }
           */

        }, {
          key: '_buildPagerGroup',
          value: function _buildPagerGroup(firstPage, lastPage) {
            var options = [];
            for (var i = firstPage; i <= lastPage; i++) {
              options.push(this._buildPagerObject(null, i, i));
            }

            return options;
          }

          /**
           * Using the first and last pages this loops over each page building the
           * required output.
           *
           * @private
           * @method _buildPagerResults
           * @param { Number } firstPage
           * @param { Number } lastPage
           * @return { Array }
           */

        }, {
          key: '_buildPagerResults',
          value: function _buildPagerResults(firstPage, lastPage) {
            var options = this._buildPagerGroup(firstPage, lastPage);
            return options.map(function(page) {
              page.link = this._buildQueryURL(page.text);
              return page;
            }.bind(this));
          }

          /**
           * Constructs the full URL plus query parameters for the specific page.
           *
           * @private
           * @method _buildQueryURL
           * @param { Number } value
           * @return { String }
           */

        }, {
          key: '_buildQueryURL',
          value: function _buildQueryURL(value) {
            return this._buildQueryParamsFaker(this.pagerParam, value);
          }

          /**
           * Helper method to allow for quickly and easily going to a page.
           *
           * @private
           * @method _helperUpdatePage
           * @param { Number } page
           * @return undefined
           */

        }, {
          key: '_helperUpdatePage',
          value: function _helperUpdatePage(page) {
            this.query._page = page;
            this._updatePushState();
          }
        }, {
          key: 'currentPage',
          get: function get() {
            return Number(this.query._page);
          }

          /**
           * Returns the number of the first result being viewed. This value is not always
           * one as the first item is dependent on how many pages deep the viewer is.
           *
           * @property firstResult
           * @return { Number }
           */

        }, {
          key: 'firstResult',
          get: function get() {
            var currentPage = this.currentPage,
              limit = this.limit;

            return Number(Math.ceil(currentPage * limit - (limit - 1)));
          }

          /**
           * Determines if the full 3 Group pager is required based on the returned
           * results
           *
           * @property isFullPager
           * @return { Boolean } true|false
           */

        }, {
          key: 'isFullPager',
          get: function get() {
            var totalPages = this.totalPages;

            return totalPages < 8;
          }

          /**
           * Returns the current keywords being searched.
           *
           * @property keywords
           * @return { String }
           */

        }, {
          key: 'keywords',
          get: function get() {
            var keywordParam = this.keywordParam;

            return String(this.query[keywordParam]);
          }

          /**
           * Returns the number of the last result being viewed. This value is not the
           * total number of results, as the last item is dependent on what page the
           * viewer is on.
           *
           * @property lastResult
           * @return { Number }
           */

        }, {
          key: 'lastResult',
          get: function get() {
            var currentPage = this.currentPage,
              limit = this.limit,
              totalResults = this.totalResults;

            var last = Math.ceil(currentPage * limit);

            return Number(last > totalResults ? totalResults : last);
          }

          /**
           * Returns the current search limit being used.
           *
           * @property limit
           * @return { Number }
           */

        }, {
          key: 'limit',
          get: function get() {
            return Number(this.query._limit);
          }

          /**
           * Get & Set the pager parameter being used. The pager parameter is the query
           * parameter being used by this API. 90% of the time this will be '_page'.
           *
           * @property pagerParam
           * @return { String }
           */

        }, {
          key: 'pagerParam',
          get: function get() {
            return this._pagerParam;
          },
          set: function set(param) {
            this._pagerParam = param;
          }

          /**
           * Get & Set an object containing the default value and threshold value for
           * each of the three pager states being created. It is assumed that there
           * will only ever be 3 pager states.
           *
           * @property pagerStates
           * @return { Object }
           */

        }, {
          key: 'pagerStates',
          get: function get() {
            return this._pagerStates;
          },
          set: function set(states) {
            this._pagerStates = states;
          }

          /**
           * Get & Set the ... separator being used between each of the 3 pager states.
           *
           * @property pagerSeparator
           * @return { Object }
           */

        }, {
          key: 'pagerSeparator',
          get: function get() {
            return this._pagerSeparator;
          },
          set: function set(separator) {
            this._pagerSeparator = separator;
          }

          /**
           * Returns the total number of pages in the current search response.
           *
           * @property totalPages
           * @return { Number }
           */

        }, {
          key: 'totalPages',
          get: function get() {
            return Number(this.meta.totalPages);
          }

          /**
           * Returns the total number of results in the current search response.
           *
           * @property totalResults
           * @return { Number }
           */

        }, {
          key: 'totalResults',
          get: function get() {
            return Number(this.meta.totalResults);
          }
        }]);

        return Pagination;
      }(_Search3.
        default);

      module.exports = Pagination;

    }, {
      "./Search": 6
    }
  ],
  6: [
    function(require, module, exports) {
      'use strict';

      var _createClass = function() {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }
        return function(Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      require('whatwg-fetch');

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      /**
       * http://yui.github.io/yuidoc/syntax/index.html
       */

      var Search = function() {
        /**
         * Primary constructor for the Search class.
         *
         * @class Pagination
         * @constructor
         * @param { Config } config
         */
        function Search(config) {
          _classCallCheck(this, Search);

          var endpoint = config.endpoint,
            keywordParam = config.keywordParam,
            searchURL = config.searchURL,
            defaultQueryParams = config.defaultQueryParams;


          this._isLoading = false;
          this._defaultQueryParams = defaultQueryParams;
          this._queryParams = [];
          this._query = {};
          this._title = 'Search';
          this._endpoint = endpoint;
          this._keywordParam = keywordParam;
          this._searchURL = searchURL;
          this._fetchOptions = {
            method: 'GET'
          };
        }

        /**
         * Get the default query parameters that are set when the Class is instantiated.
         *
         * @property defaultQueryParams
         * @return { String }
         */


        _createClass(Search, [{
          key: 'compact',


          /**
           * Returns a new enumerable that excludes the passed value. The default
           * implementation returns an array regardless of the receiver type.
           * If the receiver does not contain the value it returns the original array.
           *
           * ```javascript
           * let arr = ['a', 'b', 'a', 'c'];
           * arr.compact('a');  // ['b', 'c']
           * ```
           *
           * @method compact
           * @param { array } itms
           * @return { Array }
           * @public
           */
          value: function compact(items) {
            return items.filter(function(value) {
              return value != null;
            });
          }

          /**
           * Allow for debouncing.
           *
           * @method debounce
           * @param { Function } func
           * @param { Number } wait
           * @param { Boolean } immediate
           * @return { Method }
           */

        }, {
          key: 'debounce',
          value: function debounce(func, wait, immediate) {
            var timeout = void 0;
            return function() {
              var context = this,
                args = arguments,
                later = function later() {
                  timeout = null;
                  if (!immediate) func.apply(context, args);
                };

              var callNow = immediate && !timeout;
              clearTimeout(timeout);

              timeout = setTimeout(later, wait);
              if (callNow) func.apply(context, args);
            };
          }

          /**
           * Init function to start the initial fetch request on page load.
           *
           * @method init
           * @return { Promise }
           */

        }, {
          key: 'init',
          value: function init() {
            var defaultQueryParams = this.defaultQueryParams,
              query = this.query,
              queryParams = this.queryParams,
              defaults = Object.keys(defaultQueryParams);


            var options = defaults.concat(queryParams),
              compact = this.compact(options),
              uniqe = this.uniq(compact);

            this.queryParams = uniqe;
            this.query = this._initQuery();

            return this._fetchRequest();
          }

          /**
           * Returns a new enumerable that contains only unique values. The default
           * implementation returns an array regardless of the receiver type.
           *
           * ```javascript
           * let arr = ['a', 'a', 'b', 'b'];
           * arr.uniq();  // ['a', 'b']
           * ```
           *
           * This only works on primitive data types, e.g. Strings, Numbers, etc.
           * @method uniq
           * @return { Array } items
           * @public
           */

        }, {
          key: 'uniq',
          value: function uniq(items) {
            var ret = [];
            items.forEach(function(k) {
              if (ret.indexOf(k) < 0) {
                ret.push(k);
              }
            });

            return ret;
          }

          /**
           * Processes requested template.
           *
           * @method template
           * @param { String } templateName
           * @param { Object } options
           * @return { HTML }
           */

        }, {
          key: 'template',
          value: function template(templateName, options) {
            var template = this.templates[templateName];
            options.map(function(_ref) {
              var variable = _ref.variable,
                value = _ref.value;

              template = template.replace(variable, value);
            });

            return template;
          }

          /**
           * Handles the primary fetch request.
           *
           * @private
           * @method _fetchRequest
           * @async
           */

        }, {
          key: '_fetchRequest',
          value: function _fetchRequest() {
            // Initiate the loading event.
            $(this).trigger('isLoading', [true]);

            var options = this.fetchOptions,
              uri = this._buildEndpointURL();

            return new Promise(function(resolve, reject) {
              fetch(uri, options).then(function(res) {
                return res.json();
              }).then(resolve).
              catch (reject);
            }).then(this._handleResponse.bind(this)).
            catch (this._handleError.bind(this));
          }

          /**
           * Builds the fetch endpoint and appends the endpoint to the formated query params.
           *
           * @private
           * @method _buildURL
           * @return { String }
           */

        }, {
          key: '_buildURL',
          value: function _buildURL() {
            var searchURL = this.searchURL,
              _buildQueryParams = this._buildQueryParams;

            return searchURL + '?' + _buildQueryParams;
          }

          /**
           * Constructs the URL for the API backend.
           *
           * @private
           * @method _buildEndpointURL
           * @return { String }
           */

        }, {
          key: '_buildEndpointURL',
          value: function _buildEndpointURL() {
            var endpoint = this.endpoint,
              _buildQueryParams = this._buildQueryParams;

            return endpoint + '?' + _buildQueryParams;
          }

          /**
           * Allows for URL's to be created for pagination without triggering an Ajax
           * request.
           *
           * @private
           * @method _buildQueryParamsFaker
           * @param { String } param
           * @param { String|Boolean } value
           * @return { String }
           */

        }, {
          key: '_buildQueryParamsFaker',
          value: function _buildQueryParamsFaker(param, value) {
            var current = param + '=' + this.query[param],
              faked = param + '=' + value;
            return this._buildURL().replace(current, faked);
          }

          /**
           * Handles Fetch error response.
           *
           * @private
           * @method _handleError
           * @param { JSON } Ajax error
           * @return undefined
           */

        }, {
          key: '_handleError',
          value: function _handleError(error) {
            $(this).trigger('isLoading', [false]).trigger('HandleError', [error]);
          }

          /**
           * Handles Fetch response and simple passes it to the client.
           *
           * @private
           * @method _handleResponse
           * @param { JSON } Ajax response
           * @return undefined
           */

        }, {
          key: '_handleResponse',
          value: function _handleResponse(response) {
            this.meta = response.meta;

            $(this).trigger('isLoading', [false]).trigger('HandleResponse', [response]);
          }

          /**
           * Helper function to quickly set or update search keywords.
           *
           * @private
           * @method _helperUpdateKeywords
           * @param { String } keywords
           * @return undefined
           */

        }, {
          key: '_helperUpdateKeywords',
          value: function _helperUpdateKeywords(keywords) {
            var keywordParam = this.keywordParam;

            this.query._page = 1;
            this.query[keywordParam] = keywords;
            this._updatePushState();
          }

          /**
           * Gets variables from url first then reverts to default values.
           *
           * @private
           * @method _initQuery
           * @return { Object }
           */

        }, {
          key: '_initQuery',
          value: function _initQuery() {
            var defaultQueryParams = this.defaultQueryParams,
              queryParams = this.queryParams;


            var defaults = defaultQueryParams;
            queryParams.map(function(query) {
              var context = query + '=([^&]*)',
                regex = new RegExp(context),
                values = window.location.search.match(regex);

              defaults[query] = !values ? defaults[query] : values[1];
            });

            return defaults;
          }
        }, {
          key: '_isEmptyQuery',
          value: function _isEmptyQuery(param, value) {
            var keywordParam = this.keywordParam;

            return !value && param !== keywordParam;
          }

          /**
           * Updates the history API and triggers the fetch call to update the content.
           *
           * @private
           * @method _updatePushState
           * @return undefined
           */

        }, {
          key: '_updatePushState',
          value: function _updatePushState() {
            var url = this._buildURL();
            history.pushState({
              search: url
            }, this.title, url);
            this._fetchRequest();
          }

          /**
           * Used to allow the frontend to update individual query parameters.
           *
           * @private
           * @method _updateQueryParam
           * @param { String } param
           * @param { String|Boolean } value
           * @return undefined
           */

        }, {
          key: '_updateQueryParam',
          value: function _updateQueryParam(param, value) {
            this.query[param] = value;
            this._updatePushState();
          }
        }, {
          key: 'defaultQueryParams',
          get: function get() {
            return this._defaultQueryParams;
          }

          /**
           * Get & Set the API endpoint being used.
           *
           * @property endpoint
           * @return { String }
           */

        }, {
          key: 'endpoint',
          get: function get() {
            return this._endpoint;
          },
          set: function set(endpoint) {
            this._endpoint = endpoint;
          }

          /**
           * Get & Set Fetch options that are sent the the Fetch command.
           *
           * @property fetchOptions
           * @return { Object }
           */

        }, {
          key: 'fetchOptions',
          get: function get() {
            return this._fetchOptions;
          },
          set: function set(options) {
            this._fetchOptions = options;
          }

          /**
           * Get the specified keyword parameter.
           *
           * @property fetchOptions
           * @return { Object }
           */

        }, {
          key: 'keywordParam',
          get: function get() {
            return this._keywordParam;
          }

          /**
           * Get & Set if the Fetch request either processing or sent a response.
           *
           * @property isLoading
           * @return { Boolean }
           */

        }, {
          key: 'isLoading',
          get: function get() {
            return this._isLoading;
          },
          set: function set(state) {
            this._isLoading = state;
          }

          /**
           * Gets & Set the meta response from the API.
           *
           * @property meta
           * @return { Object }
           */

        }, {
          key: 'meta',
          get: function get() {
            return this._meta;
          },
          set: function set(meta) {
            this._meta = meta;
          }

          /**
           * Get & Set the default query parameters that are used by default.
           *
           * @property query
           * @return { Object }
           */

        }, {
          key: 'query',
          get: function get() {
            return this._query;
          },
          set: function set(options) {
            this._query = options;
          }

          /**
           * Get & Set the query parameter being used.
           *
           * @property queryParams
           * @return { OBject }
           */

        }, {
          key: 'queryParams',
          get: function get() {
            return this._queryParams;
          },
          set: function set(params) {
            this._queryParams = params;
          }

          /**
           * Get & Set the UI URL being used. This is the page being displayed to the
           * user.
           *
           * @property searchURL
           * @return { String }
           */

        }, {
          key: 'searchURL',
          get: function get() {
            return this._searchURL;
          },
          set: function set(url) {
            this._searchURL = url;
          }

          /**
           * Get & Set all available templates.
           *
           * @property templates
           * @return { Object }
           */

        }, {
          key: 'templates',
          get: function get() {
            return this._templates;
          },
          set: function set(templates) {
            this._templates = templates;
          }

          /**
           * Get & Set the HTML5 History API page title being used.
           *
           * @property title
           * @return { String }
           */

        }, {
          key: 'title',
          get: function get() {
            return this._title;
          },
          set: function set(title) {
            this._title = title;
          }

          /**
           * Formats the available query params for the endpoint. Keep in mind that
           * there currently isn't any endpoint encoding applied.
           *
           * @private
           * @property _buildQueryParams
           * @return { String }
           */

        }, {
          key: '_buildQueryParams',
          get: function get() {
            var options = [],
              query = this.query;
            this.queryParams.map(function(key) {
              if (this._isEmptyQuery(key, query[key])) {
                return;
              }
              options.push(key + '=' + query[key]);
            }.bind(this));

            return options.join('&');
          }
        }]);

        return Search;
      }();

      module.exports = Search;

    }, {
      "whatwg-fetch": 10
    }
  ],
  7: [
    function(require, module, exports) {
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      var private_store = {
        data: "__weakmap_private_data__$",
        count: 0,
        id: function() {
          return [(Math.random() * 1e9) >>> 0, this.count++].join('_');
        },
        get: function(key) {
          if (!hasOwnProperty.call(key, this.data)) {
            Object.defineProperty(key, this.data, {
              value: Object.create(null)
            });
          }
          return key.__weakmap_private_data__$;
        },
        has: function(key, id) {
          return hasOwnProperty.call(key, this.data) && hasOwnProperty.call(key.__weakmap_private_data__$, id);
        }
      }

        function WeakMap() {
          Object.defineProperty(this, 'id', {
            value: private_store.id()
          });
        }
      WeakMap.prototype.set = function(key, value) {
        private_store.get(key)[this.id] = value;
      };

      WeakMap.prototype.get = function(key) {
        return private_store.get(key)[this.id];
      }

      WeakMap.prototype.has = function(key) {
        return private_store.has(key, this.id);
      }
      WeakMap.prototype.delete = function(key) {
        if (this.has(key)) {
          delete private_store.get(key)[this.id];
        }
      }

      module.exports = WeakMap;

    }, {}
  ],
  8: [
    function(require, module, exports) {
      (function(root) {

        // Store setTimeout reference so promise-polyfill will be unaffected by
        // other code modifying setTimeout (like sinon.useFakeTimers())
        var setTimeoutFunc = setTimeout;

        function noop() {}

        // Polyfill for Function.prototype.bind
        function bind(fn, thisArg) {
          return function() {
            fn.apply(thisArg, arguments);
          };
        }

        function Promise(fn) {
          if (!(this instanceof Promise)) throw new TypeError('Promises must be constructed via new');
          if (typeof fn !== 'function') throw new TypeError('not a function');
          this._state = 0;
          this._handled = false;
          this._value = undefined;
          this._deferreds = [];

          doResolve(fn, this);
        }

        function handle(self, deferred) {
          while (self._state === 3) {
            self = self._value;
          }
          if (self._state === 0) {
            self._deferreds.push(deferred);
            return;
          }
          self._handled = true;
          Promise._immediateFn(function() {
            var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
              (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
              return;
            }
            var ret;
            try {
              ret = cb(self._value);
            } catch (e) {
              reject(deferred.promise, e);
              return;
            }
            resolve(deferred.promise, ret);
          });
        }

        function resolve(self, newValue) {
          try {
            // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
            if (newValue && (typeof newValue === 'object' || typeof newValue === 'function')) {
              var then = newValue.then;
              if (newValue instanceof Promise) {
                self._state = 3;
                self._value = newValue;
                finale(self);
                return;
              } else if (typeof then === 'function') {
                doResolve(bind(then, newValue), self);
                return;
              }
            }
            self._state = 1;
            self._value = newValue;
            finale(self);
          } catch (e) {
            reject(self, e);
          }
        }

        function reject(self, newValue) {
          self._state = 2;
          self._value = newValue;
          finale(self);
        }

        function finale(self) {
          if (self._state === 2 && self._deferreds.length === 0) {
            Promise._immediateFn(function() {
              if (!self._handled) {
                Promise._unhandledRejectionFn(self._value);
              }
            });
          }

          for (var i = 0, len = self._deferreds.length; i < len; i++) {
            handle(self, self._deferreds[i]);
          }
          self._deferreds = null;
        }

        function Handler(onFulfilled, onRejected, promise) {
          this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
          this.onRejected = typeof onRejected === 'function' ? onRejected : null;
          this.promise = promise;
        }

        /**
         * Take a potentially misbehaving resolver function and make sure
         * onFulfilled and onRejected are only called once.
         *
         * Makes no guarantees about asynchrony.
         */
        function doResolve(fn, self) {
          var done = false;
          try {
            fn(function(value) {
              if (done) return;
              done = true;
              resolve(self, value);
            }, function(reason) {
              if (done) return;
              done = true;
              reject(self, reason);
            });
          } catch (ex) {
            if (done) return;
            done = true;
            reject(self, ex);
          }
        }

        Promise.prototype['catch'] = function(onRejected) {
          return this.then(null, onRejected);
        };

        Promise.prototype.then = function(onFulfilled, onRejected) {
          var prom = new(this.constructor)(noop);

          handle(this, new Handler(onFulfilled, onRejected, prom));
          return prom;
        };

        Promise.all = function(arr) {
          return new Promise(function(resolve, reject) {
            if (!arr || typeof arr.length === 'undefined') throw new TypeError('Promise.all accepts an array');
            var args = Array.prototype.slice.call(arr);
            if (args.length === 0) return resolve([]);
            var remaining = args.length;

            function res(i, val) {
              try {
                if (val && (typeof val === 'object' || typeof val === 'function')) {
                  var then = val.then;
                  if (typeof then === 'function') {
                    then.call(val, function(val) {
                      res(i, val);
                    }, reject);
                    return;
                  }
                }
                args[i] = val;
                if (--remaining === 0) {
                  resolve(args);
                }
              } catch (ex) {
                reject(ex);
              }
            }

            for (var i = 0; i < args.length; i++) {
              res(i, args[i]);
            }
          });
        };

        Promise.resolve = function(value) {
          if (value && typeof value === 'object' && value.constructor === Promise) {
            return value;
          }

          return new Promise(function(resolve) {
            resolve(value);
          });
        };

        Promise.reject = function(value) {
          return new Promise(function(resolve, reject) {
            reject(value);
          });
        };

        Promise.race = function(values) {
          return new Promise(function(resolve, reject) {
            for (var i = 0, len = values.length; i < len; i++) {
              values[i].then(resolve, reject);
            }
          });
        };

        // Use polyfill for setImmediate for performance gains
        Promise._immediateFn = (typeof setImmediate === 'function' && function(fn) {
          setImmediate(fn);
        }) ||
          function(fn) {
            setTimeoutFunc(fn, 0);
        };

        Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
          if (typeof console !== 'undefined' && console) {
            console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
          }
        };

        /**
         * Set the immediate function to execute callbacks
         * @param fn {function} Function to execute
         * @deprecated
         */
        Promise._setImmediateFn = function _setImmediateFn(fn) {
          Promise._immediateFn = fn;
        };

        /**
         * Change the function to execute on unhandled rejection
         * @param {function} fn Function to execute on unhandled rejection
         * @deprecated
         */
        Promise._setUnhandledRejectionFn = function _setUnhandledRejectionFn(fn) {
          Promise._unhandledRejectionFn = fn;
        };

        if (typeof module !== 'undefined' && module.exports) {
          module.exports = Promise;
        } else if (!root.Promise) {
          root.Promise = Promise;
        }

      })(this);

    }, {}
  ],
  9: [
    function(require, module, exports) {
      (function(global) {
        /*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.7.1
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
        /* global window, document, define, jQuery, setInterval, clearInterval */
        ;
        (function(factory) {
          'use strict';
          if (typeof define === 'function' && define.amd) {
            define(['jquery'], factory);
          } else if (typeof exports !== 'undefined') {
            module.exports = factory((typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null));
          } else {
            factory(jQuery);
          }

        }(function($) {
          'use strict';
          var Slick = window.Slick || {};

          Slick = (function() {

            var instanceUid = 0;

            function Slick(element, settings) {

              var _ = this,
                dataSettings;

              _.defaults = {
                accessibility: true,
                adaptiveHeight: false,
                appendArrows: $(element),
                appendDots: $(element),
                arrows: true,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                  return $('<button type="button" />').text(i + 1);
                },
                dots: false,
                dotsClass: 'slick-dots',
                draggable: true,
                easing: 'linear',
                edgeFriction: 0.35,
                fade: false,
                focusOnSelect: false,
                infinite: true,
                initialSlide: 0,
                lazyLoad: 'ondemand',
                mobileFirst: false,
                pauseOnHover: true,
                pauseOnFocus: true,
                pauseOnDotsHover: false,
                respondTo: 'window',
                responsive: null,
                rows: 1,
                rtl: false,
                slide: '',
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: true,
                swipeToSlide: false,
                touchMove: true,
                touchThreshold: 5,
                useCSS: true,
                useTransform: true,
                variableWidth: false,
                vertical: false,
                verticalSwiping: false,
                waitForAnimate: true,
                zIndex: 1000
              };

              _.initials = {
                animating: false,
                dragging: false,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: false,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                swiping: false,
                $list: null,
                touchObject: {},
                transformsEnabled: false,
                unslicked: false
              };

              $.extend(_, _.initials);

              _.activeBreakpoint = null;
              _.animType = null;
              _.animProp = null;
              _.breakpoints = [];
              _.breakpointSettings = [];
              _.cssTransitions = false;
              _.focussed = false;
              _.interrupted = false;
              _.hidden = 'hidden';
              _.paused = true;
              _.positionProp = null;
              _.respondTo = null;
              _.rowCount = 1;
              _.shouldClick = true;
              _.$slider = $(element);
              _.$slidesCache = null;
              _.transformType = null;
              _.transitionType = null;
              _.visibilityChange = 'visibilitychange';
              _.windowWidth = 0;
              _.windowTimer = null;

              dataSettings = $(element).data('slick') || {};

              _.options = $.extend({}, _.defaults, settings, dataSettings);

              _.currentSlide = _.options.initialSlide;

              _.originalSettings = _.options;

              if (typeof document.mozHidden !== 'undefined') {
                _.hidden = 'mozHidden';
                _.visibilityChange = 'mozvisibilitychange';
              } else if (typeof document.webkitHidden !== 'undefined') {
                _.hidden = 'webkitHidden';
                _.visibilityChange = 'webkitvisibilitychange';
              }

              _.autoPlay = $.proxy(_.autoPlay, _);
              _.autoPlayClear = $.proxy(_.autoPlayClear, _);
              _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
              _.changeSlide = $.proxy(_.changeSlide, _);
              _.clickHandler = $.proxy(_.clickHandler, _);
              _.selectHandler = $.proxy(_.selectHandler, _);
              _.setPosition = $.proxy(_.setPosition, _);
              _.swipeHandler = $.proxy(_.swipeHandler, _);
              _.dragHandler = $.proxy(_.dragHandler, _);
              _.keyHandler = $.proxy(_.keyHandler, _);

              _.instanceUid = instanceUid++;

              // A simple way to check for HTML strings
              // Strict HTML recognition (must start with <)
              // Extracted from jQuery v1.11 source
              _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;


              _.registerBreakpoints();
              _.init(true);

            }

            return Slick;

          }());

          Slick.prototype.activateADA = function() {
            var _ = this;

            _.$slideTrack.find('.slick-active').attr({
              'aria-hidden': 'false'
            }).find('a, input, button, select').attr({
              'tabindex': '0'
            });

          };

          Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {

            var _ = this;

            if (typeof(index) === 'boolean') {
              addBefore = index;
              index = null;
            } else if (index < 0 || (index >= _.slideCount)) {
              return false;
            }

            _.unload();

            if (typeof(index) === 'number') {
              if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
              } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
              } else {
                $(markup).insertAfter(_.$slides.eq(index));
              }
            } else {
              if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
              } else {
                $(markup).appendTo(_.$slideTrack);
              }
            }

            _.$slides = _.$slideTrack.children(this.options.slide);

            _.$slideTrack.children(this.options.slide).detach();

            _.$slideTrack.append(_.$slides);

            _.$slides.each(function(index, element) {
              $(element).attr('data-slick-index', index);
            });

            _.$slidesCache = _.$slides;

            _.reinit();

          };

          Slick.prototype.animateHeight = function() {
            var _ = this;
            if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
              var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
              _.$list.animate({
                height: targetHeight
              }, _.options.speed);
            }
          };

          Slick.prototype.animateSlide = function(targetLeft, callback) {

            var animProps = {},
              _ = this;

            _.animateHeight();

            if (_.options.rtl === true && _.options.vertical === false) {
              targetLeft = -targetLeft;
            }
            if (_.transformsEnabled === false) {
              if (_.options.vertical === false) {
                _.$slideTrack.animate({
                  left: targetLeft
                }, _.options.speed, _.options.easing, callback);
              } else {
                _.$slideTrack.animate({
                  top: targetLeft
                }, _.options.speed, _.options.easing, callback);
              }

            } else {

              if (_.cssTransitions === false) {
                if (_.options.rtl === true) {
                  _.currentLeft = -(_.currentLeft);
                }
                $({
                  animStart: _.currentLeft
                }).animate({
                  animStart: targetLeft
                }, {
                  duration: _.options.speed,
                  easing: _.options.easing,
                  step: function(now) {
                    now = Math.ceil(now);
                    if (_.options.vertical === false) {
                      animProps[_.animType] = 'translate(' +
                        now + 'px, 0px)';
                      _.$slideTrack.css(animProps);
                    } else {
                      animProps[_.animType] = 'translate(0px,' +
                        now + 'px)';
                      _.$slideTrack.css(animProps);
                    }
                  },
                  complete: function() {
                    if (callback) {
                      callback.call();
                    }
                  }
                });

              } else {

                _.applyTransition();
                targetLeft = Math.ceil(targetLeft);

                if (_.options.vertical === false) {
                  animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                  animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                  setTimeout(function() {

                    _.disableTransition();

                    callback.call();
                  }, _.options.speed);
                }

              }

            }

          };

          Slick.prototype.getNavTarget = function() {

            var _ = this,
              asNavFor = _.options.asNavFor;

            if (asNavFor && asNavFor !== null) {
              asNavFor = $(asNavFor).not(_.$slider);
            }

            return asNavFor;

          };

          Slick.prototype.asNavFor = function(index) {

            var _ = this,
              asNavFor = _.getNavTarget();

            if (asNavFor !== null && typeof asNavFor === 'object') {
              asNavFor.each(function() {
                var target = $(this).slick('getSlick');
                if (!target.unslicked) {
                  target.slideHandler(index, true);
                }
              });
            }

          };

          Slick.prototype.applyTransition = function(slide) {

            var _ = this,
              transition = {};

            if (_.options.fade === false) {
              transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
            } else {
              transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
            }

            if (_.options.fade === false) {
              _.$slideTrack.css(transition);
            } else {
              _.$slides.eq(slide).css(transition);
            }

          };

          Slick.prototype.autoPlay = function() {

            var _ = this;

            _.autoPlayClear();

            if (_.slideCount > _.options.slidesToShow) {
              _.autoPlayTimer = setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
            }

          };

          Slick.prototype.autoPlayClear = function() {

            var _ = this;

            if (_.autoPlayTimer) {
              clearInterval(_.autoPlayTimer);
            }

          };

          Slick.prototype.autoPlayIterator = function() {

            var _ = this,
              slideTo = _.currentSlide + _.options.slidesToScroll;

            if (!_.paused && !_.interrupted && !_.focussed) {

              if (_.options.infinite === false) {

                if (_.direction === 1 && (_.currentSlide + 1) === (_.slideCount - 1)) {
                  _.direction = 0;
                } else if (_.direction === 0) {

                  slideTo = _.currentSlide - _.options.slidesToScroll;

                  if (_.currentSlide - 1 === 0) {
                    _.direction = 1;
                  }

                }

              }

              _.slideHandler(slideTo);

            }

          };

          Slick.prototype.buildArrows = function() {

            var _ = this;

            if (_.options.arrows === true) {

              _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
              _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

              if (_.slideCount > _.options.slidesToShow) {

                _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
                _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

                if (_.htmlExpr.test(_.options.prevArrow)) {
                  _.$prevArrow.prependTo(_.options.appendArrows);
                }

                if (_.htmlExpr.test(_.options.nextArrow)) {
                  _.$nextArrow.appendTo(_.options.appendArrows);
                }

                if (_.options.infinite !== true) {
                  _.$prevArrow
                    .addClass('slick-disabled')
                    .attr('aria-disabled', 'true');
                }

              } else {

                _.$prevArrow.add(_.$nextArrow)

                .addClass('slick-hidden')
                  .attr({
                    'aria-disabled': 'true',
                    'tabindex': '-1'
                  });

              }

            }

          };

          Slick.prototype.buildDots = function() {

            var _ = this,
              i, dot;

            if (_.options.dots === true) {

              _.$slider.addClass('slick-dotted');

              dot = $('<ul />').addClass(_.options.dotsClass);

              for (i = 0; i <= _.getDotCount(); i += 1) {
                dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
              }

              _.$dots = dot.appendTo(_.options.appendDots);

              _.$dots.find('li').first().addClass('slick-active');

            }

          };

          Slick.prototype.buildOut = function() {

            var _ = this;

            _.$slides =
              _.$slider
              .children(_.options.slide + ':not(.slick-cloned)')
              .addClass('slick-slide');

            _.slideCount = _.$slides.length;

            _.$slides.each(function(index, element) {
              $(element)
                .attr('data-slick-index', index)
                .data('originalStyling', $(element).attr('style') || '');
            });

            _.$slider.addClass('slick-slider');

            _.$slideTrack = (_.slideCount === 0) ?
              $('<div class="slick-track"/>').appendTo(_.$slider) :
              _.$slides.wrapAll('<div class="slick-track"/>').parent();

            _.$list = _.$slideTrack.wrap(
              '<div class="slick-list"/>').parent();
            _.$slideTrack.css('opacity', 0);

            if (_.options.centerMode === true || _.options.swipeToSlide === true) {
              _.options.slidesToScroll = 1;
            }

            $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

            _.setupInfinite();

            _.buildArrows();

            _.buildDots();

            _.updateDots();


            _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

            if (_.options.draggable === true) {
              _.$list.addClass('draggable');
            }

          };

          Slick.prototype.buildRows = function() {

            var _ = this,
              a, b, c, newSlides, numOfSlides, originalSlides, slidesPerSection;

            newSlides = document.createDocumentFragment();
            originalSlides = _.$slider.children();

            if (_.options.rows > 1) {

              slidesPerSection = _.options.slidesPerRow * _.options.rows;
              numOfSlides = Math.ceil(
                originalSlides.length / slidesPerSection
              );

              for (a = 0; a < numOfSlides; a++) {
                var slide = document.createElement('div');
                for (b = 0; b < _.options.rows; b++) {
                  var row = document.createElement('div');
                  for (c = 0; c < _.options.slidesPerRow; c++) {
                    var target = (a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
                    if (originalSlides.get(target)) {
                      row.appendChild(originalSlides.get(target));
                    }
                  }
                  slide.appendChild(row);
                }
                newSlides.appendChild(slide);
              }

              _.$slider.empty().append(newSlides);
              _.$slider.children().children().children()
                .css({
                  'width': (100 / _.options.slidesPerRow) + '%',
                  'display': 'inline-block'
                });

            }

          };

          Slick.prototype.checkResponsive = function(initial, forceUpdate) {

            var _ = this,
              breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint = false;
            var sliderWidth = _.$slider.width();
            var windowWidth = window.innerWidth || $(window).width();

            if (_.respondTo === 'window') {
              respondToWidth = windowWidth;
            } else if (_.respondTo === 'slider') {
              respondToWidth = sliderWidth;
            } else if (_.respondTo === 'min') {
              respondToWidth = Math.min(windowWidth, sliderWidth);
            }

            if (_.options.responsive &&
              _.options.responsive.length &&
              _.options.responsive !== null) {

              targetBreakpoint = null;

              for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                  if (_.originalSettings.mobileFirst === false) {
                    if (respondToWidth < _.breakpoints[breakpoint]) {
                      targetBreakpoint = _.breakpoints[breakpoint];
                    }
                  } else {
                    if (respondToWidth > _.breakpoints[breakpoint]) {
                      targetBreakpoint = _.breakpoints[breakpoint];
                    }
                  }
                }
              }

              if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                  if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
                    _.activeBreakpoint =
                      targetBreakpoint;
                    if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                      _.unslick(targetBreakpoint);
                    } else {
                      _.options = $.extend({}, _.originalSettings,
                        _.breakpointSettings[
                          targetBreakpoint]);
                      if (initial === true) {
                        _.currentSlide = _.options.initialSlide;
                      }
                      _.refresh(initial);
                    }
                    triggerBreakpoint = targetBreakpoint;
                  }
                } else {
                  _.activeBreakpoint = targetBreakpoint;
                  if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                    _.unslick(targetBreakpoint);
                  } else {
                    _.options = $.extend({}, _.originalSettings,
                      _.breakpointSettings[
                        targetBreakpoint]);
                    if (initial === true) {
                      _.currentSlide = _.options.initialSlide;
                    }
                    _.refresh(initial);
                  }
                  triggerBreakpoint = targetBreakpoint;
                }
              } else {
                if (_.activeBreakpoint !== null) {
                  _.activeBreakpoint = null;
                  _.options = _.originalSettings;
                  if (initial === true) {
                    _.currentSlide = _.options.initialSlide;
                  }
                  _.refresh(initial);
                  triggerBreakpoint = targetBreakpoint;
                }
              }

              // only trigger breakpoints during an actual break. not on initialize.
              if (!initial && triggerBreakpoint !== false) {
                _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
              }
            }

          };

          Slick.prototype.changeSlide = function(event, dontAnimate) {

            var _ = this,
              $target = $(event.currentTarget),
              indexOffset, slideOffset, unevenOffset;

            // If target is a link, prevent default action.
            if ($target.is('a')) {
              event.preventDefault();
            }

            // If target is not the <li> element (ie: a child), find the <li>.
            if (!$target.is('li')) {
              $target = $target.closest('li');
            }

            unevenOffset = (_.slideCount % _.options.slidesToScroll !== 0);
            indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

            switch (event.data.message) {

              case 'previous':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                  _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
                }
                break;

              case 'next':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                  _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
                }
                break;

              case 'index':
                var index = event.data.index === 0 ? 0 :
                  event.data.index || $target.index() * _.options.slidesToScroll;

                _.slideHandler(_.checkNavigable(index), false, dontAnimate);
                $target.children().trigger('focus');
                break;

              default:
                return;
            }

          };

          Slick.prototype.checkNavigable = function(index) {

            var _ = this,
              navigables, prevNavigable;

            navigables = _.getNavigableIndexes();
            prevNavigable = 0;
            if (index > navigables[navigables.length - 1]) {
              index = navigables[navigables.length - 1];
            } else {
              for (var n in navigables) {
                if (index < navigables[n]) {
                  index = prevNavigable;
                  break;
                }
                prevNavigable = navigables[n];
              }
            }

            return index;
          };

          Slick.prototype.cleanUpEvents = function() {

            var _ = this;

            if (_.options.dots && _.$dots !== null) {

              $('li', _.$dots)
                .off('click.slick', _.changeSlide)
                .off('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .off('mouseleave.slick', $.proxy(_.interrupt, _, false));

              if (_.options.accessibility === true) {
                _.$dots.off('keydown.slick', _.keyHandler);
              }
            }

            _.$slider.off('focus.slick blur.slick');

            if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
              _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
              _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);

              if (_.options.accessibility === true) {
                _.$prevArrow.off('keydown.slick', _.keyHandler);
                _.$nextArrow.off('keydown.slick', _.keyHandler);
              }
            }

            _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
            _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
            _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
            _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

            _.$list.off('click.slick', _.clickHandler);

            $(document).off(_.visibilityChange, _.visibility);

            _.cleanUpSlideEvents();

            if (_.options.accessibility === true) {
              _.$list.off('keydown.slick', _.keyHandler);
            }

            if (_.options.focusOnSelect === true) {
              $(_.$slideTrack).children().off('click.slick', _.selectHandler);
            }

            $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);

            $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);

            $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);

            $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);

          };

          Slick.prototype.cleanUpSlideEvents = function() {

            var _ = this;

            _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
            _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));

          };

          Slick.prototype.cleanUpRows = function() {

            var _ = this,
              originalSlides;

            if (_.options.rows > 1) {
              originalSlides = _.$slides.children().children();
              originalSlides.removeAttr('style');
              _.$slider.empty().append(originalSlides);
            }

          };

          Slick.prototype.clickHandler = function(event) {

            var _ = this;

            if (_.shouldClick === false) {
              event.stopImmediatePropagation();
              event.stopPropagation();
              event.preventDefault();
            }

          };

          Slick.prototype.destroy = function(refresh) {

            var _ = this;

            _.autoPlayClear();

            _.touchObject = {};

            _.cleanUpEvents();

            $('.slick-cloned', _.$slider).detach();

            if (_.$dots) {
              _.$dots.remove();
            }

            if (_.$prevArrow && _.$prevArrow.length) {

              _.$prevArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display', '');

              if (_.htmlExpr.test(_.options.prevArrow)) {
                _.$prevArrow.remove();
              }
            }

            if (_.$nextArrow && _.$nextArrow.length) {

              _.$nextArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display', '');

              if (_.htmlExpr.test(_.options.nextArrow)) {
                _.$nextArrow.remove();
              }
            }


            if (_.$slides) {

              _.$slides
                .removeClass('slick-slide slick-active slick-center slick-visible slick-current')
                .removeAttr('aria-hidden')
                .removeAttr('data-slick-index')
                .each(function() {
                  $(this).attr('style', $(this).data('originalStyling'));
                });

              _.$slideTrack.children(this.options.slide).detach();

              _.$slideTrack.detach();

              _.$list.detach();

              _.$slider.append(_.$slides);
            }

            _.cleanUpRows();

            _.$slider.removeClass('slick-slider');
            _.$slider.removeClass('slick-initialized');
            _.$slider.removeClass('slick-dotted');

            _.unslicked = true;

            if (!refresh) {
              _.$slider.trigger('destroy', [_]);
            }

          };

          Slick.prototype.disableTransition = function(slide) {

            var _ = this,
              transition = {};

            transition[_.transitionType] = '';

            if (_.options.fade === false) {
              _.$slideTrack.css(transition);
            } else {
              _.$slides.eq(slide).css(transition);
            }

          };

          Slick.prototype.fadeSlide = function(slideIndex, callback) {

            var _ = this;

            if (_.cssTransitions === false) {

              _.$slides.eq(slideIndex).css({
                zIndex: _.options.zIndex
              });

              _.$slides.eq(slideIndex).animate({
                opacity: 1
              }, _.options.speed, _.options.easing, callback);

            } else {

              _.applyTransition(slideIndex);

              _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: _.options.zIndex
              });

              if (callback) {
                setTimeout(function() {

                  _.disableTransition(slideIndex);

                  callback.call();
                }, _.options.speed);
              }

            }

          };

          Slick.prototype.fadeSlideOut = function(slideIndex) {

            var _ = this;

            if (_.cssTransitions === false) {

              _.$slides.eq(slideIndex).animate({
                opacity: 0,
                zIndex: _.options.zIndex - 2
              }, _.options.speed, _.options.easing);

            } else {

              _.applyTransition(slideIndex);

              _.$slides.eq(slideIndex).css({
                opacity: 0,
                zIndex: _.options.zIndex - 2
              });

            }

          };

          Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {

            var _ = this;

            if (filter !== null) {

              _.$slidesCache = _.$slides;

              _.unload();

              _.$slideTrack.children(this.options.slide).detach();

              _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

              _.reinit();

            }

          };

          Slick.prototype.focusHandler = function() {

            var _ = this;

            _.$slider
              .off('focus.slick blur.slick')
              .on('focus.slick blur.slick', '*', function(event) {

                event.stopImmediatePropagation();
                var $sf = $(this);

                setTimeout(function() {

                  if (_.options.pauseOnFocus) {
                    _.focussed = $sf.is(':focus');
                    _.autoPlay();
                  }

                }, 0);

              });
          };

          Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {

            var _ = this;
            return _.currentSlide;

          };

          Slick.prototype.getDotCount = function() {

            var _ = this;

            var breakPoint = 0;
            var counter = 0;
            var pagerQty = 0;

            if (_.options.infinite === true) {
              if (_.slideCount <= _.options.slidesToShow) {
                ++pagerQty;
              } else {
                while (breakPoint < _.slideCount) {
                  ++pagerQty;
                  breakPoint = counter + _.options.slidesToScroll;
                  counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
                }
              }
            } else if (_.options.centerMode === true) {
              pagerQty = _.slideCount;
            } else if (!_.options.asNavFor) {
              pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
            } else {
              while (breakPoint < _.slideCount) {
                ++pagerQty;
                breakPoint = counter + _.options.slidesToScroll;
                counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
              }
            }

            return pagerQty - 1;

          };

          Slick.prototype.getLeft = function(slideIndex) {

            var _ = this,
              targetLeft,
              verticalHeight,
              verticalOffset = 0,
              targetSlide;

            _.slideOffset = 0;
            verticalHeight = _.$slides.first().outerHeight(true);

            if (_.options.infinite === true) {
              if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
                verticalOffset = (verticalHeight * _.options.slidesToShow) * -1;
              }
              if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                  if (slideIndex > _.slideCount) {
                    _.slideOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
                    verticalOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
                  } else {
                    _.slideOffset = ((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
                    verticalOffset = ((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
                  }
                }
              }
            } else {
              if (slideIndex + _.options.slidesToShow > _.slideCount) {
                _.slideOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
                verticalOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
              }
            }

            if (_.slideCount <= _.options.slidesToShow) {
              _.slideOffset = 0;
              verticalOffset = 0;
            }

            if (_.options.centerMode === true && _.slideCount <= _.options.slidesToShow) {
              _.slideOffset = ((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
            } else if (_.options.centerMode === true && _.options.infinite === true) {
              _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
            } else if (_.options.centerMode === true) {
              _.slideOffset = 0;
              _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
            }

            if (_.options.vertical === false) {
              targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
            } else {
              targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
            }

            if (_.options.variableWidth === true) {

              if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
              } else {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
              }

              if (_.options.rtl === true) {
                if (targetSlide[0]) {
                  targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                } else {
                  targetLeft = 0;
                }
              } else {
                targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
              }

              if (_.options.centerMode === true) {
                if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                  targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
                } else {
                  targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
                }

                if (_.options.rtl === true) {
                  if (targetSlide[0]) {
                    targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                  } else {
                    targetLeft = 0;
                  }
                } else {
                  targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
                }

                targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
              }
            }

            return targetLeft;

          };

          Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {

            var _ = this;

            return _.options[option];

          };

          Slick.prototype.getNavigableIndexes = function() {

            var _ = this,
              breakPoint = 0,
              counter = 0,
              indexes = [],
              max;

            if (_.options.infinite === false) {
              max = _.slideCount;
            } else {
              breakPoint = _.options.slidesToScroll * -1;
              counter = _.options.slidesToScroll * -1;
              max = _.slideCount * 2;
            }

            while (breakPoint < max) {
              indexes.push(breakPoint);
              breakPoint = counter + _.options.slidesToScroll;
              counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
            }

            return indexes;

          };

          Slick.prototype.getSlick = function() {

            return this;

          };

          Slick.prototype.getSlideCount = function() {

            var _ = this,
              slidesTraversed, swipedSlide, centerOffset;

            centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

            if (_.options.swipeToSlide === true) {
              _.$slideTrack.find('.slick-slide').each(function(index, slide) {
                if (slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)) {
                  swipedSlide = slide;
                  return false;
                }
              });

              slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;

              return slidesTraversed;

            } else {
              return _.options.slidesToScroll;
            }

          };

          Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {

            var _ = this;

            _.changeSlide({
              data: {
                message: 'index',
                index: parseInt(slide)
              }
            }, dontAnimate);

          };

          Slick.prototype.init = function(creation) {

            var _ = this;

            if (!$(_.$slider).hasClass('slick-initialized')) {

              $(_.$slider).addClass('slick-initialized');

              _.buildRows();
              _.buildOut();
              _.setProps();
              _.startLoad();
              _.loadSlider();
              _.initializeEvents();
              _.updateArrows();
              _.updateDots();
              _.checkResponsive(true);
              _.focusHandler();

            }

            if (creation) {
              _.$slider.trigger('init', [_]);
            }

            if (_.options.accessibility === true) {
              _.initADA();
            }

            if (_.options.autoplay) {

              _.paused = false;
              _.autoPlay();

            }

          };

          Slick.prototype.initADA = function() {
            var _ = this,
              numDotGroups = Math.ceil(_.slideCount / _.options.slidesToShow),
              tabControlIndexes = _.getNavigableIndexes().filter(function(val) {
                return (val >= 0) && (val < _.slideCount);
              });

            _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
              'aria-hidden': 'true',
              'tabindex': '-1'
            }).find('a, input, button, select').attr({
              'tabindex': '-1'
            });

            if (_.$dots !== null) {
              _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i) {
                var slideControlIndex = tabControlIndexes.indexOf(i);

                $(this).attr({
                  'role': 'tabpanel',
                  'id': 'slick-slide' + _.instanceUid + i,
                  'tabindex': -1
                });

                if (slideControlIndex !== -1) {
                  $(this).attr({
                    'aria-describedby': 'slick-slide-control' + _.instanceUid + slideControlIndex
                  });
                }
              });

              _.$dots.attr('role', 'tablist').find('li').each(function(i) {
                var mappedSlideIndex = tabControlIndexes[i];

                $(this).attr({
                  'role': 'presentation'
                });

                $(this).find('button').first().attr({
                  'role': 'tab',
                  'id': 'slick-slide-control' + _.instanceUid + i,
                  'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
                  'aria-label': (i + 1) + ' of ' + numDotGroups,
                  'aria-selected': null,
                  'tabindex': '-1'
                });

              }).eq(_.currentSlide).find('button').attr({
                'aria-selected': 'true',
                'tabindex': '0'
              }).end();
            }

            for (var i = _.currentSlide, max = i + _.options.slidesToShow; i < max; i++) {
              _.$slides.eq(i).attr('tabindex', 0);
            }

            _.activateADA();

          };

          Slick.prototype.initArrowEvents = function() {

            var _ = this;

            if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
              _.$prevArrow
                .off('click.slick')
                .on('click.slick', {
                  message: 'previous'
                }, _.changeSlide);
              _.$nextArrow
                .off('click.slick')
                .on('click.slick', {
                  message: 'next'
                }, _.changeSlide);

              if (_.options.accessibility === true) {
                _.$prevArrow.on('keydown.slick', _.keyHandler);
                _.$nextArrow.on('keydown.slick', _.keyHandler);
              }
            }

          };

          Slick.prototype.initDotEvents = function() {

            var _ = this;

            if (_.options.dots === true) {
              $('li', _.$dots).on('click.slick', {
                message: 'index'
              }, _.changeSlide);

              if (_.options.accessibility === true) {
                _.$dots.on('keydown.slick', _.keyHandler);
              }
            }

            if (_.options.dots === true && _.options.pauseOnDotsHover === true) {

              $('li', _.$dots)
                .on('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .on('mouseleave.slick', $.proxy(_.interrupt, _, false));

            }

          };

          Slick.prototype.initSlideEvents = function() {

            var _ = this;

            if (_.options.pauseOnHover) {

              _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
              _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));

            }

          };

          Slick.prototype.initializeEvents = function() {

            var _ = this;

            _.initArrowEvents();

            _.initDotEvents();
            _.initSlideEvents();

            _.$list.on('touchstart.slick mousedown.slick', {
              action: 'start'
            }, _.swipeHandler);
            _.$list.on('touchmove.slick mousemove.slick', {
              action: 'move'
            }, _.swipeHandler);
            _.$list.on('touchend.slick mouseup.slick', {
              action: 'end'
            }, _.swipeHandler);
            _.$list.on('touchcancel.slick mouseleave.slick', {
              action: 'end'
            }, _.swipeHandler);

            _.$list.on('click.slick', _.clickHandler);

            $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

            if (_.options.accessibility === true) {
              _.$list.on('keydown.slick', _.keyHandler);
            }

            if (_.options.focusOnSelect === true) {
              $(_.$slideTrack).children().on('click.slick', _.selectHandler);
            }

            $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));

            $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));

            $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);

            $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
            $(_.setPosition);

          };

          Slick.prototype.initUI = function() {

            var _ = this;

            if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

              _.$prevArrow.show();
              _.$nextArrow.show();

            }

            if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

              _.$dots.show();

            }

          };

          Slick.prototype.keyHandler = function(event) {

            var _ = this;
            //Dont slide if the cursor is inside the form fields and arrow keys are pressed
            if (!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
              if (event.keyCode === 37 && _.options.accessibility === true) {
                _.changeSlide({
                  data: {
                    message: _.options.rtl === true ? 'next' : 'previous'
                  }
                });
              } else if (event.keyCode === 39 && _.options.accessibility === true) {
                _.changeSlide({
                  data: {
                    message: _.options.rtl === true ? 'previous' : 'next'
                  }
                });
              }
            }

          };

          Slick.prototype.lazyLoad = function() {

            var _ = this,
              loadRange, cloneRange, rangeStart, rangeEnd;

            function loadImages(imagesScope) {

              $('img[data-lazy]', imagesScope).each(function() {

                var image = $(this),
                  imageSource = $(this).attr('data-lazy'),
                  imageSrcSet = $(this).attr('data-srcset'),
                  imageSizes = $(this).attr('data-sizes') || _.$slider.attr('data-sizes'),
                  imageToLoad = document.createElement('img');

                imageToLoad.onload = function() {

                  image
                    .animate({
                      opacity: 0
                    }, 100, function() {

                      if (imageSrcSet) {
                        image
                          .attr('srcset', imageSrcSet);

                        if (imageSizes) {
                          image
                            .attr('sizes', imageSizes);
                        }
                      }

                      image
                        .attr('src', imageSource)
                        .animate({
                          opacity: 1
                        }, 200, function() {
                          image
                            .removeAttr('data-lazy data-srcset data-sizes')
                            .removeClass('slick-loading');
                        });
                      _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
                    });

                };

                imageToLoad.onerror = function() {

                  image
                    .removeAttr('data-lazy')
                    .removeClass('slick-loading')
                    .addClass('slick-lazyload-error');

                  _.$slider.trigger('lazyLoadError', [_, image, imageSource]);

                };

                imageToLoad.src = imageSource;

              });

            }

            if (_.options.centerMode === true) {
              if (_.options.infinite === true) {
                rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
                rangeEnd = rangeStart + _.options.slidesToShow + 2;
              } else {
                rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
                rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
              }
            } else {
              rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
              rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);
              if (_.options.fade === true) {
                if (rangeStart > 0) rangeStart--;
                if (rangeEnd <= _.slideCount) rangeEnd++;
              }
            }

            loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);

            if (_.options.lazyLoad === 'anticipated') {
              var prevSlide = rangeStart - 1,
                nextSlide = rangeEnd,
                $slides = _.$slider.find('.slick-slide');

              for (var i = 0; i < _.options.slidesToScroll; i++) {
                if (prevSlide < 0) prevSlide = _.slideCount - 1;
                loadRange = loadRange.add($slides.eq(prevSlide));
                loadRange = loadRange.add($slides.eq(nextSlide));
                prevSlide--;
                nextSlide++;
              }
            }

            loadImages(loadRange);

            if (_.slideCount <= _.options.slidesToShow) {
              cloneRange = _.$slider.find('.slick-slide');
              loadImages(cloneRange);
            } else
            if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
              cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
              loadImages(cloneRange);
            } else if (_.currentSlide === 0) {
              cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
              loadImages(cloneRange);
            }

          };

          Slick.prototype.loadSlider = function() {

            var _ = this;

            _.setPosition();

            _.$slideTrack.css({
              opacity: 1
            });

            _.$slider.removeClass('slick-loading');

            _.initUI();

            if (_.options.lazyLoad === 'progressive') {
              _.progressiveLazyLoad();
            }

          };

          Slick.prototype.next = Slick.prototype.slickNext = function() {

            var _ = this;

            _.changeSlide({
              data: {
                message: 'next'
              }
            });

          };

          Slick.prototype.orientationChange = function() {

            var _ = this;

            _.checkResponsive();
            _.setPosition();

          };

          Slick.prototype.pause = Slick.prototype.slickPause = function() {

            var _ = this;

            _.autoPlayClear();
            _.paused = true;

          };

          Slick.prototype.play = Slick.prototype.slickPlay = function() {

            var _ = this;

            _.autoPlay();
            _.options.autoplay = true;
            _.paused = false;
            _.focussed = false;
            _.interrupted = false;

          };

          Slick.prototype.postSlide = function(index) {

            var _ = this;

            if (!_.unslicked) {

              _.$slider.trigger('afterChange', [_, index]);

              _.animating = false;

              if (_.slideCount > _.options.slidesToShow) {
                _.setPosition();
              }

              _.swipeLeft = null;

              if (_.options.autoplay) {
                _.autoPlay();
              }

              if (_.options.accessibility === true) {
                _.initADA();
                // for non-autoplay: once active slide (group) has updated, set focus on first newly showing slide 
                if (!_.options.autoplay) {
                  var $currentSlide = $(_.$slides.get(_.currentSlide));
                  $currentSlide.attr('tabindex', 0).focus();
                }
              }

            }

          };

          Slick.prototype.prev = Slick.prototype.slickPrev = function() {

            var _ = this;

            _.changeSlide({
              data: {
                message: 'previous'
              }
            });

          };

          Slick.prototype.preventDefault = function(event) {

            event.preventDefault();

          };

          Slick.prototype.progressiveLazyLoad = function(tryCount) {

            tryCount = tryCount || 1;

            var _ = this,
              $imgsToLoad = $('img[data-lazy]', _.$slider),
              image,
              imageSource,
              imageSrcSet,
              imageSizes,
              imageToLoad;

            if ($imgsToLoad.length) {

              image = $imgsToLoad.first();
              imageSource = image.attr('data-lazy');
              imageSrcSet = image.attr('data-srcset');
              imageSizes = image.attr('data-sizes') || _.$slider.attr('data-sizes');
              imageToLoad = document.createElement('img');

              imageToLoad.onload = function() {

                if (imageSrcSet) {
                  image
                    .attr('srcset', imageSrcSet);

                  if (imageSizes) {
                    image
                      .attr('sizes', imageSizes);
                  }
                }

                image
                  .attr('src', imageSource)
                  .removeAttr('data-lazy data-srcset data-sizes')
                  .removeClass('slick-loading');

                if (_.options.adaptiveHeight === true) {
                  _.setPosition();
                }

                _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
                _.progressiveLazyLoad();

              };

              imageToLoad.onerror = function() {

                if (tryCount < 3) {

                  /**
                   * try to load the image 3 times,
                   * leave a slight delay so we don't get
                   * servers blocking the request.
                   */
                  setTimeout(function() {
                    _.progressiveLazyLoad(tryCount + 1);
                  }, 500);

                } else {

                  image
                    .removeAttr('data-lazy')
                    .removeClass('slick-loading')
                    .addClass('slick-lazyload-error');

                  _.$slider.trigger('lazyLoadError', [_, image, imageSource]);

                  _.progressiveLazyLoad();

                }

              };

              imageToLoad.src = imageSource;

            } else {

              _.$slider.trigger('allImagesLoaded', [_]);

            }

          };

          Slick.prototype.refresh = function(initializing) {

            var _ = this,
              currentSlide, lastVisibleIndex;

            lastVisibleIndex = _.slideCount - _.options.slidesToShow;

            // in non-infinite sliders, we don't want to go past the
            // last visible index.
            if (!_.options.infinite && (_.currentSlide > lastVisibleIndex)) {
              _.currentSlide = lastVisibleIndex;
            }

            // if less slides than to show, go to start.
            if (_.slideCount <= _.options.slidesToShow) {
              _.currentSlide = 0;

            }

            currentSlide = _.currentSlide;

            _.destroy(true);

            $.extend(_, _.initials, {
              currentSlide: currentSlide
            });

            _.init();

            if (!initializing) {

              _.changeSlide({
                data: {
                  message: 'index',
                  index: currentSlide
                }
              }, false);

            }

          };

          Slick.prototype.registerBreakpoints = function() {

            var _ = this,
              breakpoint, currentBreakpoint, l,
              responsiveSettings = _.options.responsive || null;

            if ($.type(responsiveSettings) === 'array' && responsiveSettings.length) {

              _.respondTo = _.options.respondTo || 'window';

              for (breakpoint in responsiveSettings) {

                l = _.breakpoints.length - 1;

                if (responsiveSettings.hasOwnProperty(breakpoint)) {
                  currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

                  // loop through the breakpoints and cut out any existing
                  // ones with the same breakpoint number, we don't want dupes.
                  while (l >= 0) {
                    if (_.breakpoints[l] && _.breakpoints[l] === currentBreakpoint) {
                      _.breakpoints.splice(l, 1);
                    }
                    l--;
                  }

                  _.breakpoints.push(currentBreakpoint);
                  _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;

                }

              }

              _.breakpoints.sort(function(a, b) {
                return (_.options.mobileFirst) ? a - b : b - a;
              });

            }

          };

          Slick.prototype.reinit = function() {

            var _ = this;

            _.$slides =
              _.$slideTrack
              .children(_.options.slide)
              .addClass('slick-slide');

            _.slideCount = _.$slides.length;

            if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
              _.currentSlide = _.currentSlide - _.options.slidesToScroll;
            }

            if (_.slideCount <= _.options.slidesToShow) {
              _.currentSlide = 0;
            }

            _.registerBreakpoints();

            _.setProps();
            _.setupInfinite();
            _.buildArrows();
            _.updateArrows();
            _.initArrowEvents();
            _.buildDots();
            _.updateDots();
            _.initDotEvents();
            _.cleanUpSlideEvents();
            _.initSlideEvents();

            _.checkResponsive(false, true);

            if (_.options.focusOnSelect === true) {
              $(_.$slideTrack).children().on('click.slick', _.selectHandler);
            }

            _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

            _.setPosition();
            _.focusHandler();

            _.paused = !_.options.autoplay;
            _.autoPlay();

            _.$slider.trigger('reInit', [_]);

          };

          Slick.prototype.resize = function() {

            var _ = this;

            if ($(window).width() !== _.windowWidth) {
              clearTimeout(_.windowDelay);
              _.windowDelay = window.setTimeout(function() {
                _.windowWidth = $(window).width();
                _.checkResponsive();
                if (!_.unslicked) {
                  _.setPosition();
                }
              }, 50);
            }
          };

          Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {

            var _ = this;

            if (typeof(index) === 'boolean') {
              removeBefore = index;
              index = removeBefore === true ? 0 : _.slideCount - 1;
            } else {
              index = removeBefore === true ? --index : index;
            }

            if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
              return false;
            }

            _.unload();

            if (removeAll === true) {
              _.$slideTrack.children().remove();
            } else {
              _.$slideTrack.children(this.options.slide).eq(index).remove();
            }

            _.$slides = _.$slideTrack.children(this.options.slide);

            _.$slideTrack.children(this.options.slide).detach();

            _.$slideTrack.append(_.$slides);

            _.$slidesCache = _.$slides;

            _.reinit();

          };

          Slick.prototype.setCSS = function(position) {

            var _ = this,
              positionProps = {},
              x, y;

            if (_.options.rtl === true) {
              position = -position;
            }
            x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
            y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';

            positionProps[_.positionProp] = position;

            if (_.transformsEnabled === false) {
              _.$slideTrack.css(positionProps);
            } else {
              positionProps = {};
              if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
              } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
              }
            }

          };

          Slick.prototype.setDimensions = function() {

            var _ = this;

            if (_.options.vertical === false) {
              if (_.options.centerMode === true) {
                _.$list.css({
                  padding: ('0px ' + _.options.centerPadding)
                });
              }
            } else {
              _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
              if (_.options.centerMode === true) {
                _.$list.css({
                  padding: (_.options.centerPadding + ' 0px')
                });
              }
            }

            _.listWidth = _.$list.width();
            _.listHeight = _.$list.height();


            if (_.options.vertical === false && _.options.variableWidth === false) {
              _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
              _.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));

            } else if (_.options.variableWidth === true) {
              _.$slideTrack.width(5000 * _.slideCount);
            } else {
              _.slideWidth = Math.ceil(_.listWidth);
              _.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
            }

            var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
            if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);

          };

          Slick.prototype.setFade = function() {

            var _ = this,
              targetLeft;

            _.$slides.each(function(index, element) {
              targetLeft = (_.slideWidth * index) * -1;
              if (_.options.rtl === true) {
                $(element).css({
                  position: 'relative',
                  right: targetLeft,
                  top: 0,
                  zIndex: _.options.zIndex - 2,
                  opacity: 0
                });
              } else {
                $(element).css({
                  position: 'relative',
                  left: targetLeft,
                  top: 0,
                  zIndex: _.options.zIndex - 2,
                  opacity: 0
                });
              }
            });

            _.$slides.eq(_.currentSlide).css({
              zIndex: _.options.zIndex - 1,
              opacity: 1
            });

          };

          Slick.prototype.setHeight = function() {

            var _ = this;

            if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
              var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
              _.$list.css('height', targetHeight);
            }

          };

          Slick.prototype.setOption =
            Slick.prototype.slickSetOption = function() {

              /**
               * accepts arguments in format of:
               *
               *  - for changing a single option's value:
               *     .slick("setOption", option, value, refresh )
               *
               *  - for changing a set of responsive options:
               *     .slick("setOption", 'responsive', [{}, ...], refresh )
               *
               *  - for updating multiple values at once (not responsive)
               *     .slick("setOption", { 'option': value, ... }, refresh )
               */

              var _ = this,
                l, item, option, value, refresh = false,
                type;

              if ($.type(arguments[0]) === 'object') {

                option = arguments[0];
                refresh = arguments[1];
                type = 'multiple';

              } else if ($.type(arguments[0]) === 'string') {

                option = arguments[0];
                value = arguments[1];
                refresh = arguments[2];

                if (arguments[0] === 'responsive' && $.type(arguments[1]) === 'array') {

                  type = 'responsive';

                } else if (typeof arguments[1] !== 'undefined') {

                  type = 'single';

                }

              }

              if (type === 'single') {

                _.options[option] = value;


              } else if (type === 'multiple') {

                $.each(option, function(opt, val) {

                  _.options[opt] = val;

                });


              } else if (type === 'responsive') {

                for (item in value) {

                  if ($.type(_.options.responsive) !== 'array') {

                    _.options.responsive = [value[item]];

                  } else {

                    l = _.options.responsive.length - 1;

                    // loop through the responsive object and splice out duplicates.
                    while (l >= 0) {

                      if (_.options.responsive[l].breakpoint === value[item].breakpoint) {

                        _.options.responsive.splice(l, 1);

                      }

                      l--;

                    }

                    _.options.responsive.push(value[item]);

                  }

                }

              }

              if (refresh) {

                _.unload();
                _.reinit();

              }

          };

          Slick.prototype.setPosition = function() {

            var _ = this;

            _.setDimensions();

            _.setHeight();

            if (_.options.fade === false) {
              _.setCSS(_.getLeft(_.currentSlide));
            } else {
              _.setFade();
            }

            _.$slider.trigger('setPosition', [_]);

          };

          Slick.prototype.setProps = function() {

            var _ = this,
              bodyStyle = document.body.style;

            _.positionProp = _.options.vertical === true ? 'top' : 'left';

            if (_.positionProp === 'top') {
              _.$slider.addClass('slick-vertical');
            } else {
              _.$slider.removeClass('slick-vertical');
            }

            if (bodyStyle.WebkitTransition !== undefined ||
              bodyStyle.MozTransition !== undefined ||
              bodyStyle.msTransition !== undefined) {
              if (_.options.useCSS === true) {
                _.cssTransitions = true;
              }
            }

            if (_.options.fade) {
              if (typeof _.options.zIndex === 'number') {
                if (_.options.zIndex < 3) {
                  _.options.zIndex = 3;
                }
              } else {
                _.options.zIndex = _.defaults.zIndex;
              }
            }

            if (bodyStyle.OTransform !== undefined) {
              _.animType = 'OTransform';
              _.transformType = '-o-transform';
              _.transitionType = 'OTransition';
              if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
            }
            if (bodyStyle.MozTransform !== undefined) {
              _.animType = 'MozTransform';
              _.transformType = '-moz-transform';
              _.transitionType = 'MozTransition';
              if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
            }
            if (bodyStyle.webkitTransform !== undefined) {
              _.animType = 'webkitTransform';
              _.transformType = '-webkit-transform';
              _.transitionType = 'webkitTransition';
              if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
            }
            if (bodyStyle.msTransform !== undefined) {
              _.animType = 'msTransform';
              _.transformType = '-ms-transform';
              _.transitionType = 'msTransition';
              if (bodyStyle.msTransform === undefined) _.animType = false;
            }
            if (bodyStyle.transform !== undefined && _.animType !== false) {
              _.animType = 'transform';
              _.transformType = 'transform';
              _.transitionType = 'transition';
            }
            _.transformsEnabled = _.options.useTransform && (_.animType !== null && _.animType !== false);
          };


          Slick.prototype.setSlideClasses = function(index) {

            var _ = this,
              centerOffset, allSlides, indexOffset, remainder;

            allSlides = _.$slider
              .find('.slick-slide')
              .removeClass('slick-active slick-center slick-current')
              .attr('aria-hidden', 'true');

            _.$slides
              .eq(index)
              .addClass('slick-current');

            if (_.options.centerMode === true) {

              centerOffset = Math.floor(_.options.slidesToShow / 2);

              if (_.options.infinite === true) {

                if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {

                  _.$slides
                    .slice(index - centerOffset, index + centerOffset + 1)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

                } else {

                  indexOffset = _.options.slidesToShow + index;
                  allSlides
                    .slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

                }

                if (index === 0) {

                  allSlides
                    .eq(allSlides.length - 1 - _.options.slidesToShow)
                    .addClass('slick-center');

                } else if (index === _.slideCount - 1) {

                  allSlides
                    .eq(_.options.slidesToShow)
                    .addClass('slick-center');

                }

              }

              _.$slides
                .eq(index)
                .addClass('slick-center');

            } else {

              if (index >= 0 && index <= (_.slideCount - _.options.slidesToShow)) {

                _.$slides
                  .slice(index, index + _.options.slidesToShow)
                  .addClass('slick-active')
                  .attr('aria-hidden', 'false');

              } else if (allSlides.length <= _.options.slidesToShow) {

                allSlides
                  .addClass('slick-active')
                  .attr('aria-hidden', 'false');

              } else {

                remainder = _.slideCount % _.options.slidesToShow;
                indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

                if (_.options.slidesToShow == _.options.slidesToScroll && (_.slideCount - index) < _.options.slidesToShow) {

                  allSlides
                    .slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

                } else {

                  allSlides
                    .slice(indexOffset, indexOffset + _.options.slidesToShow)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

                }

              }

            }

            if (_.options.lazyLoad === 'ondemand' || _.options.lazyLoad === 'anticipated') {
              _.lazyLoad();
            }
          };

          Slick.prototype.setupInfinite = function() {

            var _ = this,
              i, slideIndex, infiniteCount;

            if (_.options.fade === true) {
              _.options.centerMode = false;
            }

            if (_.options.infinite === true && _.options.fade === false) {

              slideIndex = null;

              if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                  infiniteCount = _.options.slidesToShow + 1;
                } else {
                  infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                  infiniteCount); i -= 1) {
                  slideIndex = i - 1;
                  $(_.$slides[slideIndex]).clone(true).attr('id', '')
                    .attr('data-slick-index', slideIndex - _.slideCount)
                    .prependTo(_.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount + _.slideCount; i += 1) {
                  slideIndex = i;
                  $(_.$slides[slideIndex]).clone(true).attr('id', '')
                    .attr('data-slick-index', slideIndex + _.slideCount)
                    .appendTo(_.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                  $(this).attr('id', '');
                });

              }

            }

          };

          Slick.prototype.interrupt = function(toggle) {

            var _ = this;

            if (!toggle) {
              _.autoPlay();
            }
            _.interrupted = toggle;

          };

          Slick.prototype.selectHandler = function(event) {

            var _ = this;

            var targetElement =
              $(event.target).is('.slick-slide') ?
              $(event.target) :
              $(event.target).parents('.slick-slide');

            var index = parseInt(targetElement.attr('data-slick-index'));

            if (!index) index = 0;

            if (_.slideCount <= _.options.slidesToShow) {

              _.slideHandler(index, false, true);
              return;

            }

            _.slideHandler(index);

          };

          Slick.prototype.slideHandler = function(index, sync, dontAnimate) {

            var targetSlide, animSlide, oldSlide, slideLeft, targetLeft = null,
              _ = this,
              navTarget;

            sync = sync || false;

            if (_.animating === true && _.options.waitForAnimate === true) {
              return;
            }

            if (_.options.fade === true && _.currentSlide === index) {
              return;
            }

            if (sync === false) {
              _.asNavFor(index);
            }

            targetSlide = index;
            targetLeft = _.getLeft(targetSlide);
            slideLeft = _.getLeft(_.currentSlide);

            _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

            if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
              if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true) {
                  _.animateSlide(slideLeft, function() {
                    _.postSlide(targetSlide);
                  });
                } else {
                  _.postSlide(targetSlide);
                }
              }
              return;
            } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > (_.slideCount - _.options.slidesToScroll))) {
              if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true) {
                  _.animateSlide(slideLeft, function() {
                    _.postSlide(targetSlide);
                  });
                } else {
                  _.postSlide(targetSlide);
                }
              }
              return;
            }

            if (_.options.autoplay) {
              clearInterval(_.autoPlayTimer);
            }

            if (targetSlide < 0) {
              if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
              } else {
                animSlide = _.slideCount + targetSlide;
              }
            } else if (targetSlide >= _.slideCount) {
              if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = 0;
              } else {
                animSlide = targetSlide - _.slideCount;
              }
            } else {
              animSlide = targetSlide;
            }

            _.animating = true;

            _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

            oldSlide = _.currentSlide;
            _.currentSlide = animSlide;

            _.setSlideClasses(_.currentSlide);

            if (_.options.asNavFor) {

              navTarget = _.getNavTarget();
              navTarget = navTarget.slick('getSlick');

              if (navTarget.slideCount <= navTarget.options.slidesToShow) {
                navTarget.setSlideClasses(_.currentSlide);
              }

            }

            _.updateDots();
            _.updateArrows();

            if (_.options.fade === true) {
              if (dontAnimate !== true) {

                _.fadeSlideOut(oldSlide);

                _.fadeSlide(animSlide, function() {
                  _.postSlide(animSlide);
                });

              } else {
                _.postSlide(animSlide);
              }
              _.animateHeight();
              return;
            }

            if (dontAnimate !== true) {
              _.animateSlide(targetLeft, function() {
                _.postSlide(animSlide);
              });
            } else {
              _.postSlide(animSlide);
            }

          };

          Slick.prototype.startLoad = function() {

            var _ = this;

            if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

              _.$prevArrow.hide();
              _.$nextArrow.hide();

            }

            if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

              _.$dots.hide();

            }

            _.$slider.addClass('slick-loading');

          };

          Slick.prototype.swipeDirection = function() {

            var xDist, yDist, r, swipeAngle, _ = this;

            xDist = _.touchObject.startX - _.touchObject.curX;
            yDist = _.touchObject.startY - _.touchObject.curY;
            r = Math.atan2(yDist, xDist);

            swipeAngle = Math.round(r * 180 / Math.PI);
            if (swipeAngle < 0) {
              swipeAngle = 360 - Math.abs(swipeAngle);
            }

            if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
              return (_.options.rtl === false ? 'left' : 'right');
            }
            if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
              return (_.options.rtl === false ? 'left' : 'right');
            }
            if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
              return (_.options.rtl === false ? 'right' : 'left');
            }
            if (_.options.verticalSwiping === true) {
              if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
                return 'down';
              } else {
                return 'up';
              }
            }

            return 'vertical';

          };

          Slick.prototype.swipeEnd = function(event) {

            var _ = this,
              slideCount,
              direction;

            _.dragging = false;
            _.swiping = false;

            if (_.scrolling) {
              _.scrolling = false;
              return false;
            }

            _.interrupted = false;
            _.shouldClick = (_.touchObject.swipeLength > 10) ? false : true;

            if (_.touchObject.curX === undefined) {
              return false;
            }

            if (_.touchObject.edgeHit === true) {
              _.$slider.trigger('edge', [_, _.swipeDirection()]);
            }

            if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {

              direction = _.swipeDirection();

              switch (direction) {

                case 'left':
                case 'down':

                  slideCount =
                    _.options.swipeToSlide ?
                    _.checkNavigable(_.currentSlide + _.getSlideCount()) :
                    _.currentSlide + _.getSlideCount();

                  _.currentDirection = 0;

                  break;

                case 'right':
                case 'up':

                  slideCount =
                    _.options.swipeToSlide ?
                    _.checkNavigable(_.currentSlide - _.getSlideCount()) :
                    _.currentSlide - _.getSlideCount();

                  _.currentDirection = 1;

                  break;

                default:


              }

              if (direction != 'vertical') {

                _.slideHandler(slideCount);
                _.touchObject = {};
                _.$slider.trigger('swipe', [_, direction]);

              }

            } else {

              if (_.touchObject.startX !== _.touchObject.curX) {

                _.slideHandler(_.currentSlide);
                _.touchObject = {};

              }

            }

          };

          Slick.prototype.swipeHandler = function(event) {

            var _ = this;

            if ((_.options.swipe === false) || ('ontouchend' in document && _.options.swipe === false)) {
              return;
            } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
              return;
            }

            _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
              event.originalEvent.touches.length : 1;

            _.touchObject.minSwipe = _.listWidth / _.options
              .touchThreshold;

            if (_.options.verticalSwiping === true) {
              _.touchObject.minSwipe = _.listHeight / _.options
                .touchThreshold;
            }

            switch (event.data.action) {

              case 'start':
                _.swipeStart(event);
                break;

              case 'move':
                _.swipeMove(event);
                break;

              case 'end':
                _.swipeEnd(event);
                break;

            }

          };

          Slick.prototype.swipeMove = function(event) {

            var _ = this,
              edgeWasHit = false,
              curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;

            touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

            if (!_.dragging || _.scrolling || touches && touches.length !== 1) {
              return false;
            }

            curLeft = _.getLeft(_.currentSlide);

            _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
            _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

            _.touchObject.swipeLength = Math.round(Math.sqrt(
              Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

            verticalSwipeLength = Math.round(Math.sqrt(
              Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));

            if (!_.options.verticalSwiping && !_.swiping && verticalSwipeLength > 4) {
              _.scrolling = true;
              return false;
            }

            if (_.options.verticalSwiping === true) {
              _.touchObject.swipeLength = verticalSwipeLength;
            }

            swipeDirection = _.swipeDirection();

            if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
              _.swiping = true;
              event.preventDefault();
            }

            positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);
            if (_.options.verticalSwiping === true) {
              positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
            }


            swipeLength = _.touchObject.swipeLength;

            _.touchObject.edgeHit = false;

            if (_.options.infinite === false) {
              if ((_.currentSlide === 0 && swipeDirection === 'right') || (_.currentSlide >= _.getDotCount() && swipeDirection === 'left')) {
                swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
                _.touchObject.edgeHit = true;
              }
            }

            if (_.options.vertical === false) {
              _.swipeLeft = curLeft + swipeLength * positionOffset;
            } else {
              _.swipeLeft = curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
            }
            if (_.options.verticalSwiping === true) {
              _.swipeLeft = curLeft + swipeLength * positionOffset;
            }

            if (_.options.fade === true || _.options.touchMove === false) {
              return false;
            }

            if (_.animating === true) {
              _.swipeLeft = null;
              return false;
            }

            _.setCSS(_.swipeLeft);

          };

          Slick.prototype.swipeStart = function(event) {

            var _ = this,
              touches;

            _.interrupted = true;

            if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
              _.touchObject = {};
              return false;
            }

            if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
              touches = event.originalEvent.touches[0];
            }

            _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
            _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

            _.dragging = true;

          };

          Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {

            var _ = this;

            if (_.$slidesCache !== null) {

              _.unload();

              _.$slideTrack.children(this.options.slide).detach();

              _.$slidesCache.appendTo(_.$slideTrack);

              _.reinit();

            }

          };

          Slick.prototype.unload = function() {

            var _ = this;

            $('.slick-cloned', _.$slider).remove();

            if (_.$dots) {
              _.$dots.remove();
            }

            if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
              _.$prevArrow.remove();
            }

            if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
              _.$nextArrow.remove();
            }

            _.$slides
              .removeClass('slick-slide slick-active slick-visible slick-current')
              .attr('aria-hidden', 'true')
              .css('width', '');

          };

          Slick.prototype.unslick = function(fromBreakpoint) {

            var _ = this;
            _.$slider.trigger('unslick', [_, fromBreakpoint]);
            _.destroy();

          };

          Slick.prototype.updateArrows = function() {

            var _ = this,
              centerOffset;

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if (_.options.arrows === true &&
              _.slideCount > _.options.slidesToShow && !_.options.infinite) {

              _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
              _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

              if (_.currentSlide === 0) {

                _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

              } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

              } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

              }

            }

          };

          Slick.prototype.updateDots = function() {

            var _ = this;

            if (_.$dots !== null) {

              _.$dots
                .find('li')
                .removeClass('slick-active')
                .end();

              _.$dots
                .find('li')
                .eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
                .addClass('slick-active');

            }

          };

          Slick.prototype.visibility = function() {

            var _ = this;

            if (_.options.autoplay) {

              if (document[_.hidden]) {

                _.interrupted = true;

              } else {

                _.interrupted = false;

              }

            }

          };

          $.fn.slick = function() {
            var _ = this,
              opt = arguments[0],
              args = Array.prototype.slice.call(arguments, 1),
              l = _.length,
              i,
              ret;
            for (i = 0; i < l; i++) {
              if (typeof opt == 'object' || typeof opt == 'undefined')
                _[i].slick = new Slick(_[i], opt);
              else
                ret = _[i].slick[opt].apply(_[i].slick, args);
              if (typeof ret != 'undefined') return ret;
            }
            return _;
          };

        }));

      }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

    }, {}
  ],
  10: [
    function(require, module, exports) {
      (function(self) {
        'use strict';

        if (self.fetch) {
          return
        }

        var support = {
          searchParams: 'URLSearchParams' in self,
          iterable: 'Symbol' in self && 'iterator' in Symbol,
          blob: 'FileReader' in self && 'Blob' in self && (function() {
            try {
              new Blob()
              return true
            } catch (e) {
              return false
            }
          })(),
          formData: 'FormData' in self,
          arrayBuffer: 'ArrayBuffer' in self
        }

        if (support.arrayBuffer) {
          var viewClasses = [
            '[object Int8Array]',
            '[object Uint8Array]',
            '[object Uint8ClampedArray]',
            '[object Int16Array]',
            '[object Uint16Array]',
            '[object Int32Array]',
            '[object Uint32Array]',
            '[object Float32Array]',
            '[object Float64Array]'
          ]

          var isDataView = function(obj) {
            return obj && DataView.prototype.isPrototypeOf(obj)
          }

          var isArrayBufferView = ArrayBuffer.isView || function(obj) {
              return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
            }
        }

        function normalizeName(name) {
          if (typeof name !== 'string') {
            name = String(name)
          }
          if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
            throw new TypeError('Invalid character in header field name')
          }
          return name.toLowerCase()
        }

        function normalizeValue(value) {
          if (typeof value !== 'string') {
            value = String(value)
          }
          return value
        }

        // Build a destructive iterator for the value list
        function iteratorFor(items) {
          var iterator = {
            next: function() {
              var value = items.shift()
              return {
                done: value === undefined,
                value: value
              }
            }
          }

          if (support.iterable) {
            iterator[Symbol.iterator] = function() {
              return iterator
            }
          }

          return iterator
        }

        function Headers(headers) {
          this.map = {}

          if (headers instanceof Headers) {
            headers.forEach(function(value, name) {
              this.append(name, value)
            }, this)
          } else if (Array.isArray(headers)) {
            headers.forEach(function(header) {
              this.append(header[0], header[1])
            }, this)
          } else if (headers) {
            Object.getOwnPropertyNames(headers).forEach(function(name) {
              this.append(name, headers[name])
            }, this)
          }
        }

        Headers.prototype.append = function(name, value) {
          name = normalizeName(name)
          value = normalizeValue(value)
          var oldValue = this.map[name]
          this.map[name] = oldValue ? oldValue + ',' + value : value
        }

        Headers.prototype['delete'] = function(name) {
          delete this.map[normalizeName(name)]
        }

        Headers.prototype.get = function(name) {
          name = normalizeName(name)
          return this.has(name) ? this.map[name] : null
        }

        Headers.prototype.has = function(name) {
          return this.map.hasOwnProperty(normalizeName(name))
        }

        Headers.prototype.set = function(name, value) {
          this.map[normalizeName(name)] = normalizeValue(value)
        }

        Headers.prototype.forEach = function(callback, thisArg) {
          for (var name in this.map) {
            if (this.map.hasOwnProperty(name)) {
              callback.call(thisArg, this.map[name], name, this)
            }
          }
        }

        Headers.prototype.keys = function() {
          var items = []
          this.forEach(function(value, name) {
            items.push(name)
          })
          return iteratorFor(items)
        }

        Headers.prototype.values = function() {
          var items = []
          this.forEach(function(value) {
            items.push(value)
          })
          return iteratorFor(items)
        }

        Headers.prototype.entries = function() {
          var items = []
          this.forEach(function(value, name) {
            items.push([name, value])
          })
          return iteratorFor(items)
        }

        if (support.iterable) {
          Headers.prototype[Symbol.iterator] = Headers.prototype.entries
        }

        function consumed(body) {
          if (body.bodyUsed) {
            return Promise.reject(new TypeError('Already read'))
          }
          body.bodyUsed = true
        }

        function fileReaderReady(reader) {
          return new Promise(function(resolve, reject) {
            reader.onload = function() {
              resolve(reader.result)
            }
            reader.onerror = function() {
              reject(reader.error)
            }
          })
        }

        function readBlobAsArrayBuffer(blob) {
          var reader = new FileReader()
          var promise = fileReaderReady(reader)
          reader.readAsArrayBuffer(blob)
          return promise
        }

        function readBlobAsText(blob) {
          var reader = new FileReader()
          var promise = fileReaderReady(reader)
          reader.readAsText(blob)
          return promise
        }

        function readArrayBufferAsText(buf) {
          var view = new Uint8Array(buf)
          var chars = new Array(view.length)

          for (var i = 0; i < view.length; i++) {
            chars[i] = String.fromCharCode(view[i])
          }
          return chars.join('')
        }

        function bufferClone(buf) {
          if (buf.slice) {
            return buf.slice(0)
          } else {
            var view = new Uint8Array(buf.byteLength)
            view.set(new Uint8Array(buf))
            return view.buffer
          }
        }

        function Body() {
          this.bodyUsed = false

          this._initBody = function(body) {
            this._bodyInit = body
            if (!body) {
              this._bodyText = ''
            } else if (typeof body === 'string') {
              this._bodyText = body
            } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
              this._bodyBlob = body
            } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
              this._bodyFormData = body
            } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
              this._bodyText = body.toString()
            } else if (support.arrayBuffer && support.blob && isDataView(body)) {
              this._bodyArrayBuffer = bufferClone(body.buffer)
              // IE 10-11 can't handle a DataView body.
              this._bodyInit = new Blob([this._bodyArrayBuffer])
            } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
              this._bodyArrayBuffer = bufferClone(body)
            } else {
              throw new Error('unsupported BodyInit type')
            }

            if (!this.headers.get('content-type')) {
              if (typeof body === 'string') {
                this.headers.set('content-type', 'text/plain;charset=UTF-8')
              } else if (this._bodyBlob && this._bodyBlob.type) {
                this.headers.set('content-type', this._bodyBlob.type)
              } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
                this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8')
              }
            }
          }

          if (support.blob) {
            this.blob = function() {
              var rejected = consumed(this)
              if (rejected) {
                return rejected
              }

              if (this._bodyBlob) {
                return Promise.resolve(this._bodyBlob)
              } else if (this._bodyArrayBuffer) {
                return Promise.resolve(new Blob([this._bodyArrayBuffer]))
              } else if (this._bodyFormData) {
                throw new Error('could not read FormData body as blob')
              } else {
                return Promise.resolve(new Blob([this._bodyText]))
              }
            }

            this.arrayBuffer = function() {
              if (this._bodyArrayBuffer) {
                return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
              } else {
                return this.blob().then(readBlobAsArrayBuffer)
              }
            }
          }

          this.text = function() {
            var rejected = consumed(this)
            if (rejected) {
              return rejected
            }

            if (this._bodyBlob) {
              return readBlobAsText(this._bodyBlob)
            } else if (this._bodyArrayBuffer) {
              return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
            } else if (this._bodyFormData) {
              throw new Error('could not read FormData body as text')
            } else {
              return Promise.resolve(this._bodyText)
            }
          }

          if (support.formData) {
            this.formData = function() {
              return this.text().then(decode)
            }
          }

          this.json = function() {
            return this.text().then(JSON.parse)
          }

          return this
        }

        // HTTP methods whose capitalization should be normalized
        var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

          function normalizeMethod(method) {
            var upcased = method.toUpperCase()
            return (methods.indexOf(upcased) > -1) ? upcased : method
          }

          function Request(input, options) {
            options = options || {}
            var body = options.body

            if (input instanceof Request) {
              if (input.bodyUsed) {
                throw new TypeError('Already read')
              }
              this.url = input.url
              this.credentials = input.credentials
              if (!options.headers) {
                this.headers = new Headers(input.headers)
              }
              this.method = input.method
              this.mode = input.mode
              if (!body && input._bodyInit != null) {
                body = input._bodyInit
                input.bodyUsed = true
              }
            } else {
              this.url = String(input)
            }

            this.credentials = options.credentials || this.credentials || 'omit'
            if (options.headers || !this.headers) {
              this.headers = new Headers(options.headers)
            }
            this.method = normalizeMethod(options.method || this.method || 'GET')
            this.mode = options.mode || this.mode || null
            this.referrer = null

            if ((this.method === 'GET' || this.method === 'HEAD') && body) {
              throw new TypeError('Body not allowed for GET or HEAD requests')
            }
            this._initBody(body)
          }

        Request.prototype.clone = function() {
          return new Request(this, {
            body: this._bodyInit
          })
        }

        function decode(body) {
          var form = new FormData()
          body.trim().split('&').forEach(function(bytes) {
            if (bytes) {
              var split = bytes.split('=')
              var name = split.shift().replace(/\+/g, ' ')
              var value = split.join('=').replace(/\+/g, ' ')
              form.append(decodeURIComponent(name), decodeURIComponent(value))
            }
          })
          return form
        }

        function parseHeaders(rawHeaders) {
          var headers = new Headers()
          rawHeaders.split(/\r?\n/).forEach(function(line) {
            var parts = line.split(':')
            var key = parts.shift().trim()
            if (key) {
              var value = parts.join(':').trim()
              headers.append(key, value)
            }
          })
          return headers
        }

        Body.call(Request.prototype)

        function Response(bodyInit, options) {
          if (!options) {
            options = {}
          }

          this.type = 'default'
          this.status = 'status' in options ? options.status : 200
          this.ok = this.status >= 200 && this.status < 300
          this.statusText = 'statusText' in options ? options.statusText : 'OK'
          this.headers = new Headers(options.headers)
          this.url = options.url || ''
          this._initBody(bodyInit)
        }

        Body.call(Response.prototype)

        Response.prototype.clone = function() {
          return new Response(this._bodyInit, {
            status: this.status,
            statusText: this.statusText,
            headers: new Headers(this.headers),
            url: this.url
          })
        }

        Response.error = function() {
          var response = new Response(null, {
            status: 0,
            statusText: ''
          })
          response.type = 'error'
          return response
        }

        var redirectStatuses = [301, 302, 303, 307, 308]

        Response.redirect = function(url, status) {
          if (redirectStatuses.indexOf(status) === -1) {
            throw new RangeError('Invalid status code')
          }

          return new Response(null, {
            status: status,
            headers: {
              location: url
            }
          })
        }

        self.Headers = Headers
        self.Request = Request
        self.Response = Response

        self.fetch = function(input, init) {
          return new Promise(function(resolve, reject) {
            var request = new Request(input, init)
            var xhr = new XMLHttpRequest()

            xhr.onload = function() {
              var options = {
                status: xhr.status,
                statusText: xhr.statusText,
                headers: parseHeaders(xhr.getAllResponseHeaders() || '')
              }
              options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL')
              var body = 'response' in xhr ? xhr.response : xhr.responseText
              resolve(new Response(body, options))
            }

            xhr.onerror = function() {
              reject(new TypeError('Network request failed'))
            }

            xhr.ontimeout = function() {
              reject(new TypeError('Network request failed'))
            }

            xhr.open(request.method, request.url, true)

            if (request.credentials === 'include') {
              xhr.withCredentials = true
            }

            if ('responseType' in xhr && support.blob) {
              xhr.responseType = 'blob'
            }

            request.headers.forEach(function(value, name) {
              xhr.setRequestHeader(name, value)
            })

            xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
          })
        }
        self.fetch.polyfill = true
      })(typeof self !== 'undefined' ? self : this);

    }, {}
  ],
  11: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports.
      default = function() {
        $('.accordion-trigger').on('click', function(e) {

          e.preventDefault();
          $(this).toggleClass('-active').parents('li').toggleClass('-active').find($('.accordion-content')).slideToggle(250);
        });
      };

    }, {}
  ],
  12: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _isWeakmapPolyfill = require('is-weakmap-polyfill');

      var _isWeakmapPolyfill2 = _interopRequireDefault(_isWeakmapPolyfill);

      var _hyperform = require('hyperform');

      var _hyperform2 = _interopRequireDefault(_hyperform);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      // -----------------------------------------------------------------------------
      // Hyperform
      //
      // Hyperform is a pure JS implementation of the HTML 5 form validation API.
      //
      // Docs: https://hyperform.js.org/docs
      // -----------------------------------------------------------------------------
      exports.
      default = function() {

        (0, _isWeakmapPolyfill2.
          default)();

        if ($('form').length) {
          (0, _hyperform2.
            default)(window, {
            revalidate: 'onsubmit',
            classes: {
              warning: 'error-message',
              validated: '-validated',
              valid: '-valid',
              invalid: '-invalid'
            }
          });

          // Override email validation message for invalid email address
          $('input[type="email"]').map(function(index, el) {
            // setMessage takes three parameters (element, validator, message)
            _hyperform2.
            default.setMessage(el, 'typeMismatch', 'Please enter a valid email address.');
          });

          // Move error messages for checks and radios after the label instead of after the input
          $('form').on('validate', function(e) {
            setTimeout(function() {
              $(e.currentTarget).find('.radio .error-message, .check .error-message').map(function(index, el) {
                $(el).parent().parent().addClass('-invalid');
                $(el).insertAfter($(el).next());
              });
            }, 50);
          });
        }
      };
      // Import hyperform from node_modules

    }, {
      "hyperform": 1,
      "is-weakmap-polyfill": 7
    }
  ],
  13: [
    function(require, module, exports) {
      (function(global) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
          value: true
        });

        var _jquery = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

        var _jquery2 = _interopRequireDefault(_jquery);

        var _slickCarousel = require('slick-carousel');

        var _slickCarousel2 = _interopRequireDefault(_slickCarousel);

        function _interopRequireDefault(obj) {
          return obj && obj.__esModule ? obj : {
            default: obj
          };
        }

        exports.
        default = function() {
          (0, _jquery2.
            default)('[data-slick]').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            arrows: false,
            infinite: true,
            speed: 1000,
            fade: true,
            cssEase: 'ease-in-out',
            pauseOnHover: false,
            pauseOnDotsHover: true
          });
        };

      }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

    }, {
      "slick-carousel": 9
    }
  ],
  14: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) {
          return typeof obj;
        } : function(obj) {
          return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
        };

      exports.
      default = function() {

        /* eslint-disable */

        $.fn.pushMenu = function() {

          // idk about this
          var _this = this;

          // Define option defaults
          var defaults = {
            level: 0,
            menu: '',
            position: 0,
            maxDepth: -1,
            ul: 'ul',
            li: 'li',
            hideMenu: true,
            data: 'data-menu',
            mobileNav: '',
            target: this[0],
            nextHTML: '',
            prevHTML: '',
            navButton: '',
            pageContainer: '',
            scrollBlocker: '',
            resetOnClose: true,
            contents: function contents(selectedContent) {
              return selectedContent;
            }
          };

          // Create options by extending defaults with the passed in arugments
          if (arguments[0] && _typeof(arguments[0]) === "object") {
            this.options = this.extend(true, defaults, arguments[0]);
          }

          this.reg = function(opts) {
            $(this.options.target).data('opts', opts);
          };

          this.getOpts = function(opt) {
            return $(this.options.target).data('opts');
          };

          this.getContent = function() {
            return $(_this.options.menu).clone()[0];
          };

          /**
           * Function for setting up the menus
           */
          this.setup = function(index, item) {
            if (index === 0) $(item).attr(_this.options.data, 0);
            var cloned = item.cloneNode(true);

            return {
              unique: index,
              menu: _this.removeChildren(cloned)
            };
          };

          /**
           * Helper function to get menu levels
           */
          this.menuContainers = function(index) {
            return '<' + _this.options.ul + ' data-position="' + index + '" />';
          };

          /**
           * .. it works?
           */
          this.removeChildren = function(cloned) {
            $(cloned).find(this.options.li + ' > ' + this.options.ul).children().remove();
            return cloned;
          };

          /**
           * Change menu levels and resetsdfas
           */
          this.change = function(event, level) {

            // here are your options .. do or do not do .. there is no try
            var opts = _this.getOpts();

            // now some functions
            var appendContentArray = function appendContentArray(target, content) {
              for (var x = 0; x < content.length; x++) {
                if ($(content[x]).children(_this.options.ul).length >= 1) {
                  $(content[x]).append(_this.options.nextHTML).find(_this.options.ul).remove();
                }
              }
              target.append(content);
            };
            var updateDrawer = function updateDrawer(options) {
              $(_this.options.mobileNav).attr('data-current', _this.options.position);
            };

            // more variable .. haven't done anything yet
            var contents = opts.content,
              total = opts.position,
              items = $(contents[level].menu.children).clone(),
              length = items.length + 1,
              nextHTMLName = (typeof event === 'undefined' ? 'undefined' : _typeof(event)) === 'object' ? event.target.dataset.direction : event;

            // let the games BEGIN!
            switch (nextHTMLName) {
              // reset menu to level 0
              case 'reset':
                for (; total > 0; total--) {
                  $(opts.target).find(opts.ul + '[data-position=' + opts.position + ']').empty();
                  opts.position--;
                  updateDrawer(opts);
                }

                break;
                // close submenus
              case 'prev':
                // Update Target.
                $(opts.target).children(this.options.ul + '[data-position=' + this.options.position + ']').empty();

                opts.position--;
                updateDrawer(opts);
                break;

                // everything else .. mostly open sub menus
              default:
                if (event && event.target.dataset.direction === 'next') {
                  opts.position++;
                }

                var currentUL = $(opts.target).find(opts.ul + '[data-position="' + opts.position + '"]');

                if (event) {
                  currentUL.prepend(opts.prevHTML);
                }

                // content is in the form of li's in items array
                appendContentArray(currentUL, items);

                // Update Target.
                updateDrawer(opts);
                if (_typeof(contents[level].menu.dataset) !== 'object') {
                  contents[level].menu.dataset = {};
                }
                contents[level].menu.dataset.position = opts.position;
            }

            this.reg(opts);
          };

          /**
           * Updates data attribute on drawer container
           */
          this.addDataAttr = function(content) {
            var counter = 1;
            return content.map(function(index, item) {
              $(item).find('> ' + _this.options.ul).map(function(position, ul) {
                $(item).attr(_this.options.data, counter);
                counter++;
              });
            });
          };

          /**
           * Gets the max depth for stuff
           */
          this.maxDepth = function(content) {
            var options = this.options,
              maxDepth = options.maxDepth || -1,
              realDepth = 0;

            var ulS = $(content).find('ul');

            for (var x = 0; x < ulS.length || realDepth == maxDepth; x++) {
              if ($(ulS[x]).parents(options.ul).length > realDepth) {
                realDepth = $(ulS[x]).parents(options.ul).length;
              }
            }

            this.options.realDepth = realDepth;

            // pre place containers for the menu
            for (var y = 0; y <= realDepth; y++) {
              $(options.target).append(_this.menuContainers(y));
            }
          };

          /**
           * Binds open/close functionality to scrollBlocker and navbutton
           */
          this.bindopenClose = function() {

            var options = this.options,
              opts = {
                options: options
              },
              navButton = $(options.navButton),
              scrollBlocker = $(options.scrollBlocker);

            // Toggle nav show/hide on click
            navButton.click(options, this.openClose);

            // Close nav on page click
            scrollBlocker.click(options, this.openClose);
          };

          /**
           * If the drawer is open, this function will close it
           * If the drawer is closed, this function will open it
           */
          this.openClose = function(options) {
            var opts = _this.getOpts();

            var pageContainer = $(opts.pageContainer);

            // currently open .. close it
            if (_this.isDrawerOpen(pageContainer)) {
              _this.closeDrawer(opts);

              // reset the drawer to level 0 on close
              if (opts.resetOnClose) {
                _this.change('reset', opts.position);
              }
            }
            // currently closed .. open it
            else {
              _this.openDrawer(opts);
            }
          };

          /**
           * Helper function to determine if drawer is open or closed
           */
          this.isDrawerOpen = function(pageContainer) {
            if ($('html').hasClass('-js-menu-open')) {
              return true;
            }
            return false;
          };

          /**
           * Function for opening the drawer
           */
          this.openDrawer = function(options) {
            $('html').addClass('-js-menu-open');
            $(this.options.navButton).addClass('-js-menu-open');
          };

          /**
           * Function for closing the drawer
           */
          this.closeDrawer = function(options) {
            $('html').removeClass('-js-menu-open');
            $(this.options.navButton).removeClass('-js-menu-open');
          };

          /**
           * START HERE
           */
          this.init = function() {

            var origContent = this.getContent();

            // now manip the content
            var manipContent = this.options.contents(origContent);

            // add attribute data
            this.addDataAttr($(manipContent).find(this.options.li));

            // map the content
            var content = $(manipContent).find(this.options.ul).map(this.setup);

            // maxDepth set up work
            this.maxDepth(manipContent);

            // store the content for later
            this.options.content = content;
            this.options.manipContent = manipContent;

            // register all options to the target container
            this.reg(this.options);

            this.change(false, 0);
            this.bindopenClose();

            if (this.options.hideMenu) {
              $(this.menu).hide();
            }

            $(this.options.target).on('click', '[data-direction="prev"], [data-direction="next"]', function(event) {
              var opts = _this.getOpts();
              var level = 0;
              switch (event.target.dataset.direction) {
                case 'next':
                  level = $(this).parent().attr(opts.data);
                  break;

                case 'prev':
                  var possibleLevel = $(opts.menu).find('[' + opts.data + '="' + opts.level + '"]').parent().closest('[' + opts.data + ']');
                  if (possibleLevel.length > 0) {
                    level = possibleLevel.data(opts.data.replace('data-', ''));
                  } else {
                    level = 0;
                  }

                  break;
              }

              _this.change(event, level);
            });
          };

          /**
           * Initialze the setup
           */
          this.init();
        };

        /* eslint-enable */

        $('.js-mobile-nav-inner').pushMenu({
          menu: '.js-validatek-header-desktop-menu',
          mobileNav: '.js-mobile-nav',
          navButton: '.js-validatek-header-mobile-menu-trigger',
          pageContainer: '.page-container',
          scrollBlocker: '.blocker',
          nextHTML: '<a href="#" class="sub-open" data-direction="next">Next</a>',
          prevHTML: '<li class="nav-back"><a class="sub-close" data-direction="prev" href="#">Back</a></li>'
        });

        $('ul[data-position="0"]').prepend('<li class="mobile-search"></li>');
        $('.js-search-box').clone().appendTo('.mobile-search');
        $('.validatek-header-action-button').clone().appendTo('ul[data-position="0"]');

        var throttled = false;

        $(window).on('resize', function() {
          if (!throttled) {
            throttled = true;
            setTimeout(function() {
              throttled = false;
              $('html, .js-mobile-nav-button').removeClass('-js-menu-open');
            }, 200);
          }
        });
      };

    }, {}
  ],
  15: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _throttle = require('../plugins/throttle');

      var _throttle2 = _interopRequireDefault(_throttle);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      exports.
      default = function() {

        if ($('.scroll-animate').length) {

          $(window).on('scroll', animateOnScroll);

          $(window).on('resize', (0, _throttle2.
            default)(animateOnScroll, 100));

          animateOnScroll();
        }
      };

      function animateOnScroll() {
        var scrolled = $(window).scrollTop(),
          winHeightPadded = $(window).height() * 1.1;

        // Shown...
        $('.scroll-animate:not(.animated)').map(function(index, el) {
          if (scrolled + winHeightPadded > $(el).offset().top) {
            if ($(el).data('timeout')) {
              window.setTimeout(function() {
                $(el).addClass('animated');
              }, parseInt($(el).data('timeout'), 30));
            } else {
              $(el).addClass('animated');
            }
          }
        });

        // Hidden...
        $('.scroll-animate.animated').map(function(index, el) {
          if (scrolled + winHeightPadded < $(el).offset().top) {
            $(el).removeClass('animated');
          }
        });
      }

    }, {
      "../plugins/throttle": 19
    }
  ],
  16: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function() {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }
        return function(Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      var _isSearch = require('is-search');

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      function _possibleConstructorReturn(self, call) {
        if (!self) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }
        return call && (typeof call === "object" || typeof call === "function") ? call : self;
      }

      function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
          throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }
        subClass.prototype = Object.create(superClass && superClass.prototype, {
          constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
          }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
      } /* eslint-disable no-multi-spaces */
      /* eslint-disable object-curly-spacing */
      /* eslint-disable object-property-newline */
      /* eslint-disable no-unused-vars */

      /* eslint-disable */
      // TODO: Fix linting errors

      var SiteSearch = function(_Facets) {
        _inherits(SiteSearch, _Facets);

        /**
         * Initialize the Search.
         */
        function SiteSearch() {
          _classCallCheck(this, SiteSearch);

          // All available default query parameters.
          var config = new _isSearch.Config();
          var _window$location = window.location,
            origin = _window$location.origin,
            pathname = _window$location.pathname;

          var endpoint = 'api/search/results';

          config.endpoint = origin + '/' + endpoint;
          config.searchURL = pathname;
          config.defaultQueryParams = {
            _limit: 10,
            _page: 1,
            keywords: ''
          };
          config.pagerParam = '_page';
          config.keywordParam = 'keywords';

          config.fetchOptions = {
            method: 'GET',
            origin: 'http://localhost:8080'
          };

          var _this = _possibleConstructorReturn(this, (SiteSearch.__proto__ || Object.getPrototypeOf(SiteSearch)).call(this, config));

          _this.filterArea = '.search-results-form';
          _this.contentArea = '.search-landing-content';
          _this.loading = '.js-search-content-loading';
          _this.noResults = '.js-search-landing-content-no-results';
          _this.paginationClass = '.pagination';

          // Event callback for Fetch requests which build the reults and pager.
          $(_this).on('isLoading', _this._mixinIsLoading.bind(_this)).on('HandleResponse', _this._searchResponse.bind(_this));

          // Keyword search.
          $(_this.filterArea).find('input').on('keyup', _this.debounce(_this._mixinKeywordSearch.bind(_this), 500));

          var search = new _isSearch.Pagination(config);
          $(_this).on('HandleResponse', _this._buildPagination.bind(_this));

          $('a.prev, a.next').on('click', function(e) {
            e.preventDefault();
            if ($(e.target).hasClass('next')) {
              this.nextPage();
            } else {
              this.previousPage();
            }
          }.bind(_this));
          return _this;
        }

        /**
         * Callback that allows for a progress loaded to be initialized and deinitialized.
         *
         * @private
         * @method _mixinIsLoading
         * @param { Object } event The jQuery event passed from on().
         * @param { Boolean } state true|false
         */


        _createClass(SiteSearch, [{
          key: '_mixinIsLoading',
          value: function _mixinIsLoading(event, state) {
            switch (state) {
              case true:
                // Show progress html.
                $(this.loading).show();
                break;

              default:
                // Hide progress html.
                $(this.loading).hide();

                break;
            }

            $(this.noResults).hide();
          }

          /**
           * Callback for processing the keywords entered into the search.
           *
           * @private
           * @method _mixinKeywordSearch
           * @param { Object } event The jQuery event passed from on().
           */

        }, {
          key: '_mixinKeywordSearch',
          value: function _mixinKeywordSearch(event) {
            event.preventDefault();
            var parent = $(event.target).parent(),
              keywords = parent.find('input').val();
            this._helperUpdateKeywords(keywords);
          }

          /**
           * Callback for the primary Fetch responses.
           *
           * @private
           * @method _searchResponse
           * @param { Object } event The jQuery event passed from on().
           * @param { JSON } response The raw response object.
           */

        }, {
          key: '_searchResponse',
          value: function _searchResponse(event, response) {
            var results = response.results,
              firstResult = this.firstResult,
              lastResult = this.lastResult,
              totalResults = this.totalResults,
              container = $(this.contentArea);

            // Empty out the current search results and stats.
            container.empty();
            $(this.noResults).hide();

            // Stop if there are no results to show.
            if (!results.length) {
              $(this.noResults).show();
              return;
            }

            if (this.query.keywords) {
              var min = this.currentPage * this.limit - this.limit + 1;
              if (this.limit > this.totalResults) {
                var _max = this.totalResults;
              } else {
                var _max2 = this.currentPage * this.limit;
              }
              $('.search-results-stats').empty();
              var resultsText = 'Showing ' + min + '-' + max + ' of ' + this.totalResults + ' results for: <strong>“' + this.query.keywords + '”</strong>';
              $('.search-results-stats').append(resultsText);
            }

            // Build and output the each individual search result.
            results.map(function(_ref) {
              var id = _ref.id,
                title = _ref.title,
                url = _ref.url,
                text = _ref.text;

              text = typeof text === 'undefined' ? '' : '' + text;

              var template = '\n        <div class="search-result">\n          <h2>\n            <a href="' + url + '">' + title + '</a>\n          </h2>\n          ' + text + '\n        </div>\n      ';

              container.append(template);
            });
          }

          /**
           * Callback for build the pagination based off the returned results.
           *
           * @private
           * @method _buildPagination
           * @return undefined
           */

        }, {
          key: '_buildPagination',
          value: function _buildPagination() {
            $(this.paginationClass).empty();
            this.buildPager().map(this._buildPaginationItem.bind(this));
          }

          /**
           * Callback for building an individual pager item.
           *
           * @private
           * @method _buildPaginationItem
           * @param { Object } page
           * @return undefined
           */

        }, {
          key: '_buildPaginationItem',
          value: function _buildPaginationItem(page) {
            var current = page.current,
              link = page.link,
              text = page.text,
              value = page.value,
              currentHTML = '<li><a href="#" class="-active">' + text + '</a></li>';

            var defaultHTML = '<li><a href="' + link + '" data-value="' + value + '">' + text + '</a></li>';
            if (value === null) {
              defaultHTML = '<li class="dots">...</li>';
            }
            $(this.paginationClass).append(current ? currentHTML : defaultHTML);
          }
        }]);

        return SiteSearch;
      }(_isSearch.Facets);

      exports.
      default = SiteSearch;

    }, {
      "is-search": 2
    }
  ],
  17: [
    function(require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports.
      default = function() {

        // Opens search box
        $('html').on('click', '.js-search-open', function(e) {
          e.preventDefault();
          $('.js-search-box').addClass('-is-open');
          setTimeout(function() {
            $('.js-header-search').focus();
          }, 20);
        });

        // Closes search box
        $('.js-search-close').on('click', function(e) {
          e.preventDefault();
          $('.js-search-box').removeClass('-is-open');
        });

        // Closes search box on esc keypress
        $('html').on('keyup', function(e) {
          // escape key maps to keycode `27`
          if (e.keyCode === 27) {
            $('.js-search-box').removeClass('-is-open');
          }
        });
      };

    }, {}
  ],
  18: [
    function(require, module, exports) {
      'use strict';

      var _formValidation = require('./components/form-validation');

      var _formValidation2 = _interopRequireDefault(_formValidation);

      var _mobileNav = require('./components/mobile-nav');

      var _mobileNav2 = _interopRequireDefault(_mobileNav);

      var _siteHeader = require('./components/site-header');

      var _siteHeader2 = _interopRequireDefault(_siteHeader);

      var _heroCarousel = require('./components/hero-carousel');

      var _heroCarousel2 = _interopRequireDefault(_heroCarousel);

      var _scrollAnimation = require('./components/scroll-animation');

      var _scrollAnimation2 = _interopRequireDefault(_scrollAnimation);

      var _accordion = require('./components/accordion');

      var _accordion2 = _interopRequireDefault(_accordion);

      var _siteSearch = require('./components/search/site-search');

      var _siteSearch2 = _interopRequireDefault(_siteSearch);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          default: obj
        };
      }

      window.$ = jQuery;

      // import weakMapPolyfill from 'is-weakmap-polyfill';


      (function($, root, undefined) {

        // Document Ready
        $(function() {

          // weakMapPolyfill();
          (0, _formValidation2.
            default)();
          (0, _scrollAnimation2.
            default)();
          (0, _siteHeader2.
            default)();
          (0, _heroCarousel2.
            default)();
          (0, _mobileNav2.
            default)();
          (0, _accordion2.
            default)();

          if ($('.search-results-listing').length) {
            new _siteSearch2.
            default ().init();
          }
        });
      })(jQuery, undefined);

    }, {
      "./components/accordion": 11,
      "./components/form-validation": 12,
      "./components/hero-carousel": 13,
      "./components/mobile-nav": 14,
      "./components/scroll-animation": 15,
      "./components/search/site-search": 16,
      "./components/site-header": 17
    }
  ],
  19: [
    function(require, module, exports) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      // throttle function
      exports.
      default = function(fn, threshhold, scope) {
        threshhold || (threshhold = 250);
        var last = void 0,
          deferTimer = void 0;

        return function gulp() {
          var context = scope || this,
            now = +new Date(),
            args = arguments;

          if (last && now < last + threshhold) {
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function() {
              last = now;
              fn.apply(context, args);
            }, threshhold);
          } else {
            last = now;
            fn.apply(context, args);
          }
        };
      };

    }, {}
  ]
}, {}, [18])
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      cache: false,
      url: drupalSettings.statistics.url,
      data: drupalSettings.statistics.data
    });
  });
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  var states = {
    postponed: []
  };

  Drupal.states = states;

  function invert(a, invertState) {
    return invertState && typeof a !== 'undefined' ? !a : a;
  }

  function _compare2(a, b) {
    if (a === b) {
      return typeof a === 'undefined' ? a : true;
    }

    return typeof a === 'undefined' || typeof b === 'undefined';
  }

  function ternary(a, b) {
    if (typeof a === 'undefined') {
      return b;
    }
    if (typeof b === 'undefined') {
      return a;
    }

    return a && b;
  }

  Drupal.behaviors.states = {
    attach: function attach(context, settings) {
      var $states = $(context).find('[data-drupal-states]');
      var il = $states.length;

      var _loop = function _loop(i) {
        var config = JSON.parse($states[i].getAttribute('data-drupal-states'));
        Object.keys(config || {}).forEach(function (state) {
          new states.Dependent({
            element: $($states[i]),
            state: states.State.sanitize(state),
            constraints: config[state]
          });
        });
      };

      for (var i = 0; i < il; i++) {
        _loop(i);
      }

      while (states.postponed.length) {
        states.postponed.shift()();
      }
    }
  };

  states.Dependent = function (args) {
    var _this = this;

    $.extend(this, { values: {}, oldValue: null }, args);

    this.dependees = this.getDependees();
    Object.keys(this.dependees || {}).forEach(function (selector) {
      _this.initializeDependee(selector, _this.dependees[selector]);
    });
  };

  states.Dependent.comparisons = {
    RegExp: function RegExp(reference, value) {
      return reference.test(value);
    },
    Function: function Function(reference, value) {
      return reference(value);
    },
    Number: function Number(reference, value) {
      return typeof value === 'string' ? _compare2(reference.toString(), value) : _compare2(reference, value);
    }
  };

  states.Dependent.prototype = {
    initializeDependee: function initializeDependee(selector, dependeeStates) {
      var _this2 = this;

      this.values[selector] = {};

      Object.keys(dependeeStates).forEach(function (i) {
        var state = dependeeStates[i];

        if ($.inArray(state, dependeeStates) === -1) {
          return;
        }

        state = states.State.sanitize(state);

        _this2.values[selector][state.name] = null;

        $(selector).on('state:' + state, { selector: selector, state: state }, function (e) {
          _this2.update(e.data.selector, e.data.state, e.value);
        });

        new states.Trigger({ selector: selector, state: state });
      });
    },
    compare: function compare(reference, selector, state) {
      var value = this.values[selector][state.name];
      if (reference.constructor.name in states.Dependent.comparisons) {
        return states.Dependent.comparisons[reference.constructor.name](reference, value);
      }

      return _compare2(reference, value);
    },
    update: function update(selector, state, value) {
      if (value !== this.values[selector][state.name]) {
        this.values[selector][state.name] = value;
        this.reevaluate();
      }
    },
    reevaluate: function reevaluate() {
      var value = this.verifyConstraints(this.constraints);

      if (value !== this.oldValue) {
        this.oldValue = value;

        value = invert(value, this.state.invert);

        this.element.trigger({
          type: 'state:' + this.state,
          value: value,
          trigger: true
        });
      }
    },
    verifyConstraints: function verifyConstraints(constraints, selector) {
      var result = void 0;
      if ($.isArray(constraints)) {
        var hasXor = $.inArray('xor', constraints) === -1;
        var len = constraints.length;
        for (var i = 0; i < len; i++) {
          if (constraints[i] !== 'xor') {
            var constraint = this.checkConstraints(constraints[i], selector, i);

            if (constraint && (hasXor || result)) {
              return hasXor;
            }
            result = result || constraint;
          }
        }
      } else if ($.isPlainObject(constraints)) {
          for (var n in constraints) {
            if (constraints.hasOwnProperty(n)) {
              result = ternary(result, this.checkConstraints(constraints[n], selector, n));

              if (result === false) {
                return false;
              }
            }
          }
        }
      return result;
    },
    checkConstraints: function checkConstraints(value, selector, state) {
      if (typeof state !== 'string' || /[0-9]/.test(state[0])) {
        state = null;
      } else if (typeof selector === 'undefined') {
        selector = state;
        state = null;
      }

      if (state !== null) {
        state = states.State.sanitize(state);
        return invert(this.compare(value, selector, state), state.invert);
      }

      return this.verifyConstraints(value, selector);
    },
    getDependees: function getDependees() {
      var cache = {};

      var _compare = this.compare;
      this.compare = function (reference, selector, state) {
        (cache[selector] || (cache[selector] = [])).push(state.name);
      };

      this.verifyConstraints(this.constraints);

      this.compare = _compare;

      return cache;
    }
  };

  states.Trigger = function (args) {
    $.extend(this, args);

    if (this.state in states.Trigger.states) {
      this.element = $(this.selector);

      if (!this.element.data('trigger:' + this.state)) {
        this.initialize();
      }
    }
  };

  states.Trigger.prototype = {
    initialize: function initialize() {
      var _this3 = this;

      var trigger = states.Trigger.states[this.state];

      if (typeof trigger === 'function') {
        trigger.call(window, this.element);
      } else {
        Object.keys(trigger || {}).forEach(function (event) {
          _this3.defaultTrigger(event, trigger[event]);
        });
      }

      this.element.data('trigger:' + this.state, true);
    },
    defaultTrigger: function defaultTrigger(event, valueFn) {
      var oldValue = valueFn.call(this.element);

      this.element.on(event, $.proxy(function (e) {
        var value = valueFn.call(this.element, e);

        if (oldValue !== value) {
          this.element.trigger({
            type: 'state:' + this.state,
            value: value,
            oldValue: oldValue
          });
          oldValue = value;
        }
      }, this));

      states.postponed.push($.proxy(function () {
        this.element.trigger({
          type: 'state:' + this.state,
          value: oldValue,
          oldValue: null
        });
      }, this));
    }
  };

  states.Trigger.states = {
    empty: {
      keyup: function keyup() {
        return this.val() === '';
      }
    },

    checked: {
      change: function change() {
        var checked = false;
        this.each(function () {
          checked = $(this).prop('checked');

          return !checked;
        });
        return checked;
      }
    },

    value: {
      keyup: function keyup() {
        if (this.length > 1) {
          return this.filter(':checked').val() || false;
        }
        return this.val();
      },
      change: function change() {
        if (this.length > 1) {
          return this.filter(':checked').val() || false;
        }
        return this.val();
      }
    },

    collapsed: {
      collapsed: function collapsed(e) {
        return typeof e !== 'undefined' && 'value' in e ? e.value : !this.is('[open]');
      }
    }
  };

  states.State = function (state) {
    this.pristine = state;
    this.name = state;

    var process = true;
    do {
      while (this.name.charAt(0) === '!') {
        this.name = this.name.substring(1);
        this.invert = !this.invert;
      }

      if (this.name in states.State.aliases) {
        this.name = states.State.aliases[this.name];
      } else {
        process = false;
      }
    } while (process);
  };

  states.State.sanitize = function (state) {
    if (state instanceof states.State) {
      return state;
    }

    return new states.State(state);
  };

  states.State.aliases = {
    enabled: '!disabled',
    invisible: '!visible',
    invalid: '!valid',
    untouched: '!touched',
    optional: '!required',
    filled: '!empty',
    unchecked: '!checked',
    irrelevant: '!relevant',
    expanded: '!collapsed',
    open: '!collapsed',
    closed: 'collapsed',
    readwrite: '!readonly'
  };

  states.State.prototype = {
    invert: false,

    toString: function toString() {
      return this.name;
    }
  };

  var $document = $(document);
  $document.on('state:disabled', function (e) {
    if (e.trigger) {
      $(e.target).prop('disabled', e.value).closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-disabled', e.value).find('select, input, textarea').prop('disabled', e.value);
    }
  });

  $document.on('state:required', function (e) {
    if (e.trigger) {
      if (e.value) {
        var label = 'label' + (e.target.id ? '[for=' + e.target.id + ']' : '');
        var $label = $(e.target).attr({ required: 'required', 'aria-required': 'true' }).closest('.js-form-item, .js-form-wrapper').find(label);

        if (!$label.hasClass('js-form-required').length) {
          $label.addClass('js-form-required form-required');
        }
      } else {
        $(e.target).removeAttr('required aria-required').closest('.js-form-item, .js-form-wrapper').find('label.js-form-required').removeClass('js-form-required form-required');
      }
    }
  });

  $document.on('state:visible', function (e) {
    if (e.trigger) {
      $(e.target).closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggle(e.value);
    }
  });

  $document.on('state:checked', function (e) {
    if (e.trigger) {
      $(e.target).prop('checked', e.value);
    }
  });

  $document.on('state:collapsed', function (e) {
    if (e.trigger) {
      if ($(e.target).is('[open]') === e.value) {
        $(e.target).find('> summary').trigger('click');
      }
    }
  });
})(jQuery, Drupal);;
/**
 * @file
 * JavaScript behaviors for custom webform #states.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.states = Drupal.webform.states || {};
  Drupal.webform.states.slideDown = Drupal.webform.states.slideDown || {};
  Drupal.webform.states.slideDown.duration = 'slow';
  Drupal.webform.states.slideUp = Drupal.webform.states.slideUp || {};
  Drupal.webform.states.slideUp.duration = 'fast';

  /**
   * Check if an element has a specified data attribute.
   *
   * @param {string} data
   *   The data attribute name.
   *
   * @returns {boolean}
   *   TRUE if an element has a specified data attribute.
   */
  $.fn.hasData = function (data) {
    return (typeof this.data(data) !== 'undefined');
  };

  /**
   * Check if element is within the webform or not.
   *
   * @returns {boolean}
   *   TRUE if element is within the webform.
   */
  $.fn.isWebform = function () {
    return $(this).closest('form[id^="webform"]').length ? true : false;
  };

  // The change event is triggered by cut-n-paste and select menus.
  // Issue #2445271: #states element empty check not triggered on mouse
  // based paste.
  // @see https://www.drupal.org/node/2445271
  Drupal.states.Trigger.states.empty.change = function change() {
    return this.val() === '';
  };

  // Apply solution included in #1962800 patch.
  // Issue #1962800: Form #states not working with literal integers as
  // values in IE11.
  // @see https://www.drupal.org/project/drupal/issues/1962800
  // @see https://www.drupal.org/files/issues/core-states-not-working-with-integers-ie11_1962800_46.patch
  //
  // This issue causes pattern, less than, and greater than support to break.
  // @see https://www.drupal.org/project/webform/issues/2981724
  var states = Drupal.states;
  Drupal.states.Dependent.prototype.compare = function compare(reference, selector, state) {
    var value = this.values[selector][state.name];

    var name = reference.constructor.name;
    if (!name) {
      name = $.type(reference);

      name = name.charAt(0).toUpperCase() + name.slice(1);
    }
    if (name in states.Dependent.comparisons) {
      return states.Dependent.comparisons[name](reference, value);
    }

    if (reference.constructor.name in states.Dependent.comparisons) {
      return states.Dependent.comparisons[reference.constructor.name](reference, value);
    }

    return _compare2(reference, value);
  };
  function _compare2(a, b) {
    if (a === b) {
      return typeof a === 'undefined' ? a : true;
    }

    return typeof a === 'undefined' || typeof b === 'undefined';
  }

  // Adds pattern, less than, and greater than support to #state API.
  // @see http://drupalsun.com/julia-evans/2012/03/09/extending-form-api-states-regular-expressions
  Drupal.states.Dependent.comparisons.Object = function (reference, value) {
    if ('pattern' in reference) {
      return (new RegExp(reference['pattern'])).test(value);
    }
    else if ('!pattern' in reference) {
      return !((new RegExp(reference['!pattern'])).test(value));
    }
    else if ('less' in reference) {
      return (value !== '' && parseFloat(reference['less']) > parseFloat(value));
    }
    else if ('greater' in reference) {
      return (value !== '' && parseFloat(reference['greater']) < parseFloat(value));
    }
    else {
      return reference.indexOf(value) !== false;
    }
  };

  var $document = $(document);

  $document.on('state:required', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      var $target = $(e.target);
      // Fix #required file upload.
      // @see Issue #2860529: Conditional required File upload field don't work.
      if (e.value) {
        $target.find('input[type="file"]').attr({'required': 'required', 'aria-required': 'true'});
      }
      else {
        $target.find('input[type="file"]').removeAttr('required aria-required');
      }

      // Fix required label for checkboxes and radios.
      // @see Issue #2938414: Checkboxes don't support #states required
      // @see Issue #2731991: Setting required on radios marks all options required.
      // @see Issue #2856315: Conditional Logic - Requiring Radios in a Fieldset.
      // Fix #required for fieldsets.
      // @see Issue #2977569: Hidden fieldsets that become visible with conditional logic cannot be made required.
      if ($target.is('.js-webform-type-radios, .js-webform-type-checkboxes, fieldset')) {
        if (e.value) {
          $target.find('legend span:not(.visually-hidden)').addClass('js-form-required form-required');
        }
        else {
          $target.find('legend span:not(.visually-hidden)').removeClass('js-form-required form-required');
        }
      }

      // Fix #required for radios.
      // @see Issue #2856795: If radio buttons are required but not filled form is nevertheless submitted.
      if ($target.is('.js-webform-type-radios, .js-form-type-webform-radios-other')) {
        if (e.value) {
          $target.find('input[type="radio"]').attr({'required': 'required', 'aria-required': 'true'});
        }
        else {
          $target.find('input[type="radio"]').removeAttr('required aria-required');
        }
      }

      // Fix #required for checkboxes.
      // @see Issue #2938414: Checkboxes don't support #states required.
      // @see checkboxRequiredhandler
      if ($target.is('.js-webform-type-checkboxes, .js-form-type-webform-checkboxes-other')) {
        var $checkboxes = $target.find('input[type="checkbox"]');
        if (e.value) {
          // Bind the event handler and add custom HTML5 required validation
          // to all checkboxes.
          $checkboxes.bind('click', checkboxRequiredhandler);
          if (!$checkboxes.is(':checked')) {
            $checkboxes.attr({'required': 'required', 'aria-required': 'true'});
          }
        }
        else {
          // Remove custom HTML5 required validation from all checkboxes
          // and unbind the event handler.
          $checkboxes
            .removeAttr('required aria-required')
            .unbind('click', checkboxRequiredhandler);
        }
      }

      // Issue #2986017: Fieldsets shouldn't have required attribute.
      if ($target.is('fieldset')) {
        $target.removeAttr('required aria-required');
      }
    }

  });

  $document.on('state:readonly', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      $(e.target).prop('readonly', e.value).closest('.js-form-item, .js-form-wrapper').toggleClass('webform-readonly', e.value).find('input, textarea').prop('readonly', e.value);
    }
  });

  $document.on('state:visible state:visible-slide', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      if (e.value) {
        $(':input', e.target).addBack().each(function () {
          restoreValueAndRequired(this);
          triggerEventHandlers(this);
        });
      }
      else {
        // @see https://www.sitepoint.com/jquery-function-clear-form-data/
        $(':input', e.target).addBack().each(function () {
          backupValueAndRequired(this);
          clearValueAndRequired(this);
          triggerEventHandlers(this);
        });
      }
    }
  });

  $document.bind('state:visible-slide', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      var effect = e.value ? 'slideDown' : 'slideUp';
      var duration = Drupal.webform.states[effect].duration;
      $(e.target).closest('.js-form-item, .js-form-submit, .js-form-wrapper')[effect](duration);
    }
  });
  Drupal.states.State.aliases['invisible-slide'] = '!visible-slide';

  $document.on('state:disabled', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      // Make sure disabled property is set before triggering webform:disabled.
      // Copied from: core/misc/states.js
      $(e.target)
        .prop('disabled', e.value)
        .closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-disabled', e.value)
        .find('select, input, textarea').prop('disabled', e.value);

      // Trigger webform:disabled.
      $(e.target).trigger('webform:disabled')
        .find('select, input, textarea').trigger('webform:disabled');
    }
  });

  /**
   * Trigger custom HTML5 multiple checkboxes validation.
   *
   * @see https://stackoverflow.com/a/37825072/145846
   */
  function checkboxRequiredhandler() {
    var $checkboxes = $(this).closest('.js-webform-type-checkboxes, .js-form-type-webform-checkboxes-other').find('input[type="checkbox"]');
    if ($checkboxes.is(':checked')) {
      $checkboxes.removeAttr('required aria-required');
    }
    else {
      $checkboxes.attr({'required': 'required', 'aria-required': 'true'});
    }
  }

  /**
   * Trigger an input's event handlers.
   *
   * @param input
   *   An input.
   */
  function triggerEventHandlers(input) {
    var $input = $(input);
    var type = input.type;
    var tag = input.tagName.toLowerCase();
    // Add 'webform.states' as extra parameter to event handlers.
    // @see Drupal.behaviors.webformUnsaved
    var extraParameters = ['webform.states'];
    if (type === 'checkbox' || type === 'radio') {
      $input
        .trigger('change', extraParameters)
        .trigger('blur', extraParameters);
    }
    else if (tag === 'select') {
      $input
        .trigger('change', extraParameters)
        .trigger('blur', extraParameters);
    }
    else if (type !== 'submit' && type !== 'button' && type !== 'file') {
      $input
        .trigger('input', extraParameters)
        .trigger('change', extraParameters)
        .trigger('keydown', extraParameters)
        .trigger('keyup', extraParameters)
        .trigger('blur', extraParameters);
    }
  }

  /**
   * Backup an input's current value and required attribute
   *
   * @param input
   *   An input.
   */
  function backupValueAndRequired(input) {
    var $input = $(input);
    var type = input.type;
    var tag = input.tagName.toLowerCase(); // Normalize case.

    // Backup required.
    if ($input.prop('required') && !$input.hasData('webform-required')) {
      $input.data('webform-required', true);
    }

    // Backup value.
    if (!$input.hasData('webform-value')) {
      if (type === 'checkbox' || type === 'radio') {
        $input.data('webform-value', $input.prop('checked'));
      }
      else if (tag === 'select') {
        var values = [];
        $input.find('option:selected').each(function (i, option) {
          values[i] = option.value;
        });
        $input.data('webform-value', values);
      }
      else if (type !== 'submit' && type !== 'button') {
        $input.data('webform-value', input.value);
      }
    }
  }

  /**
   * Restore an input's value and required attribute.
   *
   * @param input
   *   An input.
   */
  function restoreValueAndRequired(input) {
    var $input = $(input);

    // Restore value.
    var value = $input.data('webform-value');
    if (typeof value !== 'undefined') {
      var type = input.type;
      var tag = input.tagName.toLowerCase(); // Normalize case.

      if (type === 'checkbox' || type === 'radio') {
        $input.prop('checked', value);
      }
      else if (tag === 'select') {
        $.each(value, function (i, option_value) {
          $input.find("option[value='" + option_value + "']").prop('selected', true);
        });
      }
      else if (type !== 'submit' && type !== 'button') {
        input.value = value;
      }
      $input.removeData('webform-value');
    }

    // Restore required.
    var required = $input.data('webform-required');
    if (typeof required !== 'undefined') {
      if (required) {
        $input.prop('required', true);
      }
      $input.removeData('webform-required');
    }
  }

  /**
   * Clear an input's value and required attributes.
   *
   * @param input
   *   An input.
   */
  function clearValueAndRequired(input) {
    var $input = $(input);

    // Check for #states no clear attribute.
    // @see https://css-tricks.com/snippets/jquery/make-an-jquery-hasattr/
    if ($input.closest('[data-webform-states-no-clear]').length) {
      return;
    }

    // Clear value.
    var type = input.type;
    var tag = input.tagName.toLowerCase(); // Normalize case.
    if (type === 'checkbox' || type === 'radio') {
      $input.prop('checked', false);
    }
    else if (tag === 'select') {
      if ($input.find('option[value=""]').length) {
        $input.val('');
      }
      else {
        input.selectedIndex = -1;
      }
    }
    else if (type !== 'submit' && type !== 'button') {
      input.value = (type === 'color') ? '#000000' : '';
    }

    // Clear required.
    $input.prop('required', false);
  }

})(jQuery, Drupal);
;
/**
 * @file
 * JavaScript behaviors for webforms.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Autofocus first input.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the webform autofocusing.
   */
  Drupal.behaviors.webformAutofocus = {
    attach: function (context) {
      $(context).find('.webform-submission-form.js-webform-autofocus :input:visible:enabled:first').focus();
    }
  };

  /**
   * Prevent webform autosubmit on wizard pages.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for disabling webform autosubmit.
   *   Wizard pages need to be progressed with the Previous or Next buttons, not by pressing Enter.
   */
  Drupal.behaviors.webformDisableAutoSubmit = {
    attach: function (context) {
      // @see http://stackoverflow.com/questions/11235622/jquery-disable-form-submit-on-enter
      $(context).find('.webform-submission-form.js-webform-disable-autosubmit input')
        .not(':button, :submit, :reset, :image, :file')
        .once('webform-disable-autosubmit')
        .on('keyup keypress', function (e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) {
            e.preventDefault();
            return false;
          }
        });
    }
  };

  /**
   * Skip client-side validation when submit button is pressed.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the skipping client-side validation.
   */
  Drupal.behaviors.webformSubmitNoValidate = {
    attach: function (context) {
      $(context).find(':submit.js-webform-novalidate').once('webform-novalidate').on('click', function () {
        $(this.form).attr('novalidate', 'novalidate');
      });
    }
  };

  /**
   * Attach behaviors to trigger submit button from input onchange.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches form trigger submit events.
   */
  Drupal.behaviors.webformSubmitTrigger = {
    attach: function (context) {
      $('[data-webform-trigger-submit]').once('webform-trigger-submit').on('change', function () {
        var submit = $(this).attr('data-webform-trigger-submit');
        $(submit).mousedown();
      });
    }
  };

  /**
   * Custom required error message.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the webform custom required error message.
   *
   * @see http://stackoverflow.com/questions/5272433/html5-form-required-attribute-set-custom-validation-message
   */
  Drupal.behaviors.webformRequiredError = {
    attach: function (context) {
      $(context).find(':input[data-webform-required-error]').once('webform-required-error')
        .on('invalid', function () {
          this.setCustomValidity('');
          if (!this.valid) {
            this.setCustomValidity($(this).attr('data-webform-required-error'));
          }
        })
        .on('input, change', function () {
          // Find all related elements by name and reset custom validity.
          // This specifically applies to required radios and checkboxes.
          var name = $(this).attr('name');
          $(this.form).find(':input[name="' + name + '"]').each(function () {
            this.setCustomValidity('');
          });
        });
    }
  };

  // When #state:required is triggered we need to reset the target elements
  // custom validity.
  $(document).on('state:required', function (e) {
    $(e.target).filter('[data-webform-required-error]')
      .each(function () {this.setCustomValidity('');});
  });

  if (window.imceInput) {
    window.imceInput.processUrlInput = function (i, el) {
      var button = imceInput.createUrlButton(el.id, el.getAttribute('data-imce-type'));
      el.parentNode.insertAfter(button, el);
    };
  }

})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

Drupal.debounce = function (func, wait, immediate) {
  var timeout = void 0;
  var result = void 0;
  return function () {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var context = this;
    var later = function later() {
      timeout = null;
      if (!immediate) {
        result = func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      result = func.apply(context, args);
    }
    return result;
  };
};;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, debounce) {
  var liveElement = void 0;
  var announcements = [];

  Drupal.behaviors.drupalAnnounce = {
    attach: function attach(context) {
      if (!liveElement) {
        liveElement = document.createElement('div');
        liveElement.id = 'drupal-live-announce';
        liveElement.className = 'visually-hidden';
        liveElement.setAttribute('aria-live', 'polite');
        liveElement.setAttribute('aria-busy', 'false');
        document.body.appendChild(liveElement);
      }
    }
  };

  function announce() {
    var text = [];
    var priority = 'polite';
    var announcement = void 0;

    var il = announcements.length;
    for (var i = 0; i < il; i++) {
      announcement = announcements.pop();
      text.unshift(announcement.text);

      if (announcement.priority === 'assertive') {
        priority = 'assertive';
      }
    }

    if (text.length) {
      liveElement.innerHTML = '';

      liveElement.setAttribute('aria-busy', 'true');

      liveElement.setAttribute('aria-live', priority);

      liveElement.innerHTML = text.join('\n');

      liveElement.setAttribute('aria-busy', 'false');
    }
  }

  Drupal.announce = function (text, priority) {
    announcements.push({
      text: text,
      priority: priority
    });

    return debounce(announce, 200)();
  };
})(Drupal, Drupal.debounce);;
/**
 * @file
 * JavaScript behaviors for details element.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.detailsToggle = Drupal.webform.detailsToggle || {};
  Drupal.webform.detailsToggle.options = Drupal.webform.detailsToggle.options || {};

  /**
   * Attach handler to toggle details open/close state.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformDetailsToggle = {
    attach: function (context) {
      $('.js-webform-details-toggle', context).once('webform-details-toggle').each(function () {
        var $form = $(this);
        var $tabs = $form.find('.webform-tabs');

        // Get only the main details elements and ignore all nested details.
        var selector = ($tabs.length) ? '.webform-tab' : '.js-webform-details-toggle';
        var $details = $form.find('details').filter(function () {
          // @todo Figure out how to optimize the below code.
          var $parents = $(this).parentsUntil(selector);
          return ($parents.find('details').length === 0);
        });

        // Toggle is only useful when there are two or more details elements.
        if ($details.length < 2) {
          return;
        }

        var options = $.extend({
          'button': '<button type="button" class="webform-details-toggle-state"></button>'
        }, Drupal.webform.detailsToggle.options);

        // Create toggle buttons.
        var $toggle = $(options.button)
          .attr('title', Drupal.t('Toggle details widget state.'))
          .on('click', function (e) {
            var open;
            if (isFormDetailsOpen($form)) {
              $form.find('details').removeAttr('open');
              open = 0;
            }
            else {
              $form.find('details').attr('open', 'open');
              open = 1;
            }
            setDetailsToggleLabel($form);

            // Set the saved states for all the details elements.
            // @see webform.element.details.save.js
            if (Drupal.webformDetailsSaveGetName) {
              $form.find('details').each(function () {
                var name = Drupal.webformDetailsSaveGetName($(this));
                if (name) {
                  localStorage.setItem(name, open);
                }
              });
            }
          })
          .wrap('<div class="webform-details-toggle-state-wrapper"></div>')
          .parent();

        if ($tabs.length) {
          // Add toggle state before the tabs.
          $tabs.find('.item-list').before($toggle);
        }
        else {
          // Add toggle state link to first details element.
          $details.eq(0).before($toggle);
        }

        setDetailsToggleLabel($form);
      });
    }
  };

  /**
   * Determine if a webform's details are all opened.
   *
   * @param {jQuery} $form
   *   A webform.
   *
   * @return {boolean}
   *   TRUE if a webform's details are all opened.
   */
  function isFormDetailsOpen($form) {
    return ($form.find('details[open]').length === $form.find('details').length);
  }

  /**
   * Set a webform's details toggle state widget label.
   *
   * @param {jQuery} $form
   *   A webform.
   */
  function setDetailsToggleLabel($form) {
    var isOpen = isFormDetailsOpen($form);

    var label = (isOpen) ? Drupal.t('Collapse all') : Drupal.t('Expand all');
    $form.find('.webform-details-toggle-state').html(label);

    var text = (isOpen) ? Drupal.t('All details have been expanded.') : Drupal.t('All details have been collapsed.');
    Drupal.announce(text);
  }

})(jQuery, Drupal);
;
/**
 * @file
 * JavaScript behaviors for details element.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attach handler to save details open/close state.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformDetailsSave = {
    attach: function (context) {
      if (!window.localStorage) {
        return;
      }

      // Summary click event handler.
      $('details > summary', context).once('webform-details-summary-save').click(function () {
        var $details = $(this).parent();


        // @see https://css-tricks.com/snippets/jquery/make-an-jquery-hasattr/
        if ($details[0].hasAttribute('data-webform-details-nosave')) {
          return;
        }

        var name = Drupal.webformDetailsSaveGetName($details);
        if (!name) {
          return;
        }

        var open = ($details.attr('open') !== 'open') ? '1' : '0';
        localStorage.setItem(name, open);
      });

      // Initialize details open state via local storage.
      $('details', context).once('webform-details-save').each(function () {
        var $details = $(this);

        var name = Drupal.webformDetailsSaveGetName($details);
        if (!name) {
          return;
        }

        var open = localStorage.getItem(name);
        if (open === null) {
          return;
        }

        if (open === '1') {
          $details.attr('open', 'open');
        }
        else {
          $details.removeAttr('open');
        }
      });
    }

  };

  /**
   * Get the name used to store the state of details element.
   *
   * @param {jQuery} $details
   *   A details element.
   *
   * @return string
   *   The name used to store the state of details element.
   */
  Drupal.webformDetailsSaveGetName = function ($details) {
    if (!window.localStorage) {
      return '';
    }

    // Any details element not included a webform must have define its own id.
    var webformId = $details.attr('data-webform-element-id');
    if (webformId) {
      return 'Drupal.webform.' + webformId.replace('--', '.');
    }

    var detailsId = $details.attr('id');
    if (!detailsId) {
      return '';
    }

    var $form = $details.parents('form');
    if (!$form.length || !$form.attr('id')) {
      return '';
    }

    var formId = $form.attr('id');
    if (!formId) {
      return '';
    }

    // ISSUE: When Drupal renders a webform in a modal dialog it appends a unique
    // identifier to webform ids and details ids. (i.e. my-form--FeSFISegTUI)
    // WORKAROUND: Remove the unique id that delimited using double dashes.
    formId = formId.replace(/--.+?$/, '').replace(/-/g, '_');
    detailsId = detailsId.replace(/--.+?$/, '').replace(/-/g, '_');
    return 'Drupal.webform.' + formId + '.' + detailsId;
  }

})(jQuery, Drupal);
;
